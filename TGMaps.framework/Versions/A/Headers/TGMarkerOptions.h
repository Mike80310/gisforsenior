//
//  TGOSMarkerOptions.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/10.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TGMarkerOptions : NSObject

@property (nonatomic, assign) CLLocationCoordinate2D position;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *snippet;

@property (nonatomic, strong) UIImage *icon;

@property (nonatomic, assign) CGPoint groundAnchor;

@property (nonatomic, assign) CGPoint infoWindowAnchor;

@property (nonatomic, assign) BOOL tappable;

@property (nonatomic, assign) BOOL draggable;

@property (nonatomic, assign) BOOL visibility;

@property (nonatomic, strong) id userData;

/** Convenience constructor for default initialized options. */
+ (TGMarkerOptions *)options;

@end
