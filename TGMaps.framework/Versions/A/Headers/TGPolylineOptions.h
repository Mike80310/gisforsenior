//
//  TGOSPolylineOptions.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/9.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class TGPath;

typedef enum {
    kTGLineStyleSolid = 0,
    kTGLineStyleDash,
    kTGLineStyleDot,
    kTGLineStyleDashDot,
    kTGLineStyleDashDotDot,
    kTGLineStyleNull,
} TGLineStyle;

@interface TGPolylineOptions : NSObject

@property (nonatomic) TGPath *path;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, assign) int width;

@property (nonatomic, assign) TGLineStyle style;

@property (nonatomic, assign) BOOL visibility;

/** Convenience constructor for default initialized options. */
+ (TGPolylineOptions *)options;

@end
