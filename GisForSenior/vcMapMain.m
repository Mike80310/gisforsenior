//
//  ViewController.m
//  GisForSenior
//
//  Created by jane on 2015/7/1.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcMapMain.h"
#import "vcCenterInfo.h"
#import "vcPathPlanning.h"

#define pickerRowHeight 40 //在picker的row高度
#define pickerHeight 200 //picker本身的高度 也是放pk的cell的高度
#define kArea @"Area" //區域
#define kCity @"City" //縣市
#define kStrLostResource @"尚未選擇服務方式"
#define kHomestayNo 2

#define kRegionName @"Name"
#define kRegionParentNo @"ParentNo"
#define kRegionNo @"No"

#define kContidtionBarOpenSectionCount 6

@interface vcMapMain ()
{
    int nSpreadCell; //展開的cell
    
    NSArray *aryServiceList;//資源的所有資料
    int nServiceType;//目前選取的資源類型
    NSMutableArray *aryServiceItemNo; //目前選取的項目列表
    
    NSArray *aryCenterInfoList;//所有機構的資料
    NSDictionary *dicNowCenterInfo;//單一機構的資料
    NSArray *aryShowInfo;//篩選後顯示的資料

    NSMutableArray *aryArea;//地區列表
    NSArray *aryCityList;
    NSString *strCityArea;//目前選區的行政區
    NSArray *aryAreaType;//for居服用
    int nAreaTypeIndex;
    
    int nCitySelectedIndex;
    NSString *strRegionAreaNo;
    
    TGMapView *mapView_;
    id<TGMarker> mkUserLocation;
    id<TGMarker> mkNowSelected;
    NSString* addresslocator2;//行政區定位service url
    NSMutableArray * aryMarkerList;//地圖上的圖釘列表
    CLLocationCoordinate2D locationUser;
    
    int nConditionCellCount;
    BOOL bCondtionBarIsOpen;
    
    BOOL bFromVcMainLogin;//是不是從vcMain進來的
    
    UIButton *btnNewLeft;
    
    int nKeyBoardInitY;
    NSArray* aryKeyBoardMoveControllers;
    NSArray* aryKeyBoardTxt;
    UITapGestureRecognizer* tapKeyBoard;

    SoapTool *soap;
    
    float fNavButtonWidth;
    float fNavButtonInterval;
    
    CGRect cgOriConditionBar;
}
@end

@implementation vcMapMain

-(void)viewDidAppear:(BOOL)animated
{
    if([self.strPrePage isEqualToString:@""]) return; //防止從第二頁返回時重新定位
    
    NSDictionary *dicLatLng = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserLatLng];
    locationUser = CLLocationCoordinate2DMake([[dicLatLng objectForKey:@"Lat"] doubleValue],
                                              [[dicLatLng objectForKey:@"Lng"] doubleValue]);
    
    [self downloadListWithLocation:locationUser];
    [self initNewLeftButton];
}

- (void) viewDidDisappear:(BOOL)animated
{
    self.strPrePage = @"";//防止從第二頁返回時重新定位
    if (btnNewLeft)  [btnNewLeft removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    soap = [[SoapTool alloc] init];
    soap.delegate = self;
    
    nAreaTypeIndex = -1;
    nServiceType = -1;
    nSpreadCell = -1;
    aryServiceItemNo =[[NSMutableArray alloc] init];
    aryMarkerList = [[NSMutableArray alloc] init];
    aryAreaType = @[@"單位位址",@"服務範圍"];
    
    aryArea = [[NSUserDefaults standardUserDefaults] objectForKey:kServiceRegionList];
    aryCityList = [self getRegionCityList];
    
    cgOriConditionBar = self.tvConditionList.frame;
    
    [self setTvConditionStyle];
    [self setTvResultStyle];
    [self setMap];
    [self setSearchBar];
    [self initKeyBoardModifier];
    
    if ([self.strPrePage isEqualToString:@"vcMain"]) //判斷從哪一頁過來
    {
        btnLeft.hidden = NO;
        
        bFromVcMainLogin = YES;
        self.vMap.hidden = YES;
        self.tvResult.hidden = NO;
        aryServiceList = [self getResetServiceType:
                          [[NSUserDefaults standardUserDefaults] valueForKey:kServiceCareCenterTypeList]];
//        bCondtionBarIsOpen = NO;
        [self openCondtionBar:YES];
    }
    else
    {
        btnLeft.hidden = YES;
        self.vMap.hidden = YES;
        self.tvResult.hidden = NO;
        
        fNavButtonWidth = 45;
        fNavButtonInterval = 22;
        
        fFontSize = 18;
        
        if (screenSize.height == kI6ScreenHeight)
        {
            fNavButtonWidth += 5;
            fFontSize = 20;
        }
        else if(screenSize.height > kI6ScreenHeight)
        {
            fNavButtonWidth += 10;
            fFontSize = 20;
        }
        
        [self initNewLeftButton];
         [self openCondtionBar:YES];
        [self startGetSurveyResult];
        
        bFromVcMainLogin = NO;
        
        
    }
}

- (void) startGetSurveyResult
{
    [self HudShowMsg:@"尋找符合的資源中..."];
    BOOL bIsDisabileities = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyDisabilities] boolValue];
    BOOL bIsAborigines = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyAborigines] boolValue];
    int nAge = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyAge] intValue];
    int nCareTime = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyCareTime] intValue];
    int nIntubation = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyIntubation] intValue];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        
        [soap getSurveyResult:bIsDisabileities IsAborigines:bIsAborigines Age:nAge CareTime:nCareTime Intubation:nIntubation adlLevel:0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // 更新界面
            
            
        });
    });

//    [soap getSurveyResult:bIsDisabileities IsAborigines:bIsAborigines Age:nAge CareTime:nCareTime Intubation:nIntubation];

}

#pragma mark - 條件bar相關

- (void) openCondtionBar:(BOOL) bIsOpen
{
    BOOL bTmp = bCondtionBarIsOpen;
    bCondtionBarIsOpen = bIsOpen;
    
    
    
    if (bIsOpen)
    {
        [self.txtSearchTxt resignFirstResponder];
        self.txtSearchTxt.text = @"";
        btnRight.hidden = YES;
        
        self.tvConditionList.frame = cgOriConditionBar;
        [self.view bringSubviewToFront:self.tvConditionList];
    }
    else
    {
        btnRight.hidden = NO;
        [self.tvResult reloadData];
        
        if(aryMarkerList.count != 0) [self clearMarker];
        
        [self addMarker];

        int nEmptyHeight;
        int nConditionHeight = barHeightInPlus;
        if (screenSize.height >= kI6ScreenHeight)
        {
            nEmptyHeight = barHeightInPlus/2 - 10;
        }
        else
        {
            nEmptyHeight = barHeightInPlus / 3 * 2 - 10;
            
        }
        
        CGRect cgCondition = self.tvConditionList.frame;
        cgCondition.size.height = nEmptyHeight+nConditionHeight;
        self.tvConditionList.frame = cgCondition;
        
        [self.view bringSubviewToFront:self.tvResult];
        
    }
    if (bTmp != bIsOpen) [self controlConditionAllSpreadCell];
    [self.view bringSubviewToFront:self.vSearchBar];
    [self.view bringSubviewToFront:self.vSearchTool];

}

- (void) controlConditionAllSpreadCell
{
//    if(nSpreadCell == -1 && bCondtionBarIsOpen) return;
    
    int nTmp = nSpreadCell;
    nSpreadCell = -1;
    
    if (bCondtionBarIsOpen)
    {
        NSMutableArray* aryInsert = [NSMutableArray new];
        NSMutableArray *aryReload = [NSMutableArray new];
        
        for (int x = kContidtionBarOpenSectionCount-1; x >= 0; x--)
        {
            if (x == 0 || x == 5)
            {
                [aryReload addObject:[NSIndexPath indexPathForRow:0 inSection:x]];
            }
            else
            {
                [aryInsert addObject:[NSIndexPath indexPathForRow:0 inSection:x]];
            }
           
        }
        
        [self.tvConditionList beginUpdates];
        [self.tvConditionList insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationFade];
        [self.tvConditionList reloadRowsAtIndexPaths:aryReload withRowAnimation:UITableViewRowAnimationFade];
        [self.tvConditionList endUpdates];
        [aryInsert removeAllObjects];
    }
    else
    {
        NSMutableArray* aryDelete = [NSMutableArray new];
        NSMutableArray *aryReload = [NSMutableArray new];
        for (int x = 0; x < kContidtionBarOpenSectionCount; x++)
        {
            if (x == 0 || x == 5)
            {
                [aryReload addObject:[NSIndexPath indexPathForRow:0 inSection:x]];
            }
            else
            {
                
                [aryDelete addObject:[NSIndexPath indexPathForRow:0 inSection:x]];
                if (nTmp == x)
                {
                    //                    [self deleteCell:[NSIndexPath indexPathForRow:1 inSection:nTmp]];
                    [aryDelete addObject:[NSIndexPath indexPathForRow:1 inSection:nTmp]];
                    nTmp = -1;
                    
                }
                
            }
        }
        
        [self.tvConditionList beginUpdates];
        [self.tvConditionList deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationFade];
        [self.tvConditionList reloadRowsAtIndexPaths:aryReload withRowAnimation:UITableViewRowAnimationFade];
        [self.tvConditionList endUpdates];
        [aryDelete removeAllObjects];
    }
}

- (IBAction) clickCondtion:(id)sender
{
    if (aryShowInfo.count==0)
    {
        [self HudErrMsgWithError:@"目前沒有資料顯示" finishBlock:^{}];
        return;
    }
    
    [self openCondtionBar: !bCondtionBarIsOpen];
}


- (IBAction) searchContitionResult:(id)sender
{
    if (nServiceType == -1 )
    {
        [self HudErrMsgWithError:kStrLostResource finishBlock:^{}];

        return;
    }
    else if (aryServiceItemNo.count == 0)
    {
        
        [self HudErrMsgWithError:@"尚未選擇分類" finishBlock:^{}];

        return;
    }
    NSString *strKeyword;
    [self HudShowMsg:@"資料下載中..."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        if (nAreaTypeIndex == -1)
        {
            [soap getCareCenterData:aryServiceItemNo RegionNo:strRegionAreaNo IsHomeServiceRegion:false CenterName:strKeyword];
        }
        else
        {
            [soap getCareCenterData:aryServiceItemNo RegionNo:strRegionAreaNo IsHomeServiceRegion:(BOOL)nAreaTypeIndex CenterName:strKeyword];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // 更新界面
            
            
        });
    });

    
}

-(IBAction) clickSearchText:(id)sender
{
    [self.view removeGestureRecognizer:tapKeyBoard];
    [self.txtSearchTxt resignFirstResponder];

    if ([self.txtSearchTxt.text isEqualToString:@""])
    {
        [self HudErrMsgWithError:@"未輸入任何文字" finishBlock:^{}];

        return;
    }
    
    [self HudShowMsg:@"名稱搜尋中..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        
        [soap searchCareCenterName:self.txtSearchTxt.text];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // 更新界面
            
            
        });
    });
    
    
   
}

#pragma mark - tableviewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView isEqual: self.tvConditionList])
    {
        return kContidtionBarOpenSectionCount;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual: self.tvConditionList])
    {
        if (section == nSpreadCell)
        {
            return 2;//展開的那個section要兩個row
        }
        
        if (bCondtionBarIsOpen)
        {
            return 1;
        }
        else
        {
            if (section == 0 || section == 5)
            {
                return 1;
            }
            return 0;
        }
        return 1;
    }
    else if([tableView isEqual: self.tvResult])
    {
        return aryShowInfo.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual: self.tvConditionList])
    {
        NSLog(@"section %li row %li",(long)indexPath.section,(long)indexPath.row);
        if (indexPath.section == 0)
        {
            if (bCondtionBarIsOpen)
            {
                if (screenSize.height >= kI6ScreenHeight)
                {
                    NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,barHeightInPlus/2);
                    return barHeightInPlus/2;
                }
                else
                {
                    NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,barHeightInPlus / 3 -5);
                    return barHeightInPlus / 3 - 5;
                }
            }
            else
            {
                if (screenSize.height >= kI6ScreenHeight)
                {
                    NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,barHeightInPlus/2 - 10);
                    return barHeightInPlus/2 - 10;
                }
                else
                {
                    NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,barHeightInPlus/3 - 15);
                    return barHeightInPlus / 3 - 15;
                }
            }
            
            
        }
        
        if (indexPath.section == 1 && indexPath.row ==1)
        {
            NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,pickerHeight);
            return pickerHeight;
        }
        else if (indexPath.section == 2 && indexPath.row ==1)
        {
            int nCount = (int)[[[aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"] count];
            NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,nCount * barHeightInPlus);
            return nCount * barHeightInPlus;
        }
        else if (indexPath.section == 3 && indexPath.row ==1)
        {
            NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,pickerHeight);
            return pickerHeight;
        }
        else if (indexPath.section == 4)
        {
            NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,barHeightInPlus + 20);
            return barHeightInPlus + 20;
        }
        else if(indexPath.section == 5)
        {
            NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,barHeightInPlus);
            return barHeightInPlus;
        }
        
        if (screenSize.height >= kI6ScreenHeight)
        {
            NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,barHeightInPlus+10);
            return barHeightInPlus + 10;
        }
        else
        {
            NSLog(@"section %li row %li height %i",(long)indexPath.section,(long)indexPath.row,40 +5);
            return barHeightInPlus;
        }
        
        
        
    }
    else if([tableView isEqual: self.tvResult])
    {
        if (screenSize.height >= kI6ScreenHeight) {
            return 95;
        }
        return 75;
    }
    return barHeightInPlus;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;

    if ([tableView isEqual: self.tvConditionList])
    {
        NSString *CellIdentifier;
        if (indexPath.section == 0)
        {
            CellIdentifier = [NSString stringWithFormat:@"EmptyCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (bCondtionBarIsOpen)
            {
                cell.backgroundColor =[UIColor whiteColor];
            }
            else
            {
                cell.backgroundColor =[UIColor clearColor];
            }
        }
        else if(indexPath.section == 4)
        {
            CellIdentifier = [NSString stringWithFormat:@"endCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
            
            CGRect cgTmp = vContent.frame;
            cgTmp.size.width = cell.contentView.frame.size.width - vContent.frame.origin.x * 2;
            cgTmp.size.height = cell.contentView.frame.size.height - vContent.frame.origin.y * 2;
            vContent.frame = cgTmp;
            vContent.backgroundColor = [UIColor whiteColor];
            
            UIButton *btn = (UIButton *) [cell.contentView viewWithTag:104];
            CGRect cgBtn = btn.frame;
            cgBtn.origin.y = cgTmp.size.height/2 - cgBtn.size.height/2;
            btn.frame = cgBtn;
            btn.layer.cornerRadius = 25;
            btn.layer.masksToBounds = YES;
            [btn addTarget:self action:@selector(searchContitionResult:) forControlEvents:UIControlEventTouchUpInside];

        }
        else if(indexPath.section == 5 )
        {
            CellIdentifier = [NSString stringWithFormat:@"ConditionCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
            
            CGRect cgTmp = vContent.frame;
            cgTmp.size.height = barHeightInPlus /3 * 2;
            vContent.frame = cgTmp;
            vContent.backgroundColor = [UIColor clearColor];
            cell.backgroundColor = [UIColor clearColor];
            
            UIButton *btn = (UIButton *) [cell.contentView viewWithTag:101];
            cgTmp.origin.x = 0;
            btn.frame = cgTmp;
            [btn addTarget:self action:@selector(clickCondtion:) forControlEvents:UIControlEventTouchUpInside];
            
            UIImageView *imgLeft = (UIImageView *) [cell.contentView viewWithTag:102];
            UIImageView *imgRight = (UIImageView *) [cell.contentView viewWithTag:103];
            
            [self imageRotateActionForConditionCell:bCondtionBarIsOpen vImage:imgLeft];
            [self imageRotateActionForConditionCell:bCondtionBarIsOpen vImage:imgRight];
        }
        else
        {
            
            CellIdentifier = [NSString stringWithFormat:@"titleCell"];
            
            if (indexPath.section == 1)
            {
                if (indexPath.row == 0)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    NSString *str = @"服務方式";
                    
                    if (nServiceType != -1) {
                        str = [aryServiceList[nServiceType] objectForKey:@"ServiceTypeName"];
                        str = [NSString stringWithFormat:@"服務方式：%@",str];
                    }
                    
                    return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:str];
                }
                else
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"scCell"];
                    return  [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:301];
                }
               
            }
            else if(indexPath.section == 2)
            {
                if (indexPath.row == 0)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"分類選擇(可複選)"];
                }
                else
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"btnListCell"];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
                    
                    CGRect cgTmp = vContent.frame;
                    cgTmp.size.width = cell.contentView.frame.size.width - vContent.frame.origin.x * 2;
                    cgTmp.size.height = cell.contentView.frame.size.height - vContent.frame.origin.y * 2;
                    vContent.frame = cgTmp;
                    vContent.backgroundColor = [UIColor whiteColor];
                    
                    int iIndex = 1;
                    NSArray *aryServiceItem =[[aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
                    
                    for (UIView *vTmp in [vContent subviews])
                    {
                        UIButton *btn = (UIButton *) vTmp;
                        
                        [btn addTarget:self action:@selector(clickServicceItemBtn:) forControlEvents:UIControlEventTouchUpInside];
                        btn.titleLabel.font = [UIFont systemFontOfSize:fFontSize];
                        CGRect cgBtn = btn.frame;
                        cgBtn.origin.y = (iIndex-1) * barHeightInPlus;
                        cgBtn.size.height = 40;
                        btn.frame = cgBtn;
                        
                        btn.layer.cornerRadius = 20;
                        btn.layer.masksToBounds = YES;
                        
                        if(iIndex > aryServiceItem.count)
                        {
                            btn.hidden = YES;
                            
                        }
                        else
                        {
                            btn.hidden = NO;
                            NSString *strItemName = [[aryServiceItem objectAtIndex:iIndex-1] objectForKey:@"ItemName"];
                            btn.tag = [[[aryServiceItem objectAtIndex:iIndex-1] objectForKey:@"ItemNo"] intValue];
                            [btn setTitle:strItemName forState:UIControlStateNormal];
                            [btn setTitle:strItemName forState:UIControlStateSelected];
                        }
                        
                        if(aryServiceItemNo.count == 0)
                        {
                            [btn setSelected:NO];
                            [self changeBtnColor:btn];
                            
                        }
                        
                        iIndex ++;
                    }
                }
            }
            else if (indexPath.section == 3)
            {
                if (indexPath.row == 0)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if ([strCityArea isEqualToString:@""])
                    {
                        return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"地區選擇"];
                    }
                    else
                    {
                        return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:strCityArea];
                    }
                }
                else
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"sc2Cell"];
                    return [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:302];

                }
            }
            
        }
        
        
        return cell;
    }
    else if([tableView isEqual: self.tvResult])
    {

        NSString *CellIdentifier = [NSString stringWithFormat:@"infoCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        return [self setInfoCell:cell cellForRowAtIndexPath:indexPath];
    }
    
    return cell;
}

- (UITableViewCell *) setTitleCell:(UITableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath titleName:(NSString *) strName
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    
    CGRect cgTmp = vContent.frame;
    cgTmp.size.width = cell.contentView.frame.size.width - vContent.frame.origin.x * 2;
    cgTmp.size.height = cell.contentView.frame.size.height - vContent.frame.origin.y * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    
    UIView *vBG = (UIView *) [cell.contentView viewWithTag:105];
    CGRect cgBG = vBG.frame;
    cgBG.origin.y = cgTmp.size.height/2 - cgBG.size.height/2;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        vBG.layer.cornerRadius = 25;
    }
    else
    {
        vBG.layer.cornerRadius = 20;
    }
    vBG.frame = cgBG;
    vBG.layer.masksToBounds = YES;
    
    UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
    lblTitleName.text = strName;
    
    UIImageView *imgArrow = (UIImageView *) [cell.contentView viewWithTag:102];

    if (screenSize.height >= kI6ScreenHeight)
    {
        
        CGRect cgBtn = imgArrow.frame;
        cgBtn.size = CGSizeMake(30, 30);
        cgBtn.origin.y = cgTmp.origin.y + 10;
        imgArrow.frame = cgBtn;
        
        CGRect cgTitle = lblTitleName.frame;
        cgTitle.origin.y = cgTmp.origin.y + 10;
        cgTitle.origin.x = cgTmp.origin.y + 15;
        lblTitleName.frame = cgTitle;
        lblTitleName.font = [UIFont systemFontOfSize:fFontSize];
        
    }
    
    return cell;
}

- (UITableViewCell *) setInfoCell:(UITableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    NSDictionary *dicInfo = [aryShowInfo objectAtIndex:indexPath.row];

    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    
    CGRect cgTmp = vContent.frame;
    cgTmp.size.width = cell.contentView.frame.size.width - vContent.frame.origin.x * 2;
    cgTmp.size.height = cell.contentView.frame.size.height - vContent.frame.origin.y * 2;
    
    vContent.frame = cgTmp;
    vContent.layer.cornerRadius = 30;
    vContent.layer.masksToBounds = YES;
    
    vContent.backgroundColor = [UIColor whiteColor];
    
    UILabel *lblCenterTitle = (UILabel *) [cell.contentView viewWithTag:101];
    UILabel *lblCenterTel = (UILabel *) [cell.contentView viewWithTag:102];
    
    
    lblCenterTitle.font = [UIFont boldSystemFontOfSize:fFontSize];
    lblCenterTel.font = [UIFont boldSystemFontOfSize:fFontSize];
    
    lblCenterTitle.text = [dicInfo objectForKey:@"Name"];
    lblCenterTel.text = [NSString stringWithFormat:@"電話｜%@",[dicInfo objectForKey:@"Tel"]];
    
    UIView *vTagContent = (UIView *) [cell.contentView viewWithTag:103];
    CGRect cgTag = vTagContent.frame;
    cgTag.origin.x = cgTmp.size.width - cgTag.size.width;
    vTagContent.frame = cgTag;
    vTagContent.layer.cornerRadius = 13;
    vTagContent.layer.masksToBounds = YES;
    
    UILabel *lblCenterTag = (UILabel *) [cell.contentView viewWithTag:104];
    NSArray *aryTag = [dicInfo objectForKey:@"ServiceTypeShortName"];
    lblCenterTag.text = aryTag[0];
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        cgTmp.size.height -= 5;
        vContent.frame = cgTmp;
        vContent.layer.cornerRadius = 35;
        CGRect cgTel = lblCenterTel.frame;
        cgTel.origin.y = lblCenterTitle.frame.origin.y + lblCenterTitle.frame.size.height + 8;
        lblCenterTel.frame = cgTel;
    }
    
    return cell;
}

- (UITableViewCell *) setScCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath ScrollViewTag:(int) nTag
{
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    
    CGRect cgTmp = vContent.frame;
    cgTmp.size.width = cell.contentView.frame.size.width - vContent.frame.origin.x * 2;
    cgTmp.size.height = cell.contentView.frame.size.height - vContent.frame.origin.y * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    
    if (nTag == 301)
    {
        UIPickerView *pv301 = (UIPickerView *) [cell.contentView viewWithTag:nTag];
        pv301.delegate = self;
        pv301.dataSource = self;
        pv301.backgroundColor = [UIColor clearColor];

        CGRect cgPV =pv301.frame;
        cgPV.size.width = cgTmp.size.width;
        
        if(screenSize.height >= kI6ScreenHeight)
        {
            cgPV.origin.y = vContent.frame.origin.y;
            cgPV.size.height = vContent.frame.size.height;
        }
        pv301.frame = cgPV;
    }
    else
    {
        UIPickerView *pv302 = (UIPickerView *) [cell.contentView viewWithTag:302];

        pv302.backgroundColor = [UIColor clearColor];
        
        UIPickerView *pv303 = (UIPickerView *) [cell.contentView viewWithTag:303];//居服使用
        pv303.backgroundColor = [UIColor clearColor];
        
        if (nServiceType == kHomestayNo)
        {
            pv302.hidden = YES;
            pv303.hidden = NO;
            pv303.delegate = self;
            pv303.dataSource = self;
            pv302.delegate = nil;
            pv302.dataSource = nil;

        }
        else
        {
            pv302.hidden = NO;
            pv303.hidden = YES;
            pv303.delegate = nil;
            pv303.dataSource = nil;
            pv302.delegate = self;
            pv302.dataSource = self;
            
        }
        CGRect cgPV =pv302.frame;
        cgPV.size.width = cgTmp.size.width;
        
        if(screenSize.height >= kI6ScreenHeight)
        {
            cgPV.origin.y = vContent.frame.origin.y;
            cgPV.size.height = vContent.frame.size.height;
        }
        pv302.frame = cgPV;
        pv303.frame = cgPV;

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([tableView isEqual:self.tvConditionList])
    {
        
        if (indexPath.section == 0 ) return;
        
        if (indexPath.section ==4)
        {
            [self searchContitionResult:nil];
        }
        else if(indexPath.section == 5)
        {
            [self clickCondtion:nil];
        }
        else
        {
            if (nSpreadCell == -1) //還沒有展開的Cell
            {
                if (indexPath.section == 2 && nServiceType == -1)//還沒有選資源，分類不會展開
                {
                    [self HudDismissWithError:kStrLostResource finishBlock:^{
                        
                    } Title:nil];
                    
                    return;
                }
                [self SpreadClassCell:indexPath];
                return;
            }
            
            if (indexPath.section == nSpreadCell && indexPath.row == 0) //把已經展開的Cell關起來
            {
                nSpreadCell = -1;
                [self deleteCell:indexPath];
                return;
            }
            
            if (indexPath.row == 0)//展開其他cell
            {
                if (indexPath.section == 2 && nServiceType == -1) //還沒有選資源，分類不會展開
                {
                    [self HudDismissWithError:kStrLostResource finishBlock:^{
                        
                    } Title:nil];

                    return;
                }
                
                [self SpreadClassCell:indexPath];
            }

        
        }
        
    }
    else
    {
        dicNowCenterInfo = aryShowInfo[indexPath.row];
        [self pushToVcCenterInfo];
    }
    
}

#pragma mark - btnListCell 的相關功能

- (IBAction) clickServicceItemBtn:(id)sender
{
    UIButton *btn = (UIButton *) sender;
    [btn setSelected:!btn.selected];
    [self changeBtnColor:btn];
}

- (void) changeBtnColor:(UIButton *) btn
{
    if (btn.selected)
    {
        [aryServiceItemNo addObject:[NSNumber numberWithInt:(int)btn.tag]];
        
        btn.backgroundColor = clrMaker(255, 205, 0, 1);
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    }
    else
    {
        [aryServiceItemNo removeObject:[NSNumber numberWithInt:(int)btn.tag]];
        btn.backgroundColor = clrMaker(220, 221, 221, 1);
    }
    
}

#pragma mark - cell的展開與關閉

- (void) SpreadClassCell:(NSIndexPath *)indexPath
{
    if (nSpreadCell == -1)
    {
        NSIndexPath * ip;
        
        [self imageRotateAction:NO indexPath:indexPath];
        ip = [NSIndexPath indexPathForRow:0 inSection:nSpreadCell];
        [self imageRotateAction:YES indexPath:ip];
        
        nSpreadCell = (int)indexPath.section;
        
        [self creatCell:indexPath];
    }
    else if(indexPath.section != nSpreadCell)
    {
        NSIndexPath * ip;
        
        [self imageRotateAction:NO indexPath:indexPath];
        ip = [NSIndexPath indexPathForRow:0 inSection:nSpreadCell];
        [self imageRotateAction:YES indexPath:ip];
        
        nSpreadCell = (int)indexPath.section;
        
        [self updateTableView:ip InsertIndex:indexPath];
        
    }
    
    
    
}

- (void) deleteCell:(NSIndexPath*) indexPath
{
    [self imageRotateAction:YES indexPath:indexPath];
    NSMutableArray* aryDelete = [NSMutableArray new];
    [aryDelete addObject:[NSIndexPath indexPathForRow:1 inSection:indexPath.section]];
    
    [self.tvConditionList beginUpdates];
    [self.tvConditionList deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationFade];
    [self.tvConditionList endUpdates];
    [aryDelete removeAllObjects];
}

- (void) creatCell:(NSIndexPath*) indexPath
{
    NSMutableArray* aryInsert = [NSMutableArray new];
    [aryInsert addObject:[NSIndexPath indexPathForRow:1 inSection:indexPath.section]];
    
    [self.tvConditionList beginUpdates];
    [self.tvConditionList insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationFade];
    [self.tvConditionList endUpdates];
    
    [aryInsert removeAllObjects];
}

- (void) updateTableView:(NSIndexPath *) removeIndex InsertIndex:(NSIndexPath *)insertIndex
{
    NSMutableArray* aryDelete = [NSMutableArray new];
    NSMutableArray* aryInsert = [NSMutableArray new];

    NSIndexPath *ip = [NSIndexPath indexPathForRow:1 inSection:removeIndex.section];
    [aryDelete addObject:ip];
    
    ip = [NSIndexPath indexPathForRow:1 inSection:insertIndex.section];
    [aryInsert addObject:ip];
    
    [self.tvConditionList beginUpdates];
    [self.tvConditionList deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationFade];
    [self.tvConditionList insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationFade];
    [self.tvConditionList endUpdates];
    
    int nTmp = nSpreadCell;
    nSpreadCell = 2;
    [self changeCellTitle:@"分類選擇(可複選)"];
    nSpreadCell = nTmp;
    
    [aryInsert removeAllObjects];
    [aryDelete removeAllObjects];
    
}

#pragma mark - bar名稱改變

- (void) changeCellTitle:(NSString *) strNewTitle
{

    UITableViewCell *cell = [self.tvConditionList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:nSpreadCell]];
    
    for (UIView *v100 in cell.contentView.subviews) {
        
        for(UIView *v105 in [v100 subviews])
        {
            
            for(UIView *vTmp in [v105 subviews])
            {
                if (![vTmp isKindOfClass:[UILabel class]]) continue;
                
                UILabel *lbl = (UILabel *) vTmp;
                lbl.text = strNewTitle;
            }
            
            
        }
        
    }
    
}

#pragma mark - arrow的動畫

- (void) imageRotateActionForConditionCell:(BOOL) isRotate vImage:(UIImageView *)vImage
{
    UIImageView *imgArrow = (UIImageView *)vImage;
    [UIView animateWithDuration:.35
                     animations:^{
                         if (isRotate)
                         {
                             imgArrow.transform = CGAffineTransformIdentity;
                             
                         }else
                         {
                             imgArrow.transform = CGAffineTransformMakeRotation(180 * M_PI / 180);//180 * M_PI / 180 轉半圓
                         }
                         
                     }completion:^(BOOL finished) {
                     }];


}

- (void) imageRotateAction:(BOOL)isRotate indexPath:(NSIndexPath *) indexPath
{
    UITableViewCell *cell = [self.tvConditionList cellForRowAtIndexPath:indexPath];
    for (UIView *v100 in cell.contentView.subviews) {
        
        for(UIView *v105 in [v100 subviews])
        {
            
            for(UIView *vTmp in [v105 subviews])
            {
                if ([vTmp isKindOfClass:[UIImageView class]])
                {
                    UIImageView *imgArrow = (UIImageView *)vTmp;
                    [UIView animateWithDuration:.35
                                     animations:^{
                                         if (isRotate)
                                         {
                                             imgArrow.transform = CGAffineTransformIdentity;
                                             
                                         }else
                                         {
                                             imgArrow.transform = CGAffineTransformMakeRotation(M_PI_2);//180 * M_PI / 180 轉半圓
                                         }
                                         
                                     }completion:^(BOOL finished) {
                                     }];
                }
            }
            
            
        }
       
    }
    
    
}


#pragma mark - vcBase

- (void) clickRightButton:(id)sender
{
    if (self.tvResult.hidden)
    {
        
        [btnRight setImage:[UIImage imageNamed:kMapIcon] forState:UIControlStateNormal];
    }
    else
    {
         [btnRight setImage:[UIImage imageNamed:kListIcon] forState:UIControlStateNormal];
    }
    
    self.tvResult.hidden = !self.tvResult.hidden;
    self.vMap.hidden = !self.vMap.hidden;
}


- (void) clickLeftButton:(id)sender
{
    [self clearMarker];
    soap = nil;
    mapView_ = nil;
    [self.vMap insertSubview:nil atIndex:0];
    for (UIView *vSubMap in self.vMap.subviews)
    {
        if ([NSStringFromClass([vSubMap class]) isEqualToString:@"TGSGMapView"])
        {
            [vSubMap removeFromSuperview];
        }
    }
    
    [super clickLeftButton:sender];
}

#pragma mark - 所有元件style設定

- (void) setTvConditionStyle
{
    self.tvConditionList.delegate = self;
    self.tvConditionList.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tvConditionList.backgroundColor = [UIColor clearColor];
}

- (void) setTvResultStyle
{
    self.tvResult.delegate = self;
    self.tvResult.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tvResult.backgroundColor = [UIColor clearColor];
}

- (void) setSearchBar
{
//    self.vCondition.layer.cornerRadius = 30;
    self.vSearchTool.backgroundColor = [UIColor clearColor];
    self.vSearchBar.layer.cornerRadius = 20;
    self.vSearchBar.layer.borderWidth = 1;
    self.vSearchBar.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.txtSearchTxt.font = [UIFont boldSystemFontOfSize:fFontSize];
    self.txtSearchTxt.delegate = self;
    self.txtSearchTxt.returnKeyType = UIReturnKeySearch;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.vSearchBar.layer.cornerRadius = 25;
        
        CGRect cgSearchBar = self.vSearchBar.frame;
        cgSearchBar = CGRectMake(cgSearchBar.origin.x + 15,cgSearchBar.origin.y + (cgSearchBar.size.height/2 - self.txtSearchTxt.frame.size.height/2), self.txtSearchTxt.frame.size.width, cgSearchBar.size.height);
        self.txtSearchTxt.frame = cgSearchBar;
        
        CGRect cgSearchBtn = self.btnSearch.frame;
        cgSearchBtn = CGRectMake(screenSize.width - cgSearchBar.size.width/4, 0,
                                 cgSearchBar.size.width/4, cgSearchBar.size.height);
        self.btnSearch.frame = cgSearchBtn;
        
        CGRect cgTool = self.vSearchTool.frame;
        cgTool = CGRectMake(cgTool.origin.x, cgTool.origin.y + 5, cgTool.size.width, cgTool.size.height + 10);
        self.vSearchTool.frame = cgTool;
        
//        CGRect cgCondition = self.vCondition.frame;
//        cgCondition = CGRectMake(cgCondition.origin.x - 5, cgCondition.origin.y - 5, cgCondition.size.width + 10, cgCondition.size.height + 10);
//        self.vCondition.frame = cgCondition;
//        
//        self.btnCondition.titleLabel.font = [UIFont boldSystemFontOfSize:fFontSize];
        
        CGRect cgTvResult = self.tvResult.frame;
        cgTvResult = CGRectMake(cgTvResult.origin.x, cgTvResult.origin.y + 15, cgTvResult.size.width, cgTvResult.size.height - 15);
        self.tvResult.frame = cgTvResult;
    }
}

- (void) setMap
{
    
    addresslocator2 = @"http://gis.tgos.nat.gov.tw/addresslocator/locate.aspx?op=dist&format=json&nogeometry=true&keystr=defaulthash&district=";//行政區定位
    
    self.vCenterInfoBG.hidden = YES;
    [self setMapCenterInfo];

   
        mapView_ = [[TGMapView alloc] initWithFrame:self.vMap.bounds];
        [self.vMap insertSubview:mapView_ atIndex:0];
         mapView_.delegate = self;
    
    //以下用來顯示使用者位置
    TGMapGPSDisplay *mapGPSDisplay_ = [[TGMapGPSDisplay alloc]init];
    mapGPSDisplay_.DirectionArcFillColor = [UIColor redColor];
    mapGPSDisplay_.DirectionArcDegree = 60.f;
    mapGPSDisplay_.DirectionArcGradientStyle = YES;
    mapGPSDisplay_.DirectionArcRadius = 150;
    mapGPSDisplay_.ArcVisibility = NO;
    [mapView_ addMapGPSDisplay:mapGPSDisplay_];
    
    [self HudShowMsg:@"位置取得中"];
    [self getNowAddress];
//下一步到viewdidappear做
}

- (void) setMapCenterInfo
{
    self.lblCenterName.font = [UIFont boldSystemFontOfSize:fFontSize];
    
    self.vCenterInfoBG.backgroundColor = [UIColor clearColor];
    
    self.vCenterInfo.layer.masksToBounds = NO;
    self.vCenterInfo.layer.cornerRadius = 8;
    self.vCenterInfo.layer.shadowRadius = 5;
    self.vCenterInfo.layer.shadowOpacity = 0.5;
    
    self.vCenterInfo.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.vCenterInfo.bounds].CGPath;
    
    gestCenterInfoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushToVcCenterInfo)];
    [self.vCenterInfoBG addGestureRecognizer:gestCenterInfoTap];
}

- (void) downloadListWithLocation:(CLLocationCoordinate2D)loc
{
    [self stopUpdateCurrentLocation];
    [self startGoogleApi];
}

#pragma mark - tgos

-(void)clearMarker
{
    self.lblCenterName.text = @"";
    self.vCenterInfoBG.hidden = YES;
    
    for (id<TGMarker> marker in aryMarkerList) {
        [marker setUserData:nil];
        [marker setIcon:nil];
        [marker remove];
    }
    [mapView_ clear];
    [mapView_ startRendering];
    [aryMarkerList removeAllObjects];
}

- (void) addMarker
{
    for (NSDictionary *dic in aryShowInfo)
    {
        double dLat = [[dic objectForKey:@"Lat"] doubleValue];
        double dLng = [[dic objectForKey:@"Lng"] doubleValue];
        
        TGMarkerOptions *options1 = [[TGMarkerOptions alloc] init];
        options1.position = CLLocationCoordinate2DMake(dLat, dLng);
        options1.userData = dic;
        options1.icon = [UIImage imageNamed:strMapOriIcon];
        
        [aryMarkerList addObject:[mapView_ addMarkerWithOptions:options1]];
    }
    
}

- (IBAction)RegionClick:(id)sender
{
    NSString *strTmp = [strCityArea stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString * seachStr = [addresslocator2 stringByAppendingString:[strTmp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL * url = [NSURL URLWithString:seachStr];
    
    NSError *error;
    NSDictionary *ServiceJson = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url]
                                                                options:NSJSONReadingMutableLeaves
                                                                  error:&error];
    if (error == nil) {
        NSMutableDictionary *Info =[ServiceJson objectForKey:@"Information"];
        TGLatLngBounds *bounds = [[TGLatLngBounds alloc] initWithCoordinate: CLLocationCoordinate2DMake([[Info objectForKey:@"maxN"] doubleValue],[[Info objectForKey:@"minE"] doubleValue])
                                                              andCoordinate: CLLocationCoordinate2DMake([[Info objectForKey:@"minN"] doubleValue],[[Info objectForKey:@"maxE"] doubleValue])];
        TGViewerUpdate *update = [TGViewerUpdate fitBounds:bounds];//依照TGLatLngBounds的設置顯示於地圖上
        [mapView_ moveViewer:update];
    }
}


#pragma mark - tgosDelegate
- (BOOL)mapView:(TGMapView *)mapView didTapMarker:(id<TGMarker>)marker
{
    
    NSDictionary *dic = [marker userData];
    
    if (dic == nil ) return NO;
    
    dicNowCenterInfo = dic;
    
    self.lblCenterName.text = [dic objectForKey:@"Name"];
    self.vCenterInfoBG.hidden = NO;
    
    [marker setIcon:[UIImage imageNamed:strMapTapIcon]];//將新選取的marker換成被點選的icon
    
    [mkNowSelected setIcon:[UIImage imageNamed:strMapOriIcon]];//將舊的被選取的marker icon復原
//    [marker setPosition:defaultLocation]; //這樣可以換位置
    mkNowSelected = marker;//將新選取的marker記錄下來
    
    return YES;
}

//當畫面變動時觸發
- (void)mapView:(TGMapView *)mapView didChangeViewerPosition:(TGViewerPosition *)position
{
    
}

//當點擊地圖時觸發
- (void)mapView:(TGMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
    self.vCenterInfoBG.hidden = YES;
    [mkNowSelected setIcon:[UIImage imageNamed:strMapOriIcon]];
}

- (void)mapView:(TGMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - picker

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerRowHeight;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView.tag == 301) {
        return 1;
    }
    else if(pickerView.tag == 302)
    {
        return 2;
    }
    else if(pickerView.tag == 303) //居服地區選擇用
    {
        return 3;
    }
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 301)
    {
        return aryServiceList.count;
    }
    else  if(pickerView.tag == 302)
    {
        if (component == 0)
        {
            return aryCityList.count;
        }
        else
        {
            NSLog(@"目前城市的nCitySelectedIndex為 %i",(int)nCitySelectedIndex);
            NSDictionary *dict = [aryCityList objectAtIndex:nCitySelectedIndex];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];

            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSLog(@"picker取得 %@ 有 %i 個區域",[dict objectForKey:kRegionNo],(int)aryChildRegion.count);
            
            return [aryChildRegion count];
        }
    }
    else if (pickerView.tag == 303)
    {
        if (component == 0)
        {
            return aryAreaType.count;
        }
        else if (component == 1)
        {
            return aryCityList.count;
        }
        else if(component == 2)
        {
            NSLog(@"目前城市的nCitySelectedIndex為 %i",(int)nCitySelectedIndex);
            NSDictionary *dict = [aryCityList objectAtIndex:nCitySelectedIndex];
            
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSLog(@"picker取得 %@ 有 %i 個區域",[dict objectForKey:kRegionNo],(int)aryChildRegion.count);
            
            return [aryChildRegion count] ;
        }

    }
    return aryServiceList.count;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag == 301)
    {
        nServiceType = (int)row;
        [self changeCellTitle:[aryServiceList[row] objectForKey:@"ServiceTypeName"]];
        return [aryServiceList[row] objectForKey:@"ServiceTypeName"];
    }
    else if (pickerView.tag == 302)
    {
        if (component == 0)
        {
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSString *strCity = [dict objectForKey:kRegionName];
            
            return strCity;
        }
        else
        {
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:0]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            return strArea;
        }
    }
    else if (pickerView.tag == 303)
    {
        if (component == 0)
        {
            return aryAreaType[row];
        }
        else if (component == 1)
        {
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];

            NSString *strCity = [dict objectForKey:kRegionName];

            return strCity;
        }
        else
        {
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:1]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            return strArea;
        }
    }
    
    return @"";
}

- (NSDictionary *) makeAllChildRegionDicFromRegionNo:(NSString *) strRegionNo
{
    return @{kRegionNo:[NSNumber numberWithInt:[strRegionNo intValue]],
             kRegionName:@"全區",
             kRegionParentNo:[NSNumber numberWithInt:[strRegionNo intValue]]};
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 301)
    {
        nAreaTypeIndex = -1;
        nServiceType = (int)row;
        [aryServiceItemNo removeAllObjects];
        [self changeCellTitle:[[aryServiceList objectAtIndex:row] objectForKey:@"ServiceTypeName"]];
        
        if ([[[aryServiceList objectAtIndex:row] objectForKey:@"ServiceTypeName"] isEqual:@"居家式"])
        {
            int nTmp = nSpreadCell;
            nSpreadCell = 3;
            
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@",
                                   aryAreaType[0],
                                   strCityArea]];
            nSpreadCell = nTmp;
        }
        else
        {
            int nTmp = nSpreadCell;
            nSpreadCell = 3;
            
            [self changeCellTitle:strCityArea];
            nSpreadCell = nTmp;
        }
    }
    else if(pickerView.tag == 302)
    {
        if (component == 0) {
            NSLog(@"nCitySelectedIndex異動了 %i",(int)row);
            nCitySelectedIndex = (int)row;
            
            [pickerView reloadComponent:1];
            [pickerView selectRow:0 inComponent:1 animated:YES];
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[0] objectForKey:kRegionName];
            strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[0] objectForKey:kRegionNo];
            
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@",strCity,strArea]];
            
        }
        else if(component == 1)
        {
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:0]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[row] objectForKey:kRegionNo];
            
            
            [self changeCellTitle:strCityArea];
            
           
        }
        
    }
    else if (pickerView.tag == 303)
    {
        if (component == 0)
        {
            nAreaTypeIndex =(int) row;
            
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:1]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[[pickerView selectedRowInComponent:2]] objectForKey:kRegionName];
            
            strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[[pickerView selectedRowInComponent:2]] objectForKey:kRegionNo];
            
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@/%@",aryAreaType[row],strCity,strArea]];
        }
        else if (component == 1)
        {
            NSLog(@"nCitySelectedIndex異動了 %i",(int)row);
            nCitySelectedIndex = (int)row;
            
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:2 animated:YES];
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[0] objectForKey:kRegionName];
            
            strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[0] objectForKey:kRegionNo];
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@/%@",aryAreaType[[pickerView selectedRowInComponent:0]],
                                   strCity,strArea]];
            
        }else if(component == 2)
        {
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:1]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[row] objectForKey:kRegionNo];
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@/%@",
                                   aryAreaType[[pickerView selectedRowInComponent:0]],
                                   strCity,strArea]];
            
            
        }
        
       
    }
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if(pickerView.tag == 301)
    {
        return pickerView.frame.size.width;
    }
    else if (pickerView.tag == 303)
    {
        return pickerView.frame.size.width/3;
    }
    
    return pickerView.frame.size.width/2;
}

#pragma mark - 從引導式進來的左btn

- (void)initNewLeftButton
{
    btnNewLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNewLeft setBackgroundImage:[UIImage imageNamed:self.strLeftButtonImageName] forState:UIControlStateNormal];
    [btnNewLeft addTarget:self action:@selector(clickNewLeftButton:) forControlEvents:UIControlEventTouchUpInside];
    btnNewLeft.frame = CGRectMake(10, fNavButtonInterval, fNavButtonWidth, fNavButtonWidth);
    [self.view addSubview:btnNewLeft];
}

- (IBAction) clickNewLeftButton:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - 資料整理

- (NSArray *) getResetServiceType:(NSArray *) aryOri
{
    NSArray *aryAll = aryOri;
    NSMutableArray *ary = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in aryAll)
    {
        if (ary.count == 0)
        {
            [ary addObject:[dic objectForKey:@"ServiceTypeName"]];
        }
        else
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(self CONTAINS %@)", [dic objectForKey:@"ServiceTypeName"]];
            NSArray *aryTmp = [ary filteredArrayUsingPredicate:pred];
            
            if (aryTmp.count == 0) [ary addObject:[dic objectForKey:@"ServiceTypeName"]];
        }
    }
    
    NSMutableArray *aryFinally = [[NSMutableArray alloc] init];
    
    
    for (int i =0; i<ary.count; i++)
    {
        NSString *strType = ary[i];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(ServiceTypeName CONTAINS %@)", strType];
        NSArray *aryTmp = [aryAll filteredArrayUsingPredicate:pred];
        
        NSMutableArray *aryOneTypeItem = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dicTmp in aryTmp)
        {
            [aryOneTypeItem addObject:@{
                                        @"ItemNo":[dicTmp objectForKey:@"ItemNo"],
                                        @"ItemName":[dicTmp objectForKey:@"ItemName"]
                                        }];
            
        }
        
        [aryFinally addObject:@{
                                @"ServiceTypeName":strType,
                                @"Item":aryOneTypeItem
                                }];

    }
    

    
    return aryFinally;
}

- (NSArray *) getRegionCityList
{
    NSString *strPred = [NSString stringWithFormat:@"ParentNo == '-1'"];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"No" ascending:NO];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
    
    return aryTmp;
}

- (NSArray *) getOneRegionListAtParentNo:(NSString *) strParentNo
{
    NSString *strPred = [NSString stringWithFormat:@"ParentNo == '%@'",strParentNo];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"No" ascending:NO];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
    
    NSLog(@"%@ 有 %i 個區域",strParentNo, (int)aryTmp.count);
    return aryTmp;
}

- (NSString *) getRegionAreaNo:(NSString *) strArea CityName:(NSString *) strCity
{
    NSString *strPred = [NSString stringWithFormat:@"Name == '%@'",strArea];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    NSArray *aryCityTmp = [self getRegionCityList];
    
    NSString *strNo = @"-1";
    
    for (int i = 0; i<aryTmp.count; i++)
    {
        NSDictionary *dicAreaTmp =aryTmp[i];
        
        for (int j = 0; j < aryCityTmp.count; j++)
        {
            
            NSString *strCityNoTmp =[aryCityTmp[j] objectForKey:kRegionNo];
            NSString *strAreaCityNoTmp = [dicAreaTmp objectForKey:kRegionParentNo];
            NSString *strCityNameTmp = [aryCityTmp[j] objectForKey:kRegionName];
            
            if ([strAreaCityNoTmp isEqualToString:strCityNoTmp]
                && [strCityNameTmp isEqualToString:strCity])
            {
                strNo = [dicAreaTmp objectForKey:kRegionNo];
            }
            
            
        }
        
    }
    
    return strNo;
}
#pragma mark - SoapTool Delegate

- (void) loadFinishRespose:(NSArray *) resposeData userInfo:(NSString *)UserInfo error:(NSError *)error
{
    if (error)
    {
//        [self HudDismissWithError:error.localizedDescription finishBlock:^{} Title:UserInfo];
        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:UserInfo message:error.localizedDescription delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
        [alertError show];
    }
    else if (resposeData.count == 0)
    {
        if (bFromVcMainLogin)
        {
            if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenterList] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{} Title:nil];

            }
            else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenter] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{
                } Title:nil];
            }
            return;
        }
        else
        {
            if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceSurveyResult] ])
            {
                [self HudDismissWithError:@"無適合的資源" finishBlock:^{
                    [self.navigationController popViewControllerAnimated:YES];
                } Title:nil];

            }
            else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenterList] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{
                } Title:nil];

                return;
            }
            else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenter] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{
                } Title:nil];
                return;
            }
        }
    }
    else
    {
        
        if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenterList] ])
        {
            aryShowInfo = resposeData;
            
            [self HudDismissWithSuccess:@"下載完成" finishBlock:^{
                [self openCondtionBar:NO];
                [self RegionClick:nil];
            }];
            
        }
        else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceSurveyResult] ])
        {
            aryServiceList = [self getResetServiceType:resposeData];
            
            self.vMap.hidden = YES;
            self.tvResult.hidden = NO;
            [self openCondtionBar:YES];
        }
        else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenter] ])
        {
            aryShowInfo = resposeData;
            
            [self HudDismissWithSuccess:@"下載完成" finishBlock:^{
                [self openCondtionBar:NO];
                [self RegionClick:nil];
            }];
        }
        
    }
}
///////////////////////////////////---------------------------------------------------------------------------------------------------------------////////////////////////////////////////



#pragma mark - google api

- (void) startGoogleApi
{
    [GPSHelper getAddressFromCLLocationCoordinate2D:locationUser CompleteBlock:^(bool Success, NSString *ErrMsg, NSDictionary *dicStatus) {
        
        CLLocationCoordinate2D clcLoc;
        
        if(Success == NO)
        {
            strCityArea = @"臺北市/南港區";
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@",@"臺北市",@"南港區"]];
            strRegionAreaNo = [self getRegionAreaNo:@"南港區" CityName:@"臺北市"];
            clcLoc = defaultLocation;
        }
        else
        {
            clcLoc = locationUser;
            
            
            
            NSString *strCityName = [dicStatus objectForKey:kCity];
            NSString *strAreaName = [dicStatus objectForKey:kArea];
            
            strCityName = [strCityName stringByReplacingOccurrencesOfString:@"台" withString:@"臺"];
            strAreaName = [strAreaName stringByReplacingOccurrencesOfString:@"台" withString:@"臺"];
            
            
            strCityArea = [NSString stringWithFormat:@"%@/%@",strCityName,strAreaName];
            
            strRegionAreaNo = [self getRegionAreaNo:strAreaName CityName:strCityName];
            
        }
        
        mapView_.viewer = [TGViewerPosition viewerWithLatitude:clcLoc.latitude
                                                     longitude:clcLoc.longitude
                                                          zoom:10];
        [mapView_ startRendering];
        
        nSpreadCell = 3;
        [self changeCellTitle:strCityArea];
        nSpreadCell = -1;
        
        [mapView_ animateToLocation:clcLoc];
        
        [self HudDissmiss];
        
    }];
    
}

#pragma mark - keyboard from Base

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    tapKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyBoard)];
    tapKeyBoard.delegate = self;
    [self.view addGestureRecognizer:tapKeyBoard];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self clickSearchText:nil];
    return YES;
}

- (UIView*) getKeyBoardCompareView
{
    return self.view;
}

- (NSArray*) getKeyBoardMoveControllerArray
{
    NSMutableArray *aryMoveItem = [NSMutableArray new];
    
    for (UIView *view in self.view.subviews)
    {
        [aryMoveItem addObject:view];
        
    }
    
    return aryMoveItem;
}

- (NSArray*) getKeyBoardTbxArray
{
    NSMutableArray *aryMoveKeyBoard = [NSMutableArray new];
    
    [aryMoveKeyBoard addObject:self.txtSearchTxt];
    
    return aryMoveKeyBoard;
}

#pragma mark - keyboard跳出時將畫面往上推的功能

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([self getKeyBoardCompareView])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillChange:)
                                                     name:UIKeyboardWillChangeFrameNotification object:nil];
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if([self getKeyBoardCompareView])
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillChangeFrameNotification
                                                      object:nil];
    }
}

- (void)initKeyBoardModifier {
    
    if(![self getKeyBoardCompareView]) return;
    
    nKeyBoardInitY = [self getKeyBoardCompareView].frame.origin.y;
    aryKeyBoardMoveControllers = [self getKeyBoardMoveControllerArray];
    aryKeyBoardTxt = [self getKeyBoardTbxArray];
    
}

- (void) closeKeyBoard
{
    UIView* vTmp = [self getNowKeyBoardTxt];
    if(vTmp == nil) return;
    
    [self clickSearchText:nil];
    [self.view removeGestureRecognizer:tapKeyBoard];
    [vTmp resignFirstResponder];
}

- (UIView*) getNowKeyBoardTxt
{
    for (UIView* vTmp in aryKeyBoardTxt) {
        if([vTmp isFirstResponder])
            return  vTmp;
    }
    
    return nil;
}

- (void) keyboardWillChange:(NSNotification *)notification
{
    
    cgKeyBoard = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    cgKeyBoard = [self.view convertRect:cgKeyBoard fromView:nil]; //this is it!
    
//    [self moveKeyboardPosition:cgKeyBoard];
}

#pragma mark - push到其他頁面

- (void) pushToVcCenterInfo
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcCenterInfo* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcCenterInfo"];
    vc.dicCenterInfo = dicNowCenterInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)pushToVcPathPlanning:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcPathPlanning* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcPathPlanning"];
    vc.dicCenterInfo = dicNowCenterInfo;
    [self.navigationController pushViewController:vc animated:YES];
}


@end