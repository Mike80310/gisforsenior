//
//  vcPathPlanning.h
//  GisForSenior
//
//  Created by jane on 2015/7/3.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcPathPlanning : vcBase<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,
                                    UITextFieldDelegate,TGMapViewDelegate,UIWebViewDelegate>
{
    UITapGestureRecognizer* gestTapMask;
    UITapGestureRecognizer* gestTapTv;
}

@property (nonatomic,retain) NSDictionary *dicCenterInfo;

/*
 @"no":@1,
 @"name":@"首都大飯店",
 @"contact":@"負責人",
 @"addr":@"台北市松山區南京東路四段55-1號",
 @"tel":@"02 8712 1988",
 @"url":@"http://www.capital-hotel.com.tw/articles/plist/128/1.html",
 @"LatLng":@{@"Lat":[NSNumber numberWithFloat:25.051963],@"Lng":[NSNumber numberWithFloat:121.552350]},
 @"serviceType":@"服務類型",
 @"serviceContent":@"服務項目"
 */

@property (nonatomic,weak) IBOutlet UIView *vTitleBG;
@property (nonatomic,weak) IBOutlet UILabel *lblCenterName;
@property (nonatomic,weak) IBOutlet UIView *vMap;

@property (nonatomic,weak) IBOutlet UIView *vMask;
@property (nonatomic,weak) IBOutlet UIView *vPathPlanningBG;
@property (nonatomic,weak) IBOutlet UILabel *lblTimeAnDistance;
@property (nonatomic,weak) IBOutlet UIWebView *wvMain;
@property (nonatomic,weak) IBOutlet UITableView *tvMain;

@end
