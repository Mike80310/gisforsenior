//
//  vcPage4.m
//  GisForSenior
//
//  Created by jane on 2015/11/2.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcPage4.h"
#import "vcFilter.h"

#define fAnimationSec 0.5

@interface vcPage4 ()
{
    //cell的layout設定
    int nCellHeight;
    int nCornerRadius;
    int nArrow;
    int nCellInterval;
    
    NSArray *aryADLLevel;
    int nSelectedLevel;
    
    NSMutableArray *aryCount;
    
    UIImage *imgSelected;
    UIImage *imgUnselect;
    
    int nCloseCell;
}
@end

@implementation vcPage4

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.vMask.hidden = YES;
    [self changeVSurveyPostionToTop:NO];
    
    nCloseCell = -1;
    nSelectedLevel = 4;
    aryCount = [[NSMutableArray alloc] init];
    imgSelected = [UIImage imageNamed:@"check01"];
    imgUnselect = [UIImage imageNamed:@"check02"];
    
    CGRect cgBtnYes = self.btnNext.frame;
    cgBtnYes.origin.y -= 10;
    cgBtnYes.size.height = barHeightInPlus;
    self.btnNext.frame = cgBtnYes;
    self.btnNext.layer.cornerRadius = barHeightInPlus/2;
    [self setQuestionLayout];

    nCellHeight = kCellHeight;
    nCornerRadius = kCornerRadius;
    nArrow = kArrow;
    nCellInterval = kCellInterval;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        nCellHeight +=15;
        nCornerRadius += 8;
        nArrow += 10;
        nCellInterval += kCellInterval;
    }
    
    aryADLLevel = @[
                    @{@"LevelNo":@"1",@"LevelName":@"輕度失能"},
                    @{@"LevelNo":@"2",@"LevelName":@"中度失能"},
                    @{@"LevelNo":@"3",@"LevelName":@"重度失能"},
                    @{@"LevelNo":@"0",@"LevelName":@"非失能者"},
                    ];
}

- (void) setQuestionLayout
{
    self.vQuestion.layer.cornerRadius = 5;
    CGRect cgBtnYes = self.btnConfirm.frame;
    cgBtnYes.origin.y -= 10;
    cgBtnYes.size.height = barHeightInPlus;
    self.btnConfirm.frame = cgBtnYes;
    self.btnConfirm.layer.cornerRadius = barHeightInPlus/2;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section ==0) return 1;
    if (nCloseCell == -1)
    {
        return aryADLLevel.count + 2;
    }
    else
    {
        if (nCloseCell == section) return 1;
        
        if (section == 1)
        {
            return aryADLLevel.count + 2;
        }
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *CellIdentifier;
    
    int nSection = (int)indexPath.section;
    int nRow = (int)indexPath.row;
    
    if (nSection == 0)
    {
        CellIdentifier = [NSString stringWithFormat:@"TitleCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
        vContent.backgroundColor = [UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if (nSection == 1)
    {
        if (nRow == 0)
        {
            CellIdentifier = [NSString stringWithFormat:@"adlTitleCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"請問你的基本日常生活能力(ADL)"];
        }
        else if(nRow > aryADLLevel.count)
        {
            CellIdentifier = [NSString stringWithFormat:@"QuestionCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            
            UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
            CGRect cgTmp = vContent.frame;
            cgTmp.origin.y = 0;
            cgTmp.size.height = nCellHeight;
            cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
            vContent.frame = cgTmp;
            vContent.backgroundColor = [UIColor whiteColor];
            
            
            UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
            
            lblTitleName.font = [UIFont systemFontOfSize:fFontSize];
            [lblTitleName sizeToFit];
            CGRect cgTitle = lblTitleName.frame;
            cgTitle.origin.x = (cgTmp.size.width - cgTitle.size.width) /2;
            
            if (cgTitle.origin.x < 1)
            {
                cgTitle.origin.x = 30;
            }
            cgTitle.origin.y = (cgTmp.size.height - cgTitle.size.height)/2;
            
            [lblTitleName setFrame:cgTitle];

            UILabel *lblUnderLine = (UILabel *) [cell.contentView viewWithTag:102];
            cgTitle.origin.y += cgTitle.size.height;
            cgTitle.size.height = 1;
            lblUnderLine.frame = cgTitle;
        }
        else
        {
            CellIdentifier = [NSString stringWithFormat:@"adlItemCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            [self setItemCell:cell cellForRowAtIndexPath:indexPath];
        }
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return nCellHeight/2;
    }
    return nCellHeight;
}

#pragma mark - 各式Cell
- (UITableViewCell *) setTitleCell:(UITableViewCell *)cell
             cellForRowAtIndexPath:(NSIndexPath *)indexPath titleName:(NSString *) strName
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    vContent.layer.cornerRadius = nCornerRadius;
    vContent.layer.masksToBounds = YES;
    vContent.layer.borderWidth = 1;
    vContent.layer.borderColor = [self colorFromHexString:kBorderColorID].CGColor;
    
    UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
    lblTitleName.text = strName;
    
    UIImageView *imgArrow = (UIImageView *) [cell.contentView viewWithTag:102];
    
    CGRect cgImg = imgArrow.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    cgImg.origin.y = (nCellHeight - nArrow) /2;
    cgImg.origin.x = cgTmp.size.width - nArrow*4/3;
    imgArrow.frame = cgImg;
    
    CGRect cgTitle = lblTitleName.frame;
    cgTitle.origin.y = cgTmp.origin.y;
    cgTitle.origin.x = cgTmp.origin.x;
    //    cgTitle.size.width = cgTmp.size.width - (cgTmp.size.width - cgImg.origin.x);
    cgTitle.size.height = cgTmp.size.height;
    lblTitleName.frame = cgTitle;
    lblTitleName.font = [UIFont systemFontOfSize:fFontSize];
    
    return cell;
}

- (UITableViewCell *) setItemCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    
    int nRow = (int)indexPath.row;
    
    UIImageView* imgSelect = (UIImageView *) [cell.contentView viewWithTag:101];
    CGRect cgImg = imgSelect.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    imgSelect.frame = cgImg;
    
    UILabel *lblItemName = (UILabel *) [cell.contentView viewWithTag:102];
    CGRect cgLabel = lblItemName.frame;
    cgLabel.size.height = nCellHeight;
    cgLabel.origin.x = cgImg.origin.x + cgImg.size.width + 5;
    cgLabel.origin.y = 0;
    lblItemName.frame = cgLabel;
    [lblItemName setFont:[UIFont systemFontOfSize:fFontSize]];
    
    NSString *strItemName = [[aryADLLevel objectAtIndex:nRow-1] objectForKey:@"LevelName"];
    [lblItemName setText:strItemName];
    
    int nItemNo = [[[aryADLLevel objectAtIndex:nRow-1] objectForKey:@"LevelNo"] intValue];
    cell.tag = nItemNo;
    
    BOOL bIsSelected = false;
    
    bIsSelected = (indexPath.row == nSelectedLevel);
    
    if (bIsSelected)
    {
        [imgSelect setImage:imgSelected];
    }
    else
    {
        [imgSelect setImage:imgUnselect];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0)
    {
        if (nCloseCell == -1)
        {
            nCloseCell = (int)indexPath.section;
            [self deleteCell:indexPath amount:(int)aryADLLevel.count +2];
        }
        else
        {
             [self SpreadClassCell:indexPath];
        }
       
        
    }
    
    if (indexPath.section == 1 && indexPath.row != 0 && indexPath.row <= aryADLLevel.count)
    {
        nSelectedLevel = (int)indexPath.row;
        [self.tvMain reloadData];
    }
    else if (indexPath.section == 1 && indexPath.row > aryADLLevel.count)
    {
        [self changeVSurveyPostionToTop:YES];
    }
    
    
}


#pragma mark - 移動vSurvey

- (void) changeVSurveyPostionToTop:(BOOL) bIsUp
{
    float fNewY;
    
    if (bIsUp)
    {
        if (self.vMask.hidden)
        {
            self.vMask.hidden = NO;
        }
        
        
        fNewY = 0;
        [self.view bringSubviewToFront:self.vMask];
    }
    else
    {
        fNewY = screenSize.height;
    }
    
    [UIView animateWithDuration:fAnimationSec
                     animations:^{
                         
                         self.vMask.transform = CGAffineTransformMakeTranslation(0, fNewY);
                         
                         if (fNewY == 0)
                         {
                             self.vMask.backgroundColor = clrMaker(0, 0, 0, 0.5);
                         }
                         else
                         {
                             self.vMask.backgroundColor = [UIColor clearColor];
                         }
                     }
     ];
    
    
}

- (IBAction) clickConfirm:(id)sender
{
    
    if (aryCount.count == 0) //非失能者
    {
        nSelectedLevel = 4;
    }
    else if(aryCount.count <= 2) //輕度失能
    {
        nSelectedLevel = 1;
    }
    else if(aryCount.count <= 4) //中度失能
    {
        nSelectedLevel = 2;
    }
    else //重度失能
    {
        nSelectedLevel = 3;
    }
    
    [self.tvMain reloadData];
    
    [self changeVSurveyPostionToTop:NO];
}

- (IBAction) writeQuestion:(id)sender
{
    UIButton *btnClicked = (UIButton *) sender;
    
    if ([btnClicked isEqual:self.btnNO1])
    {
        [aryCount removeObject:self.btnYES1];
        [self.btnYES1 setImage:imgUnselect forState:UIControlStateNormal];
        [self.btnNO1 setImage:imgSelected forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnYES1])
    {
        [aryCount addObject:self.btnYES1];
        [self.btnYES1 setImage:imgSelected forState:UIControlStateNormal];
        [self.btnNO1 setImage:imgUnselect forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnNO2])
    {
        [aryCount removeObject:self.btnYES2];
        [self.btnYES2 setImage:imgUnselect forState:UIControlStateNormal];
        [self.btnNO2 setImage:imgSelected forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnYES2])
    {
        [aryCount addObject:self.btnYES2];
        [self.btnYES2 setImage:imgSelected forState:UIControlStateNormal];
        [self.btnNO2 setImage:imgUnselect forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnNO3])
    {
        [aryCount removeObject:self.btnYES3];
        [self.btnYES3 setImage:imgUnselect forState:UIControlStateNormal];
        [self.btnNO3 setImage:imgSelected forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnYES3])
    {
        [aryCount addObject:self.btnYES3];
        [self.btnYES3 setImage:imgSelected forState:UIControlStateNormal];
        [self.btnNO3 setImage:imgUnselect forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnNO4])
    {
        [aryCount removeObject:self.btnYES4];
        [self.btnYES4 setImage:imgUnselect forState:UIControlStateNormal];
        [self.btnNO4 setImage:imgSelected forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnYES4])
    {
        [aryCount addObject:self.btnYES4];
        [self.btnYES4 setImage:imgSelected forState:UIControlStateNormal];
        [self.btnNO4 setImage:imgUnselect forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnNO5])
    {
        [aryCount removeObject:self.btnYES5];
        [self.btnYES5 setImage:imgUnselect forState:UIControlStateNormal];
        [self.btnNO5 setImage:imgSelected forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnYES5])
    {
        [aryCount addObject:self.btnYES5];
        [self.btnYES5 setImage:imgSelected forState:UIControlStateNormal];
        [self.btnNO5 setImage:imgUnselect forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnNO6])
    {
        [aryCount removeObject:self.btnYES6];
        [self.btnYES6 setImage:imgUnselect forState:UIControlStateNormal];
        [self.btnNO6 setImage:imgSelected forState:UIControlStateNormal];
    }
    else if ([btnClicked isEqual:self.btnYES6])
    {
        [aryCount addObject:self.btnYES6];
        [self.btnYES6 setImage:imgSelected forState:UIControlStateNormal];
        [self.btnNO6 setImage:imgUnselect forState:UIControlStateNormal];
    }
}

- (IBAction)pushToNextQuestion:(id)sender
{
    if (nSelectedLevel == -1)
    {
        [self HudErrMsgWithError:@"尚未選擇日常生活能力" finishBlock:^(void){
            
        }];
        
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:@([[aryADLLevel[nSelectedLevel-1] objectForKey:@"LevelNo"]intValue]) forKey:kDicKeyAdlLevel];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcFilter* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcFilter"];
    vc.strPrePage = @"vcQuestion";
    vc.title = @"我符合的機構";
    [self.navigationController pushViewController:vc animated:YES];
}

-(UILabel *)setLabelUnderline:(UILabel *)label
{
//    CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:label.frame.size lineBreakMode:label.lineBreakMode];
 
    
    NSDictionary *attribute = @{NSFontAttributeName: label.font};
    CGSize expectedLabelSize = [label.text boundingRectWithSize:label.frame.size options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
//    CGSize expectedLabelSize = [label.text boundingRectWithSize:label.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:label.font} context:nil].size;
    
    UIView *viewUnderline=[[UIView alloc] init];
    CGFloat xOrigin=0;
    switch (label.textAlignment) {
        case NSTextAlignmentCenter:
            xOrigin=(label.frame.size.width - expectedLabelSize.width)/2;
            break;
        case NSTextAlignmentLeft:
            xOrigin=0;
            break;
        case NSTextAlignmentRight:
            xOrigin=label.frame.size.width - expectedLabelSize.width;
            break;
        default:
            break;
    }
    viewUnderline.frame=CGRectMake(xOrigin,
                                   expectedLabelSize.height + 7, //這邊可以調整底線的高低
                                   expectedLabelSize.width,
                                   1);
    viewUnderline.backgroundColor=label.textColor;
    [label addSubview:viewUnderline];
    
    return label;
    
}


#pragma mark - cell的展開與關閉

- (void) SpreadClassCell:(NSIndexPath *)indexPath
{
    
    [self imageRotateAction:NO indexPath:indexPath];//NO是展開
    
    nCloseCell = -1;
    if (indexPath.section ==1)
    {
        int nTmp = (int)aryADLLevel.count + 2;
        [self creatCell:indexPath amount:nTmp];
    }
    else
    {
        [self creatCell:indexPath amount:1];
    }
    

}

- (void) deleteCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    [self imageRotateAction:YES indexPath:indexPath];
    NSMutableArray* aryDelete = [NSMutableArray new];
    if(nAmount == 1)
    {
        [aryDelete addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryDelete addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvMain beginUpdates];
    [self.tvMain deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    [aryDelete removeAllObjects];
}

- (void) creatCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    NSMutableArray* aryInsert = [NSMutableArray new];
    
    if(nAmount == 1)
    {
        [aryInsert addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryInsert addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvMain beginUpdates];
    [self.tvMain insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    
    [aryInsert removeAllObjects];
}

#pragma mark - Arrow動畫

- (void) imageRotateAction:(BOOL)isRotate indexPath:(NSIndexPath *) indexPath
{
    UITableViewCell *cell = [self.tvMain cellForRowAtIndexPath:indexPath];
    
    for (UIView *v100 in cell.contentView.subviews)
    {
        
        for(UIView *vTmp in [v100 subviews])
        {
            if (![vTmp isKindOfClass:[UIImageView class]]) continue;
            
            UIImageView *imgArrow = (UIImageView *)vTmp;
            
            [UIView animateWithDuration:.35
                             animations:^
             {
                 if (isRotate)
                 {
                     imgArrow.transform = CGAffineTransformIdentity;
                     
                 }else
                 {
                     imgArrow.transform = CGAffineTransformMakeRotation(180 * M_PI / 180);//M_PI_2 轉四分之一
                 }
                 
             }
                             completion:^(BOOL finished)
             {
                 
             }];
            
        }
        
    }
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
