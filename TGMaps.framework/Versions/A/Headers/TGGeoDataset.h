//
//  GeoDataset.h
//  Copyright (c) 2012年 supergeo.rd@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>



@class TGEnvelope;
@class MySpatialReference;

@protocol ISpatialReference;

@interface TGGeoDataset : NSObject
{
    TGEnvelope *_Envelope;
}

@property (nonatomic, retain) id <ISpatialReference> SpatialReference;

- (TGEnvelope*)GetEnvelope;

- (TGEnvelope*)Extent:(MySpatialReference*) spaRef;

@end
