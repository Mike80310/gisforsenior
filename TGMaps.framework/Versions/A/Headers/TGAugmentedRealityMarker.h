//
//  AugmentedRealityMarker.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/5/1.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class UIColor;
@class UIImage;
@protocol TGAugmentedRealityMarker

@property (nonatomic, assign) CLLocationCoordinate2D position;

/** This property is observable using KVO. */
@property (nonatomic, readonly) CGFloat distance;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) UIImage *icon;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, strong) id userData;

@property (nonatomic, assign) CGSize size;

- (void)remove;

@end
