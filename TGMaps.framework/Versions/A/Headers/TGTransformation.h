//
//  TGTransformation.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/6/5.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class TGPointD;

@interface TGTransformation : NSObject

+ (TGPointD *)wgs84ToTWD97:(CLLocationCoordinate2D)coord;

+ (CLLocationCoordinate2D)twd97ToWGS84:(TGPointD *)point;

+ (TGPointD *)wgs84ToTWD97_119:(CLLocationCoordinate2D)coord;

+ (CLLocationCoordinate2D)twd97_119ToWGS84:(TGPointD *)point;

@end
