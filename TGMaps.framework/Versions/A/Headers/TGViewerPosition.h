//
//  TGOSCameraPosition.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/14.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import <CoreLocation/CoreLocation.h>

/**
 * An immutable class that aggregates all viewer position parameters.
 */
@interface TGViewerPosition : NSObject

+ (TGViewerPosition *)viewerWithTarget:(CLLocationCoordinate2D)target
                                   zoom:(CGFloat)zoom;

+ (TGViewerPosition *)viewerWithLatitude:(CLLocationDegrees)latitude
                                longitude:(CLLocationDegrees)longitude
                                     zoom:(CGFloat)zoom;

@property (nonatomic, readonly, getter=targetAsCoordinate) CLLocationCoordinate2D target;

@property (nonatomic, readonly) CGFloat zoom;

@property (nonatomic, readonly) CGFloat scale;

@end

/**
 * The maximum zoom (closest to the Earth's surface) permitted by the map
 */
FOUNDATION_EXTERN const CGFloat kTGMaxZoomLevel;

/**
 * The minimum zoom (farthest from the Earth's surface) permitted by the map
 */
FOUNDATION_EXTERN const CGFloat kTGMinZoomLevel;

