//
//  TGOSMapView.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/2.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "TGUISettings.h"

@class TGMapView;
@class TGViewerPosition;
@class TGViewerUpdate;
@class TGProjection;
@class TGCircleOptions;
@class TGPolylineOptions;
@class TGPolygonOptions;
@class TGMarkerOptions;
@class TGGroundOverlayOptions;
@class TGMapGPSDisplay;
@class TGMapTool;
@class TGWMSMapLayer;

@protocol TGCircle;
@protocol TGPolyline;
@protocol TGPolygon;
@protocol TGMarker;
@protocol TGGroundOverlay;


@protocol TGMapViewDelegate <NSObject>

@optional

- (void)mapView:(TGMapView *)mapView
    didChangeViewerPosition:(TGViewerPosition *)position;

- (void)mapView:(TGMapView *)mapView
    didTapAtCoordinate:(CLLocationCoordinate2D)coordinate;

- (void)mapView:(TGMapView *)mapView
    didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate;

- (BOOL)mapView:(TGMapView *)mapView
    didTapMarker:(id<TGMarker>)marker;

- (void)mapView:(TGMapView *)mapView
    didTapInfoWindowOfMarker:(id<TGMarker>)marker;

- (void)mapView:(TGMapView *)mapView
    didMarkerDrag:(id<TGMarker>)marker;

- (void)mapView:(TGMapView *)mapView
    didMarkerDragStart:(id<TGMarker>)marker;

- (void)mapView:(TGMapView *)mapView
    didMarkerDragEnd:(id<TGMarker>)marker;

/**
 * Called when a marker is about to become selected, and provides an optional
 * custom info window to use for that marker if this method returns a UIView.
 * If you change this view after this method is called, those changes will not
 * necessarily be reflected in the rendered version.
 *
 * The returned UIView must not have bounds greater than 500 points on either
 * dimension.  As there is only one info window shown at any time, the returned
 * view may be reused between other info windows.
 *
 * @return The custom info window for the specified marker, or nil for default
 */
- (UIView *)mapView:(TGMapView *)mapView
    markerInfoWindow:(id<TGMarker>)marker;

@end


typedef enum {
    /** TGOS地圖. */
    kMapTypeTGOSMap = 1,
    
    /** 通用版電子地圖. */
    kMapTypeNLSCMap,
    
    /** 混合地圖. */
    kMapTypeRoadMap,
    
    /** 福衛二號衛星影像. */
    kMapTypeF2Image,
    
} TGMapType;


@interface TGMapView : UIView

@property (nonatomic, weak) id<TGMapViewDelegate> delegate;

@property (nonatomic, strong) TGViewerPosition *viewer;

@property (nonatomic, readonly) TGUISettings *settings;

@property (nonatomic, readonly) TGProjection *projection;

@property (nonatomic, strong, readonly) CLLocation *myLocation;

@property (nonatomic, strong) id<TGMarker> selectedMarker;

@property (nonatomic, assign) TGMapType mapType;

@property (nonatomic, readonly) TGMapTool *mapTool;

@property (nonatomic, strong) UIView *loadingMessageView;

+ (TGMapView *)mapWithFrame:(CGRect)frame
                       viewer:(TGViewerPosition *)viewer;

- (void)startRendering;

- (void)stopRendering;

- (void)moveViewer:(TGViewerUpdate *)viewerUpdate;

/**
 * Animates the camera of this map to |cameraPosition|. During the animation,
 * retrieving the camera position through the camera property returns an
 * intermediate immutable TGViewerPosition.
 *
 * The duration of this animation, and those below (animateToLocation:,
 * animateToZoom:, animateToBearing: and animateToViewingAngle:) is controlled
 * by Core Animation.
 */
- (void)animateToViewerPosition:(TGViewerPosition *)viewerPosition;

- (void)animateToLocation:(CLLocationCoordinate2D)location;

- (void)animateToZoom:(CGFloat)scale;

/**
 * Overlays
 */
- (id<TGCircle>)addCircleWithOptions:(TGCircleOptions *)options;

- (NSArray *)circles;

- (id<TGPolyline>)addPolylineWithOptions:(TGPolylineOptions *)options;

- (NSArray *)polylines;

- (id<TGPolygon>)addPolygonWithOptions:(TGPolygonOptions *)options;

- (NSArray *)polygons;

- (id<TGMarker>)addMarkerWithOptions:(TGMarkerOptions *)options;

- (NSArray *)markers;

- (id<TGGroundOverlay>)addGroundOverlayWithOptions:(TGGroundOverlayOptions *)options;

- (NSArray *)groundOverlays;

- (void)clear;

/**
 * GPS settings
 */
- (void)addMapGPSDisplay:(TGMapGPSDisplay* )mapGPSDisplay;

/**
 * WMS
 */
- (void)addMapLayer:(TGWMSMapLayer *)wmsMapLayer;

- (void)addMapLayer:(TGWMSMapLayer *)wmsMapLayer withName:(NSString *)name;

- (void)insertMapLayer:(TGWMSMapLayer *)wmsMapLayer atIndex:(NSUInteger)index;

- (void)insertMapLayer:(TGWMSMapLayer *)wmsMapLayer withName:(NSString *)name atIndex:(NSUInteger)index;

- (void)removeMapLayer:(TGWMSMapLayer *)wmsMapLayer;

- (void)removeMapLayerWithName:(NSString *)name;

@end
