//
//  vcBase.m
//  GisForSenior
//
//  Created by jane on 2015/7/2.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

#define kPvDefaultWidth 250

@interface vcBase ()
{
    int nTitleFontSize;
    
    
}
@end

@implementation vcBase

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    defaultLocation = CLLocationCoordinate2DMake(25.050299, 121.595442);
    
    gH = [GPSHelper new];
    
    screenSize = [[UIScreen mainScreen] bounds].size;
    
    float fNavButtonWidth = 45;
    float fNavButtonInterval = 22;
    
    fFontSize = 18;
    
    nPvWidth = kPvDefaultWidth;
    
    if (screenSize.height == kI6ScreenHeight)
    {
        fNavButtonWidth += 5;
        fFontSize = 20;
        nPvWidth = 305;
        
    }
    else if(screenSize.height > kI6ScreenHeight)
    {
        fNavButtonWidth += 10;
        fFontSize = 20;
        nPvWidth = 344;
    }
    
    nPv2Width = nPvWidth/2;
    nPv3Width = nPvWidth/3;
    
    nTitleFontSize = fFontSize;
    
    [self initLeftButton:fNavButtonWidth fNavButtonInterval:fNavButtonInterval];
    [self initRightButton:fNavButtonWidth fNavButtonInterval:fNavButtonInterval];
    
    btnRight.hidden = YES;
    
    [self initTopTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initLeftButton:(float)fNavButtonWidth fNavButtonInterval:(float)fNavButtonInterval {
    btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLeft setBackgroundImage:[UIImage imageNamed:self.strLeftButtonImageName] forState:UIControlStateNormal];
    [btnLeft addTarget:self action:@selector(clickLeftButton:) forControlEvents:UIControlEventTouchUpInside];
    btnLeft.frame = CGRectMake(10, fNavButtonInterval, fNavButtonWidth, fNavButtonWidth);
    [self.view addSubview:btnLeft];
}

- (void)initRightButton:(float)fNavButtonWidth fNavButtonInterval:(float)fNavButtonInterval {
    btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setBackgroundImage:[UIImage imageNamed:self.strRightButtonImageName] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(clickRightButton:) forControlEvents:UIControlEventTouchUpInside];
    btnRight.frame = CGRectMake(self.view.frame.size.width - 10 - fNavButtonWidth, fNavButtonInterval, fNavButtonWidth, fNavButtonWidth);
    [self.view addSubview:btnRight];
}

- (NSString*) strLeftButtonImageName
{
    return @"btn_return";
}

- (NSString*) strRightButtonImageName
{
    return @"btn_map";
}

- (void) clickLeftButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) clickRightButton:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - TopBar

- (void) setTitle:(NSString *)title
{
    [super setTitle:title];
    
    if(lblTitle)
        lblTitle.text = title;
}

- (void)initTopTitle
{
    
    CGRect cgTitle = CGRectMake(btnLeft.frame.origin.x, btnLeft.frame.origin.y,
                                btnRight.frame.origin.x + btnRight.frame.size.width - btnLeft.frame.origin.x,
                                btnLeft.frame.size.height );
    lblTitle = [[UILabel alloc] initWithFrame:cgTitle];
    lblTitle.text = self.title;
    lblTitle.numberOfLines = 2;
    lblTitle.adjustsFontSizeToFitWidth = YES;
    lblTitle.textColor = [UIColor blackColor];
    [lblTitle setFont:[UIFont systemFontOfSize:fFontSize+5]];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [lblTitle setMinimumScaleFactor:0.5f];
    
    [self.view addSubview:lblTitle];
}

#pragma mark - GPSHelper

- (void) getNowAddress
{
    
/*
    因客戶需求, 因此註解--------------------------------------------------
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied ||
        [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
    {
        [self setNowLocation:defaultLocation];
    }
    因客戶需求, 因此註解--------------------------------------------------
*/
    [gH startGetGPSWithOKBlock:^{
        [gH stopGetGPS];
        
        [self setNowLocation:gH.GPS];
        
        
    } FailBlock:^{
        [gH stopGetGPS];
//        NSLog(@"GPS FAIL");
//        [self setNowLocation:defaultLocation];    因客戶需求, 因此註解--------------------------------------------------
    }];
}

- (void) stopUpdateCurrentLocation
{
    //    [locationManager stopUpdatingLocation];
}

- (void) setNowLocation:(CLLocationCoordinate2D) result
{
    
    NSNumber *lat;
    NSNumber *lng;
    
    NSLog(@"latitude = %f", result.latitude);
    NSLog(@"longitude = %f", result.longitude);

    
    //因客戶需求 而註解掉
//    if (result.latitude == 0 && result.longitude == 0 )
//    {
//        NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserLatLng];
//        
//        if (dic == nil)
//        {
//            lat = [NSNumber numberWithDouble:defaultLocation.latitude];
//            lng = [NSNumber numberWithDouble:defaultLocation.longitude];
//            NSDictionary *userLocation = @{@"Lat":lat,@"Lng":lng};
//            [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:kDefaultUserLatLng];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//        }
//
//    }
//    else
//    {
//        lat = [NSNumber numberWithDouble:result.latitude];
//        lng = [NSNumber numberWithDouble:result.longitude];
//        NSDictionary *userLocation = @{@"Lat":lat,@"Lng":lng};
//        [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:kDefaultUserLatLng];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//    }
    
    //新增下列程式
    if (result.latitude == 0 && result.longitude == 0 ) {
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDefaultUserLatLng];
        
    }
    else{
        
        lat = [NSNumber numberWithDouble:result.latitude];
        lng = [NSNumber numberWithDouble:result.longitude];
        NSDictionary *userLocation = @{@"Lat":lat,@"Lng":lng};
        [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:kDefaultUserLatLng];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kFirstOpenFeedBack] == nil)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kFirstOpenFeedBack object:nil];
        }

    }
    
    
    
}


#pragma mark - 年份轉換

- (NSDateComponents*) getTaiwanDateComponentsWithyyyyMMdd:(NSString*)yyyyMMdd
{
    return [self getTaiwanDateComponentsWithDateString:yyyyMMdd Format:@"yyyyMMdd"];
}

- (NSDateComponents*) getTaiwanDateComponentsWithDateString:(NSString*)strDate Format:(NSString*)strFmt
{
    NSDateFormatter* fmt = [NSDateFormatter new];
    fmt.dateFormat = strFmt;
    
    NSDate* datTmp = [fmt dateFromString:strDate];
    
    NSCalendar *taiwanCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSRepublicOfChinaCalendar];
    NSDateComponents *cmt = [taiwanCalendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:datTmp];
    cmt.calendar = taiwanCalendar;
    cmt.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    return cmt;
}

- (NSDate *) getGregorianDate:(NSString*) strDate
{
    NSDateFormatter* fmt = [NSDateFormatter new];
    fmt.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    fmt.dateFormat = @"yyyy/MM/dd";
    fmt.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSRepublicOfChinaCalendar];
    
    return [fmt dateFromString:strDate];
}

#pragma mark - Hud

- (void) HudShowMsg:(NSString*)Msg
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithStatus:Msg];
}

- (void) HudDebugMsg:(NSString*)Msg OKMsg:(NSString*)okMsg okBlock:(void(^)(void))block
{
    [self HudShowMsg:Msg];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self HudDismissWithSuccess:okMsg finishBlock:block];
    });
}

- (void) HudDebugMsg:(NSString*)Msg ErrMsg:(NSString*)errMsg okBlock:(void(^)(void))block
{
    [self HudShowMsg:Msg];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self HudErrMsgWithError:errMsg finishBlock:block];
    });
}

- (void) HudDissmiss
{
    [MMProgressHUD dismiss];
}

- (void) HudErrMsgWithError:(NSString*)ErrMSg finishBlock:(void(^)(void))block
{
    [self HudErrMsgWithError:ErrMSg Title:nil finishBlock:block];
}

- (void) HudDismissWithSuccess:(NSString*)Msg finishBlock:(void(^)(void))block
{
    [self HudDismissWithSuccess:Msg finishBlock:block Title:nil];
}

- (void) HudErrMsgWithError:(NSString*)ErrMSg Title:(NSString*)title finishBlock:(void(^)(void))block
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithTitle:title status:ErrMSg];
    
    [self HudDismissWithError:ErrMSg finishBlock:block Title:title];
}


- (void) HudDismissWithError:(NSString*)ErrMsg finishBlock:(void(^)(void))block Title:(NSString*)title
{
    [MMProgressHUD dismissWithError:ErrMsg title:title afterDelay:fDelayOfErrMsg];
    
    if(block == nil) return;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, fDelayOfErrMsg * NSEC_PER_SEC * 1.1), dispatch_get_main_queue(), ^{
        block();
    });
}

- (void) HudDismissWithSuccess:(NSString*)Msg finishBlock:(void(^)(void))block Title:(NSString*)title
{
    [MMProgressHUD dismissWithSuccess:Msg title:title afterDelay:fDelayOfOKMsg];
    if(block == nil) return;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(fDelayOfOKMsg * NSEC_PER_SEC) * 1.1), dispatch_get_main_queue(), ^{
        block();
    });
}

-(void)resizeHeightForLabel: (UILabel*)label vFrame:(CGRect) cgFrame
{
    int nTitleWidth;
    int nTitleHeight;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        nTitleWidth = cgFrame.size.width - 10;
        nTitleHeight = cgFrame.size.height + 10;
    }
    else
    {
        nTitleWidth = cgFrame.size.width - 10;
    }
    
    label.numberOfLines = 0;
    CGRect labelFrame = label.frame;
    
    
    CGRect expectedFrame = [label.text boundingRectWithSize:CGSizeMake(nTitleWidth, CGFLOAT_MAX)//
                                                    options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                 attributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             label.font, NSFontAttributeName,nil]
                                                    context:nil];
    
    
    
    
    labelFrame.size = expectedFrame.size;
    labelFrame.size.height = ceil(labelFrame.size.height); //iOS7 is not rounding up to the nearest whole number
    
    if (labelFrame.size.height > nTitleHeight)
    {
        nTitleFontSize--;
        label.font = [UIFont boldSystemFontOfSize:nTitleFontSize];
        [self resizeHeightForLabel:label vFrame:cgFrame];
        return;
    }
    
    nTitleFontSize = fFontSize;
    labelFrame.origin.x = cgFrame.size.width/2 - labelFrame.size.width/2;
    labelFrame.origin.y = cgFrame.size.height/2 - labelFrame.size.height/2;
    
    if(labelFrame.size.width == 0)
    {
        labelFrame.origin.x = 15;
        labelFrame.origin.y = 0;
        labelFrame.size.width = 250;
        labelFrame.size.height = 45;
    }
    
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"x %f y %f width %f height %f",labelFrame.origin.x,labelFrame.origin.y,labelFrame.size.width,labelFrame.size.height] delegate:nil cancelButtonTitle:@"確定" otherButtonTitles: nil];
//    [alert show];
    label.frame = labelFrame;
    
}

-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
