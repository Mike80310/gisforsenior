//
//  ViewController.h
//  GisForSenior
//
//  Created by jane on 2015/7/1.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"
#import <TGMaps/TGMaps.h>
#import <QuartzCore/QuartzCore.h>
#import "SoapTool.h"

@interface vcMapMain : vcBase<UITableViewDataSource,UITableViewDelegate,
                                TGMapViewDelegate,UIGestureRecognizerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,SOAPToolDelegate>
{
    UITapGestureRecognizer* gestCenterInfoTap;
}

@property (nonatomic,retain) NSString *strPrePage;

@property (nonatomic,weak) IBOutlet UIView *vSearchTool;
@property (nonatomic,weak) IBOutlet UIView *vSearchBar;
@property (nonatomic,weak) IBOutlet UITextField *txtSearchTxt;
@property (nonatomic,weak) IBOutlet UIButton *btnSearch;
//@property (nonatomic,weak) IBOutlet UIView *vCondition;
//@property (nonatomic,weak) IBOutlet UIButton *btnCondition;
@property (nonatomic,weak) IBOutlet UITableView *tvConditionList;

@property (nonatomic,weak) IBOutlet UITableView *tvResult;

@property (nonatomic,weak) IBOutlet UIView *vMap;
@property (nonatomic,weak) IBOutlet UIView *vCenterInfoBG;
@property (nonatomic,weak) IBOutlet UIView *vCenterInfo;
@property (nonatomic,weak) IBOutlet UILabel *lblCenterName;
@property (nonatomic,weak) IBOutlet UIButton *btnPathPlanning;

@end

