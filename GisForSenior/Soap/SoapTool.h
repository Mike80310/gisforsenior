//
//  SoapTool.h
//  GisForSenior
//
//  Created by jane on 2015/7/28.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark protocol
@protocol SOAPToolDelegate

- (void) loadFinishRespose:(NSArray *) resposeData userInfo:(NSString *)UserInfo error:(NSError *) error;

@end
#pragma end

@interface SoapTool : NSObject<NSXMLParserDelegate>
{
    
}
@property (retain,nonatomic) id <SOAPToolDelegate> delegate;

- (void) getBasicData:(NSString *) strServiceName;
- (void) getCareCenterData:(NSArray *) aryServiceItemNo RegionNo:(NSString *) strRegionAreaNo IsHomeServiceRegion:(BOOL) IsHomeServiceRegion CenterName:(NSString *) strSearchText;
- (void) getSurveyResult:(BOOL) IsDisabilities IsAborigines:(BOOL) IsAborigines Age:(int) nAge CareTime:(int) nCareTime Intubation:(int)nIntubation  adlLevel:(int) nLevel;
- (void) searchCareCenterName:(NSString *) strSearchText;
- (void) getCareCenterByCenterID:(NSString *) strCareCenterID;
@end
