//
//  TGMapTool.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/6/6.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TGMapTool : NSObject

@property (nonatomic, assign) CGPoint origin;

@property (nonatomic, assign) BOOL zoomButton;

@property (nonatomic, assign) BOOL fullMapButton;

@end
