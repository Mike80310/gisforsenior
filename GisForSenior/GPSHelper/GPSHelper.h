//
//  GPSHelper.h
//  WinGuide
//
//  Created by CHX on 2015/5/4.
//  Copyright (c) 2015年 CHX. All rights reserved.
//

#import <MapKit/MapKit.h>

typedef void (^GpsCompleteBlock)(bool Success, NSString* ErrMsg, NSDictionary* dicStatus);

@interface GPSHelper : NSObject <CLLocationManagerDelegate>
{
    CLLocationManager* manager;
}

@property (nonatomic, readonly) CLLocationCoordinate2D GPS;
@property (nonatomic, readonly) NSDate* datLastGet;
@property (nonatomic, readonly) BOOL bIsStart;
@property (nonatomic) BOOL bAlwaysInUse;

- (void) startGetGPSWithOKBlock:(void(^)(void))okBlock FailBlock:(void(^)(void))failBlock;
- (void) cancelStart;

- (void) startGetInUseGPS;
- (void) startGetGPS;
- (void) stopGetGPS;



+ (void) getCLLocationCoordinate2DOfAddress:(NSString*)strAddress CompleteBlock:(GpsCompleteBlock)completeBlock;
+ (void) getAddressFromCLLocationCoordinate2D:(CLLocationCoordinate2D)location CompleteBlock:(GpsCompleteBlock)completeBlock;

+ (double) calculateDistanceWithLat1:(double)lat1 Lng1:(double)lon1 Lat2:(double)lat2 Lng2:(double)lon2;
@end
