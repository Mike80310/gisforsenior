//
//  vcQuestion.m
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcQDisabilities.h"
#import "vcQAborigines.h"
#import "vcMapMain.h"

@interface vcQDisabilities ()

@end

@implementation vcQDisabilities

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    float fBtnRadius = 20;
    float fVRadius = 5;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.lblQuestionTitle.font =
        [UIFont boldSystemFontOfSize:fFontSize + 5];
        
        CGRect cgBtnNo = self.btnNo.frame;
        cgBtnNo.origin.y -= 10;
        cgBtnNo.size.height = barHeightInPlus;
        self.btnNo.frame = cgBtnNo;
        
        CGRect cgBtnYes = self.btnYES.frame;
        cgBtnYes.origin.y -= 10;
        cgBtnYes.size.height = barHeightInPlus;
        self.btnYES.frame = cgBtnYes;
        
        fBtnRadius = barRadiusInPlus;
        fVRadius = 8;
    }
    
    self.vQuestionBG.layer.cornerRadius = fVRadius;
    self.btnNo.layer.cornerRadius = fBtnRadius;
    self.btnYES.layer.cornerRadius = fBtnRadius;
    
}

-(IBAction) clickYES:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKeyPath:kDicKeyDisabilities];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self pushToVcMapMain];
}

-(IBAction) clickNO:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKeyPath:kDicKeyDisabilities];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self pushToNextQuestion];
}

- (void) pushToVcMapMain
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcMapMain* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcMapMain"];
    vc.strPrePage = @"vcQuestion";
    vc.title = @"查詢結果";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) pushToNextQuestion
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcQAborigines* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcQAborigines"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
