//
//  vcQuestion3.h
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcQAge : vcBase<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic,weak) IBOutlet UIView *vQuestionBG;
@property (nonatomic,weak) IBOutlet UILabel *lblQuestionTitle;

@property (nonatomic,weak) IBOutlet UIView *vYearMonthBarBG;
@property (nonatomic,weak) IBOutlet UILabel *lblYearMonthBar;
@property (nonatomic,weak) IBOutlet UIImageView *img;

@property (nonatomic,weak) IBOutlet UIPickerView *pvMain;

@property (nonatomic,weak) IBOutlet UIButton *btnYES;

@end
