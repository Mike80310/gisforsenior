//
//  GPSHelper.m
//  WinGuide
//
//  Created by CHX on 2015/5/4.
//  Copyright (c) 2015年 CHX. All rights reserved.
//

#import "GPSHelper.h"

@interface GPSHelper()
{
    void (^blkStartGPSOK)(void);
    void (^blkStartGPSFail)(void);
}

@end
@implementation GPSHelper

static GPSHelper* gps;

- (instancetype)init
{
    if(gps) return gps;
    self = [super init];
    if (self) {
        gps = self;
        manager = [[CLLocationManager alloc] init];
        manager.delegate = self;
    }
    return gps;
}

#pragma mark - Location
- (void) startGetGPSWithOKBlock:(void(^)(void))okBlock FailBlock:(void(^)(void))failBlock
{
    blkStartGPSOK = okBlock;
    blkStartGPSFail = failBlock;

    [self startGetGPS];
}

- (void) cancelStart
{
    blkStartGPSOK = nil;
    blkStartGPSFail = nil;
    
    [self stopGetGPS];
}

- (void) startGetInUseGPS
{
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([manager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [manager requestAlwaysAuthorization];
    }
    [manager startUpdatingLocation];
    
    _bIsStart = YES;
}

- (void) startGetGPS
{
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if(_bAlwaysInUse)
    {
        if ([manager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [manager requestAlwaysAuthorization];
        }
    }
    else
    {
        if ([manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [manager requestWhenInUseAuthorization];
        }
    }
    
    [manager startUpdatingLocation];
    
    _bIsStart = YES;
}

- (void) stopGetGPS
{
    _bIsStart = NO;
    [manager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if(locations.count != 0)
    {
        CLLocation* loc = locations[0];
        
        _GPS = loc.coordinate;
        _datLastGet = [NSDate new];
        
        if(blkStartGPSOK)
        {
            blkStartGPSOK();
            blkStartGPSOK = nil;
            blkStartGPSFail = nil;
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager
	didFailWithError:(NSError *)error
{
    if(blkStartGPSFail)
    {
        [self stopGetGPS];
        blkStartGPSFail();
        blkStartGPSOK = nil;
        blkStartGPSFail = nil;
    }
    return;
}

#pragma mark - Google Map API
+ (void) getCLLocationCoordinate2DOfAddress:(NSString*)strAddress CompleteBlock:(GpsCompleteBlock)completeBlock
{
    NSString* strUrl = [[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=true&language=zh-TW", strAddress] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:strUrl];
    
    [self getDataWithUrl:url CompleteBlock:completeBlock FilterBlock:^id(id Json) {
        if(Json == nil) return Json;
        
        Json = [self getLocationOwGoogleResponse:Json];
        
        return Json;
    }];
}

+ (void) getAddressFromCLLocationCoordinate2D:(CLLocationCoordinate2D)location CompleteBlock:(GpsCompleteBlock)completeBlock
{
    NSString* strUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true&language=zh-TW", location.latitude, location.longitude];
    [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:strUrl];
    
    [self getDataWithUrl:url CompleteBlock:completeBlock FilterBlock:^id(id Json) {
        if(Json == nil) return Json;
        
        Json = [self getAddressOfGoogleResponse:Json];
        
        return Json;
    }];
}


+ (void) getDataWithUrl:(NSURL*)url CompleteBlock:(GpsCompleteBlock)completeBlock FilterBlock:(id(^)(id Json))filterBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError* err = [NSError new];
    
        NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
        NSURLResponse* urlResponse = nil;
    
        NSData *sourceData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&urlResponse error:&err];
        
        @try {
            if(sourceData == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completeBlock(NO, @"下載過程發生錯誤!!", nil);
                });
                return;
            }
            
            NSString* ErrMsg = @"";
            id Json = [self analysisJson:sourceData ErrMsg:&ErrMsg];
            
            Json = filterBlock(Json);
            
            if(!Json)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completeBlock(NO, ErrMsg, nil);
                });
                return;
            }

            NSLog(@"%@，下載完成!", url);
            dispatch_async(dispatch_get_main_queue(), ^{
                completeBlock(YES, ErrMsg, Json);
            });

        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
            dispatch_async(dispatch_get_main_queue(), ^{
                completeBlock(NO, @"傳輸發生問題，請稍候..", nil);
            });
        }
    });
}

+ (id) analysisJson:(NSData *)jsonData ErrMsg:(NSString**)ErrMsg
{
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
    if(jsonObject == nil)
    {
        NSString* strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
        NSRange rangeLeft = [strData rangeOfString:@"{\""];
        if(rangeLeft.location != -1 && rangeLeft.location < strData.length)
        {
            jsonError = nil;
            strData = [strData substringFromIndex:rangeLeft.location];
        }
        jsonObject = [NSJSONSerialization JSONObjectWithData:[strData dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&jsonError];
    }
    
    if(jsonError)
    {
        *ErrMsg = @"Json解析失敗!";
        return nil;
    }

    return jsonObject;
}



+ (NSDictionary*) getAddressOfGoogleResponse:(NSDictionary*)dicJson
{
    @try {
        NSArray* aryResults = [dicJson valueForKey:@"results"];
        NSDictionary* dicAddressComponent = aryResults[0];
        NSArray *aryAddressCut = [dicAddressComponent objectForKey:@"address_components"];

        NSString *strArea = @"";
        NSString *strCity = @"";

        for (NSDictionary* dicArea in aryAddressCut) {
            NSArray* aryTypes = [dicArea valueForKey:@"types"];

            if([aryTypes containsObject:@"administrative_area_level_3"])
            {
                strArea = [dicArea valueForKey:@"long_name"];
            }

            if([aryTypes containsObject:@"administrative_area_level_2"] || [aryTypes containsObject:@"administrative_area_level_1"])
            {
                strCity = [dicArea valueForKey:@"long_name"];
            }
        }


        return @{@"Address":[dicAddressComponent valueForKey:@"formatted_address"],
                 @"Area":strArea,
                 @"City":strCity};
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        return nil;
    }
}



+ (NSDictionary*) getLocationOwGoogleResponse:(NSDictionary*)Json
{
    @try {
                
        NSArray* aryResult = [Json valueForKey:@"results"];
        NSDictionary* dicResult = aryResult[0];
        NSDictionary* dicGeo = [dicResult valueForKey:@"geometry"];
        NSDictionary* dicLocation = [dicGeo valueForKey:@"location"];
        float fLat = [[dicLocation valueForKey:@"lat"] floatValue];
        float fLng = [[dicLocation valueForKey:@"lng"] floatValue];
        
        return @{@"Lat":@(fLat), @"Lng":@(fLng)};
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        return nil;
    }
}

+ (double) calculateDistanceWithLat1:(double)lat1 Lng1:(double)lon1 Lat2:(double)lat2 Lng2:(double)lon2
{
    const double PI = 3.1415926;
    
	double er = 6378137; // 6378700.0f;
	//ave. radius = 6371.315 (someone said more accurate is 6366.707)
	//equatorial radius = 6378.388
	//nautical mile = 1.15078
	double radlat1 = PI*lat1/180.0f;
	double radlat2 = PI*lat2/180.0f;
    
	//now long.
	double radlong1 = PI*lon1/180.0f;
	double radlong2 = PI*lon2/180.0f;
    
	if( radlat1 < 0 ) radlat1 = PI/2 + fabs(radlat1);// south
	if( radlat1 > 0 ) radlat1 = PI/2 - fabs(radlat1);// north
	if( radlong1 < 0 ) radlong1 = PI*2 - fabs(radlong1);//west
    
	if( radlat2 < 0 ) radlat2 = PI/2 + fabs(radlat2);// south
	if( radlat2 > 0 ) radlat2 = PI/2 - fabs(radlat2);// north
	if( radlong2 < 0 ) radlong2 = PI*2 - fabs(radlong2);// west
    
	//spherical coordinates x=r*cos(ag)sin(at), y=r*sin(ag)*sin(at), z=r*cos(at)
	//zero ag is up so reverse lat
	double x1 = er * cos(radlong1) * sin(radlat1);
	double y1 = er * sin(radlong1) * sin(radlat1);
	double z1 = er * cos(radlat1);
    
	double x2 = er * cos(radlong2) * sin(radlat2);
	double y2 = er * sin(radlong2) * sin(radlat2);
	double z2 = er * cos(radlat2);
    
	double d = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
    
	//side, side, side, law of cosines and arccos
	double theta = acos((er*er+er*er-d*d)/(2*er*er));
	double dist  = theta*er;
    
//    NSLog(@"%f", dist);
	return dist;
}

@end
