//
//  vcQuestion3.m
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcQAge.h"
#import "vcQCareTime.h"
#define FIRST_YEAR 1911
#define pickerRowHeight 40
#define defaultAge 60

@interface vcQAge ()
{
    NSDateFormatter *formatter;
    NSDateComponents *cmt;
    int nAge;
    
    NSMutableArray *aryYears;
    NSMutableArray *aryMonths;
    
    int todayYear;
    int todayMonth;
}
@end

@implementation vcQAge

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self setLayout];
    
    formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyyMM";
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [self setPickerviewDefault];
    
//    self.dpMain.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
//    self.dpMain.timeZone = [NSTimeZone timeZoneWithName:@"defaultTimeZone"];
//    self.dpMain.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSRepublicOfChinaCalendar];
//    self.dpMain.datePickerMode = UIDatePickerModeDate;
//    [self.dpMain addTarget:self action:@selector(getDateValue:) forControlEvents:UIControlEventValueChanged];
   
}

#pragma mark - pickerview default

- (void) setPickerviewDefault
{
    self.pvMain.delegate = self;
    self.pvMain.dataSource = self;
    
    NSCalendar* calendarForPvMain = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendarForPvMain components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:[NSDate date]];
    
    todayYear  = (int)components.year;   // 現在の年を取得
    todayMonth = (int)components.month;  // 現在の月を取得
    
    aryYears = [[NSMutableArray alloc] init];
    for (int i = FIRST_YEAR; i <= todayYear; i++) {
        [aryYears addObject:[NSString stringWithFormat:@"%d", i-1911]];
    }
    
    aryMonths = [[NSMutableArray alloc] initWithCapacity:12];
    for (int i = 1; i <= 12; i++) {
        [aryMonths addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    int rowOfTodayYear  = todayYear  - FIRST_YEAR - defaultAge;    // 年の列数を計算
    int rowOfTodayMonth = todayMonth - 1;             // 月の列数を計算
    
    [self.pvMain selectRow:rowOfTodayYear  inComponent:0 animated:YES];  // 年を選択
    [self.pvMain selectRow:rowOfTodayMonth inComponent:1 animated:YES]; // 月を選択
    
    [self setYearMonthBarText:[NSString stringWithFormat:@"民國%i年/%02i月",
                               [[aryYears objectAtIndex:rowOfTodayYear] intValue],
                               [[aryMonths objectAtIndex:rowOfTodayMonth] intValue]]];
    
    NSDictionary *dicAgeInfo =
    [self getAgeInfoFromYear:[[aryYears objectAtIndex:rowOfTodayYear] intValue] Month:[[aryMonths objectAtIndex:rowOfTodayMonth] intValue]];
    
    nAge = [[dicAgeInfo objectForKey:@"Year"] intValue];
    
    NSLog(@"預設年齡為 %i",nAge);
    
}

#pragma mark - 年齡計算

- (NSDictionary* ) AgefromDate:(NSDate*)date
{
    NSString *strMyDate = [formatter stringFromDate:date];
    NSString *strNowDate = [formatter stringFromDate:[NSDate date]];
    
    NSDate *myDate = [formatter dateFromString:strMyDate];
    NSDate *nowDate = [formatter dateFromString:strNowDate];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSRepublicOfChinaCalendar];
    NSDateComponents *comps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit
                                          fromDate:myDate toDate:nowDate options:0];

    int nYear = (int)[comps year];
    int nMonth = (int)[comps month];
//    int nDay = (int)[comps day];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:
                         [NSNumber numberWithInt:nYear],@"Year",
                         [NSNumber numberWithInt:nMonth],@"Month",
                          nil];//[NSNumber numberWithInt:nDay],@"Day",
    
    //    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:nYear],@"Year",
    //                         [NSNumber numberWithInt:nMonth],@"Month",[NSNumber numberWithInt:nDay],@"Day", nil];
    
    return dic;
    //    [NSString stringWithFormat:@"歲數：%ld年又%ld個月",(long)year,(long)Month];
}

- (void) setLayout
{
    float fBtnRadius = 20;
    float fVRadius = 5;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.lblQuestionTitle.font =
        [UIFont boldSystemFontOfSize:fFontSize + 5];
        
        self.lblYearMonthBar.font =
        [UIFont systemFontOfSize:fFontSize + 5];
        
        CGRect cgBtnYes = self.btnYES.frame;
        cgBtnYes.origin.y -= 10;
        cgBtnYes.size.height = barHeightInPlus;
        self.btnYES.frame = cgBtnYes;
        
        CGRect cgBar = self.vYearMonthBarBG.frame;
        cgBar.origin.y += 20;
        cgBar.size.height = barHeightInPlus;
        self.vYearMonthBarBG.frame = cgBar;
        
        CGRect cgBarText = self.lblYearMonthBar.frame;
        cgBarText.origin.x +=15;
        cgBarText.origin.y += 2;
        self.lblYearMonthBar.frame = cgBarText;
        
        CGRect cgDp = self.pvMain.frame;
        cgDp.origin.y = cgBarText.origin.y + cgBarText.size.height + cgDp.size.height;
        self.pvMain.frame = cgDp;
        
        fBtnRadius = barRadiusInPlus;
        fVRadius = 8;
    }
    
    self.vYearMonthBarBG.layer.cornerRadius = fBtnRadius;
    self.vQuestionBG.layer.cornerRadius = fVRadius;
    self.btnYES.layer.cornerRadius = fBtnRadius;

}

-(IBAction) clickYES:(id)sender
{
    if ([self.lblYearMonthBar.text isEqualToString:@"年/月"])
    {
        [self HudDismissWithError:@"您尚未輸入出生年月" finishBlock:^{
            
        } Title:nil];

        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%i",nAge] forKeyPath:kDicKeyAge];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self pushToNextQuestion];
}

- (void) pushToNextQuestion
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcQCareTime* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcQCareTime"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - pickview delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerRowHeight;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [aryYears count];
    }
    else if (component == 1)
    {
         return [aryMonths count];
    }
    
    return 0;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row  inComponent:(NSInteger)component {
    
    // PickerViewの選択されている年と月の列番号を取得
    int rowOfYear  = (int)[pickerView selectedRowInComponent:0]; // 年を取得
    int rowOfMonth = (int)[pickerView selectedRowInComponent:1];          // 月を取得
    
    NSDictionary *dicAgeInfo = [self getAgeInfoFromYear:rowOfYear Month:rowOfMonth];
    nAge = [[dicAgeInfo objectForKey:@"Year"] intValue];
    
    NSLog(@"年齡為 %i",nAge);
    int nMonth = [[dicAgeInfo objectForKey:@"Month"] intValue];
    
    if (nAge <= 0 && nMonth <= 0 &&
        [[aryMonths objectAtIndex:rowOfMonth] intValue] != todayMonth)
    {
        [self HudDismissWithError:@"出生年月不可大於本月！" finishBlock:^{
            
        } Title:nil];
        
        int rowOfTodayYear  = todayYear  - FIRST_YEAR;    // 年の列数を計算
        int rowOfTodayMonth = todayMonth - 1;             // 月の列数を計算
        [self.pvMain selectRow:rowOfTodayYear inComponent:0 animated:YES];  // 年を選択
        [self.pvMain selectRow:rowOfTodayMonth inComponent:1 animated:YES]; // 月を選択
    }
    else
    {
        [self setYearMonthBarText:[NSString stringWithFormat:@"民國%i年/%02i月",
                                   [[aryYears objectAtIndex:rowOfYear] intValue],
                                   [[aryMonths objectAtIndex:rowOfMonth] intValue]]];
    }

}

- (NSDictionary *) getAgeInfoFromYear:(int)nYear Month:(int) nMonth
{
    NSString *strTmp = [NSString stringWithFormat:@"%i%02i",
                        nYear + 1911,
                        nMonth];
    NSDate *dtTmp = [formatter dateFromString:strTmp];
    
    
    return [self AgefromDate:dtTmp];
}

-(void) setYearMonthBarText:(NSString *) strTitle
{
    self.lblYearMonthBar.text = strTitle;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return pickerView.frame.size.width/2;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    NSString *title;
    
    if(component == 0)
    {
        title = [NSString stringWithFormat:@"民國%@年",[aryYears objectAtIndex:row]];
    }
    else if(component == 1)
    {
        title = [NSString stringWithFormat:@"%@月",[aryMonths objectAtIndex:row]];
    }
    
    
    NSLog(@"title %@",title);
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fFontSize]}];
    
    return attString;
    
}

@end
