//
//  PointD.h
//  SuperGIS Runtime
//
//  Created by Zonghan Chuang on 12/3/8.
//  Copyright (c) 2012年 supergeo.rd@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGPointD : NSObject

- (id)initWithX:(double)x andY:(double)y;

@property (nonatomic, assign) double X;

@property (nonatomic, assign) double Y;

@end
