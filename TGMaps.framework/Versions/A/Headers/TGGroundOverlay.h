//
//  TGOSGroundOverlay.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/10.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TGGroundOverlay

@property (nonatomic, assign) CLLocationCoordinate2D position;

@property (nonatomic, strong) TGLatLngBounds *bounds;

@property (nonatomic, assign) CGFloat width;

@property (nonatomic, assign) CGFloat height;

@property (nonatomic, assign) CGPoint anchor;

@property (nonatomic, strong) UIImage *icon;

@property (nonatomic, assign) CLLocationDirection bearing;

@property (nonatomic, assign) CGFloat transparency;

@property (nonatomic, assign) BOOL visibility;

- (void)remove;

@end
