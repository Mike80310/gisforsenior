//
//  vcQCareTime.m
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcQCareTime.h"
//#import "vcQIntubation.h"
#import "vcQDisabilities.h"
#import "vcMapMain.h"

@interface vcQCareTime ()

@end

@implementation vcQCareTime

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    float fBtnRadius = 20;
    float fVRadius = 5;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.lblQuestionTitle.font =
        [UIFont boldSystemFontOfSize:fFontSize + 5];
        
        CGRect cgBtnAllDay = self.btnAllDay.frame;
        cgBtnAllDay.origin.y += 10;
        cgBtnAllDay.size.height = barHeightInPlus;
        self.btnAllDay.frame = cgBtnAllDay;
        
        CGRect cgBtnAnyTime = self.btnAnyTime.frame;
        cgBtnAnyTime.origin.y += 10;
        cgBtnAnyTime.size.height = barHeightInPlus;
        self.btnAnyTime.frame = cgBtnAnyTime;
        
        CGRect cgBtnHalfDay = self.btnHalfDay.frame;
        cgBtnHalfDay.origin.y += 10;
        cgBtnHalfDay.size.height = barHeightInPlus;
        self.btnHalfDay.frame = cgBtnHalfDay;
        
        fBtnRadius = barRadiusInPlus;
        fVRadius = 8;
    }
    
    self.vQuestionBG.layer.cornerRadius = fVRadius;
    
    self.btnHalfDay.layer.cornerRadius =
    self.btnAnyTime.layer.cornerRadius =
    self.btnAllDay.layer.cornerRadius =fBtnRadius;
}

- (IBAction) clickTime:(id)sender
{
    UIButton *btn = (UIButton *) sender;
    switch (btn.tag) {
        case 101:
            [[NSUserDefaults standardUserDefaults] setValue:@0 forKeyPath:kDicKeyCareTime];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        case 102:
            [[NSUserDefaults standardUserDefaults] setValue:@1 forKeyPath:kDicKeyCareTime];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        case 103:
            [[NSUserDefaults standardUserDefaults] setValue:@2 forKeyPath:kDicKeyCareTime];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
            
        default:
            break;
    }
    
    [self pushToNextQuestion];
    
//    if (btn.tag ==101)
//    {
//        [self pushToNextQuestion];
//    }
//    else
//    {
//        [self pushToVcMapMain];
//    }
}

- (void) pushToNextQuestion
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcQDisabilities* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcQDisabilities"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) pushToVcMapMain
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcMapMain* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcMapMain"];
    vc.strPrePage = @"vcQuestion";
    vc.title = @"查詢結果";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
