//
//  SoapTool.m
//  GisForSenior
//
//  Created by jane on 2015/7/28.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "SoapTool.h"

@implementation SoapTool
{
    NSXMLParser *xmlParser;
    NSMutableString *strDicInfo;

}
@synthesize delegate;


- (void) getBasicData:(NSString *) strServiceName
{

    NSString *log = [NSString stringWithFormat:@"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><%@ xmlns=\"http://tempuri.org/\" /></soap:Body></soap:Envelope>",strServiceName];
    
//    NSLog(@"logggg: %@",log);
    
    //Now create a request to the URL
    NSURL *url = [NSURL URLWithString:kServiceUrl];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[log length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"http://tempuri.org/%@",strServiceName ]  forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [log dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:theRequest  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self startParser:data error:error];
                                          }];
    
    
    [postDataTask resume];

}

- (void) getCareCenterData:(NSArray *) aryServiceItemNo RegionNo:(NSString *) strRegionAreaNo IsHomeServiceRegion:(BOOL) IsHomeServiceRegion CenterName:(NSString *) strSearchText
{
    NSMutableString *strItemNo = [[NSMutableString alloc]init];;
    for (int i = 0; i < aryServiceItemNo.count ;i ++)
    {
        NSString *str =[NSString stringWithFormat:@"<string>%@</string>",[aryServiceItemNo objectAtIndex:i]];
        [strItemNo appendString:str];
    }
    
    NSString *log = [NSString stringWithFormat:@"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><getCareCenterList xmlns=\"http://tempuri.org/\"><ServiceItemNo>%@</ServiceItemNo><RegionNo>%@</RegionNo><IsHomeServiceRegion>%hhd</IsHomeServiceRegion><CenterName>%@</CenterName></getCareCenterList></soap:Body></soap:Envelope>",strItemNo,strRegionAreaNo,IsHomeServiceRegion,strSearchText];
                     
    
//    NSLog(@"logggg: %@",log);
    
    //Now create a request to the URL
    NSURL *url = [NSURL URLWithString:kServiceUrl];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[log length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"http://tempuri.org/%@",@"getCareCenterList"]  forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [log dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:theRequest  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self startParser:data error:error];
                                          }];
    
    
    [postDataTask resume];
    
}

- (void) startParser:(NSData *) data error:(NSError *) error
{
    if (!error)
    {
        xmlParser = [[NSXMLParser alloc] initWithData:data];
        [xmlParser setDelegate:self];
        [xmlParser parse];
    }
    else
    {
        [delegate loadFinishRespose:nil userInfo:@"服務異常" error:error];
    }
    
}

- (void) getSurveyResult:(BOOL) IsDisabilities IsAborigines:(BOOL) IsAborigines Age:(int) nAge CareTime:(int) nCareTime Intubation:(int)nIntubation adlLevel:(int) nLevel
{
    
    NSString *log = [NSString stringWithFormat:@"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><getSurveyResult xmlns=\"http://tempuri.org/\"><IsDisabilities>%hhd</IsDisabilities><IsAborigines>%hhd</IsAborigines><Age>%i</Age><CareTime>%i</CareTime><Intubation>%i</Intubation><ADLLevel>%i</ADLLevel></getSurveyResult></soap:Body></soap:Envelope>",IsDisabilities,IsAborigines,nAge,nCareTime,nIntubation,nLevel];
    
//    NSLog(@"logggg: %@",log);
    
    //Now create a request to the URL
    NSURL *url = [NSURL URLWithString:kServiceUrl];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[log length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"http://tempuri.org/%@",@"getSurveyResult"]  forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [log dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:theRequest  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self startParser:data error:error];
                                          }];
    
    
    [postDataTask resume];
    
}

- (void) searchCareCenterName:(NSString *) strSearchText
{
    
    NSString *log = [NSString stringWithFormat:@"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><getCareCenter xmlns=\"http://tempuri.org/\"><CenterName>%@</CenterName></getCareCenter></soap:Body></soap:Envelope>",strSearchText];
    
//    NSLog(@"logggg: %@",log);
    
    //Now create a request to the URL
    NSURL *url = [NSURL URLWithString:kServiceUrl];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[log length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"http://tempuri.org/%@",@"getCareCenter" ]  forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [log dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:theRequest  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self startParser:data error:error];
                                          }];
    
    
    [postDataTask resume];
    
}

- (void) getCareCenterByCenterID:(NSString *) strCareCenterID
{
    
    NSString *log = [NSString stringWithFormat:@"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><getCareCenterByCenterID xmlns=\"http://tempuri.org/\"><CareCenterID>%@</CareCenterID></getCareCenterByCenterID></soap:Body></soap:Envelope>",strCareCenterID];
    
//    NSLog(@"logggg: %@",log);
    
    //Now create a request to the URL
    NSURL *url = [NSURL URLWithString:kServiceUrl];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[log length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: [NSString stringWithFormat:@"http://tempuri.org/%@",@"getCareCenterByCenterID"]  forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [log dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:theRequest  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self startParser:data error:error];
                                          }];
    
    
    [postDataTask resume];
    
}


#pragma mark - 以下開始解析

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    strDicInfo = [[NSMutableString alloc]init];
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [strDicInfo appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{

    NSArray *aryData = [self ParserStringToData:strDicInfo];
    if(aryData != nil)
    {
        [delegate loadFinishRespose:aryData userInfo:elementName error:nil];
    }
    
   
}

- (NSArray *) ParserStringToData:(NSString *) strData
{
    strData = [strData stringByReplacingOccurrencesOfString:@"\\" withString:@""];
//    strData = [strData stringByReplacingOccurrencesOfString:@" " withString:@""];
    strData = [strData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    strData = [strData stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    strData = [strData stringByReplacingOccurrencesOfString:@"\t" withString:@""];

    NSData *data = [strData dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    NSDictionary *dicJson = (NSDictionary *) json;
    
    NSArray *aryTmp;
    if ([[dicJson objectForKey:@"Success"] boolValue])
    {
        aryTmp = [dicJson objectForKey:@"Data"];
    }
    else
    {
        NSError *errorTmp;
        if (error != nil)
        {
            errorTmp = error;
        }
        else
        {
            errorTmp = [[NSError alloc] initWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey:[dicJson objectForKey:@"Message"]}];
        }
    
        [delegate loadFinishRespose:nil userInfo:@"服務異常" error:errorTmp];
        [xmlParser abortParsing];
    }
    
    
    
    return aryTmp;

}

@end
