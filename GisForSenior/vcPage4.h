//
//  vcPage4.h
//  GisForSenior
//
//  Created by jane on 2015/11/2.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcPage4 : vcBase<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView *tvMain;
@property (nonatomic,weak) IBOutlet UIButton *btnNext;

@property (nonatomic,weak) IBOutlet UIView *vMask;
@property (nonatomic,weak) IBOutlet UIView *vQuestion;
@property (nonatomic,weak) IBOutlet UIButton *btnConfirm;

@property (nonatomic,weak) IBOutlet UIButton *btnNO1;
@property (nonatomic,weak) IBOutlet UIButton *btnYES1;
@property (nonatomic,weak) IBOutlet UIButton *btnNO2;
@property (nonatomic,weak) IBOutlet UIButton *btnYES2;
@property (nonatomic,weak) IBOutlet UIButton *btnNO3;
@property (nonatomic,weak) IBOutlet UIButton *btnYES3;
@property (nonatomic,weak) IBOutlet UIButton *btnNO4;
@property (nonatomic,weak) IBOutlet UIButton *btnYES4;
@property (nonatomic,weak) IBOutlet UIButton *btnNO5;
@property (nonatomic,weak) IBOutlet UIButton *btnYES5;
@property (nonatomic,weak) IBOutlet UIButton *btnNO6;
@property (nonatomic,weak) IBOutlet UIButton *btnYES6;

@end
