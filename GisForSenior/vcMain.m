//
//  vcMain.m
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcMain.h"
#import "vcMapMain.h"
#import "vcFilter.h"
//#import "vcQDisabilities.h"
//#import "vcQAge.h"
//#import "vcLaw.h"
#import "vcLawCatalog.h"
#import "vcInstitution.h"
#import "NetHelper.h"

@interface vcMain ()
{
    SoapTool *soap;
    NSMutableArray *aryDownloadFinshiFunction;
    
    NSString *strRegionAreaNo;
}
@end

@implementation vcMain

- (void) viewWillAppear:(BOOL)animated
{
    


    [self getNowAddress];
    [self clearDefault];// for 問卷式 已作廢
    
}

- (void) clearDefault
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyAborigines];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyAge];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyCareTime];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyDisabilities];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyIntubation];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblAppTitle.text = @"長照服務資源地理地圖";
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    self.lblVer.text = [NSString stringWithFormat:@"版本:%@",version];
    
    aryDownloadFinshiFunction = [[NSMutableArray alloc] init];
    
    btnLeft.hidden = YES;
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kFirstOpenFeedBack] == nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendAddressToServer) name:kFirstOpenFeedBack object:nil];
    }
    
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.btnHowto.titleLabel.font =
        self.btnLaw.titleLabel.font =
        self.btnSearch.titleLabel.font = [UIFont systemFontOfSize:fFontSize + 5];
//        [self.btnHowto setTitle:@"                   如何選擇服務資源" forState:UIControlStateNormal];
//        [self.btnLaw setTitle:@"                    長照知識庫" forState:UIControlStateNormal];
    }
    [self HudShowMsg:@"資料下載中..."];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
         [self getNowAddress];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startDownloadBasicData];
        });
        
    });
    
}

- (void) startDownloadBasicData
{
    soap = [[SoapTool alloc] init];
    soap.delegate = self;
    [soap getBasicData:kServiceRegionList];

}

- (void) retryDownload
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"資料未下載，無法使用此功能，將重新開始下載資料" delegate:self cancelButtonTitle:@"確定" otherButtonTitles: nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self HudShowMsg:@"資料下載中..."];
    [soap getBasicData:kServiceRegionList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) clickHowtoChoice:(id)sender
{
    if (aryDownloadFinshiFunction.count <2)
    {
        [self retryDownload];
        return;
    }
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcInstitution* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcInstitution"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction) clickSearch:(id)sender
{
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    vcMapMain* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcMapMain"];
//    vc.strPrePage = @"vcMain";
    
    if (aryDownloadFinshiFunction.count <2)
    {
        [self retryDownload];
        return;
    }

    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vcFilter *vc = [storyBoard instantiateViewControllerWithIdentifier:@"vcFilter"];
    vc.strPrePage = @"vcMain";
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction) clickLaw:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcLawCatalog* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcLawCatalog"];
    [self.navigationController pushViewController:vc animated:YES];

}

#pragma mark - SoapTool Delegate

- (void) loadFinishRespose:(NSArray *) resposeData userInfo:(NSString *)UserInfo error:(NSError *)error
{
    if (error)
    {

        [self HudErrMsgWithError:@"" finishBlock:^{
            UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:UserInfo message:error.localizedDescription delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [alertError show];
        }];
        
//        [self HudDismissWithError:error.localizedDescription finishBlock:^{} Title:UserInfo];
    }
    else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceRegionList] ])
    {
        [aryDownloadFinshiFunction addObject:kServiceRegionList];
        [[NSUserDefaults standardUserDefaults] setValue:resposeData forKeyPath:kServiceRegionList];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
//        NSLog(@"kServiceRegionList OK %@",resposeData);
        
        [soap getBasicData:kServiceCareCenterTypeList];

    }
    else if([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenterTypeList]])
    {
        [aryDownloadFinshiFunction addObject:kServiceCareCenterTypeList];
        [[NSUserDefaults standardUserDefaults] setValue:resposeData forKeyPath:kServiceCareCenterTypeList];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
//        NSLog(@"kServiceCareCenterTypeList OK %@",resposeData);
    }
    
    if (aryDownloadFinshiFunction.count >= 2)
    {
        [self HudDismissWithSuccess:@"下載完成!" finishBlock:^{
           
        }];
    }
}

- (void) sendAddressToServer
{
    NSDictionary *dicLatLng = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserLatLng];
    
    if (dicLatLng !=nil)
    {
        CLLocationCoordinate2D locationUser = CLLocationCoordinate2DMake([[dicLatLng objectForKey:@"Lat"] doubleValue],
                                                                         [[dicLatLng objectForKey:@"Lng"] doubleValue]);
        
        [self startGoogleApi:locationUser];
        

    }
    
}

- (void) startGoogleApi:(CLLocationCoordinate2D) clcLocationUser
{
    [GPSHelper getAddressFromCLLocationCoordinate2D:clcLocationUser CompleteBlock:^(bool Success, NSString *ErrMsg, NSDictionary *dicStatus) {
        
        CLLocationCoordinate2D clcLoc;
        if(Success == NO)
        {
            strRegionAreaNo = [self getRegionAreaNo:@"南港區" CityName:@"臺北市"];
        }
        else
        {
            clcLoc = clcLocationUser;
            NSString *strCityName = [dicStatus objectForKey:kCity];
            NSString *strAreaName = [dicStatus objectForKey:kArea];
            
            strCityName = [strCityName stringByReplacingOccurrencesOfString:@"台" withString:@"臺"];
            strAreaName = [strAreaName stringByReplacingOccurrencesOfString:@"台" withString:@"臺"];
            
            strRegionAreaNo = [self getRegionAreaNo:strAreaName CityName:strCityName];
            
            
        }
        
        
        NSString *strUUID = [[NSUserDefaults standardUserDefaults] objectForKey:kUUID];
        NSLog(@"vcMainUUID %@",strUUID);
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"傳送UUID" message:strUUID delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
        //        [alert show];
        
        NetHelper *nH = [[NetHelper alloc] initSendAddressToServerWithPhoneKey:strUUID regionNo:strRegionAreaNo];
        
        [nH startConnectWithOKBlock:^(id Json)
         {
             NSLog(@"sendAddressToServer Json %@",Json);
             [self SetValueToFirstOpenFeedback:Json];
             
         }
                          FailBlock:^(NSString *ErrMsg)
         {
             NSLog(@"sendAddressToServer ErrMsg %@",ErrMsg);
             [self SetValueToFirstOpenFeedback:ErrMsg];
         }];
        
        
        
    }];
}

- (void) SetValueToFirstOpenFeedback:(NSString *) strValue
{
    [[NSUserDefaults standardUserDefaults] setObject:strValue forKey:kFirstOpenFeedBack];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] removeObserver:kFirstOpenFeedBack];
    
}

- (NSString *) getRegionAreaNo:(NSString *) strArea CityName:(NSString *) strCity
{
    NSString *strPred = [NSString stringWithFormat:@"Name == '%@'",strArea];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    
    NSArray *aryArea = [[NSUserDefaults standardUserDefaults] objectForKey:kServiceRegionList];
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    NSArray *aryCityTmp = [self getRegionCityList:aryArea];

//    for (NSDictionary* dicTmp in aryArea) {
//        NSLog(@"%@", [dicTmp valueForKey:@"Name"]);
//    }

    NSString *strNo = @"-1";
    
    for (int i = 0; i<aryTmp.count; i++)
    {
        NSDictionary *dicAreaTmp =aryTmp[i];
        
        for (int j = 0; j < aryCityTmp.count; j++)
        {
            
            NSString *strCityNoTmp =[aryCityTmp[j] objectForKey:kRegionNo];
            NSString *strAreaCityNoTmp = [dicAreaTmp objectForKey:kRegionParentNo];
            NSString *strCityNameTmp = [aryCityTmp[j] objectForKey:kRegionName];
            
            if ([strAreaCityNoTmp isEqualToString:strCityNoTmp]
                && [strCityNameTmp isEqualToString:strCity])
            {
                strNo = [dicAreaTmp objectForKey:kRegionNo];
            }
            
            
        }
        
    }
    
    return strNo;
}

- (NSArray *) getRegionCityList:(NSArray *) aryArea
{
    NSString *strPred = [NSString stringWithFormat:@"ParentNo == '-1'"];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    NSMutableArray* aryNew = [NSMutableArray new];
    for (NSDictionary *dic in aryTmp)
    {
        NSMutableDictionary *dicNew = [dic mutableCopy];
        NSString *strSort = [dicNew objectForKey:@"Sort"];
        if(strSort.length == 1)
        {
            NSString *strInt = [NSString stringWithFormat:@"%02i",[strSort intValue]];
            [dicNew setObject:strInt forKey:@"Sort"];
        }
        [aryNew addObject:[dicNew copy]];
    }
    aryTmp = aryNew;
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"Sort" ascending:YES];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
    
    return aryTmp;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
