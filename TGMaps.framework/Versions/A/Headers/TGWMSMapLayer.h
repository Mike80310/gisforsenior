//
//  WMSMapLayer.h
//  TGMaps
//
//  Created by supergeo.rd on 13/7/23.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "TGMapLayer.h"

@class TGEnvelope;


@interface TGWMSMapLayer : TGMapLayer

@property (nonatomic, readonly, copy) NSString *copyright;

@property (nonatomic, readonly, copy) NSString *version;

@property (nonatomic, readonly, copy) NSString *name;

@property (nonatomic, readonly, copy) NSString *SRS;

@property (nonatomic, readonly, strong) TGEnvelope *fullEnvelope;

@property (nonatomic, readonly, assign) BOOL loaded;

@property (nonatomic, readonly, copy) NSArray *layerInfos;

@property (nonatomic, assign) BOOL transparency;

@property (nonatomic, assign) CGFloat opacity;

@property (nonatomic, copy) NSArray *visibleLayers;

- (id)initWithURL:(NSString *)url;

@end
