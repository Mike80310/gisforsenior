//
//  AppDelegate.h
//  GisForSenior
//
//  Created by jane on 2015/7/1.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

