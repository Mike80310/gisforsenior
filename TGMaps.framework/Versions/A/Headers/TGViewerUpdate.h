//
//  TGViewerUpdate.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/6/6.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreLocation/CoreLocation.h>

@class TGViewerPosition;
@class TGLatLngBounds;

@interface TGViewerUpdate : NSObject
/**
 * Returns a TGViewerUpdate that zooms in on the map.
 * The zoom incremenbt is 1.0.
 */
+ (TGViewerUpdate *)zoomIn;

/**
 * Returns a TGViewerUpdate that zooms out on the map.
 * The zoom increment is -1.0.
 */
+ (TGViewerUpdate *)zoomOut;

/**
 * Returns a TGViewerUpdate that changes the zoom by the specified amount.
 */
+ (TGViewerUpdate *)zoomBy:(CGFloat)delta;

/**
 * Returns a TGViewerUpdate that sets the zoom to the specified amount.
 */
+ (TGViewerUpdate *)zoomTo:(CGFloat)zoom;

/**
 * Returns a TGViewerUpdate that sets the camera target to the specified
 * coordinate.
 */
+ (TGViewerUpdate *)setTarget:(CLLocationCoordinate2D)target;

/**
 * Returns a TGViewerUpdate that sets the camera target and zoom to the
 * specified values.
 */
+ (TGViewerUpdate *)setTarget:(CLLocationCoordinate2D)target
                          zoom:(CGFloat)zoom;

/**
 * Returns a TGViewerUpdate that sets the camera to the specified
 * GMSCameraPosition.
 */
+ (TGViewerUpdate *)setViewer:(TGViewerPosition *)viewer;

/**
 * Returns a TGViewerUpdate that transforms the camera such that the specified
 * bounds are centered on screen at the greatest possible zoom level. The bounds
 * will have a default padding of 64 points.
 *
 * The returned camera update will set the camera's bearing and tilt to their
 * default zero values (i.e., facing north and looking directly at the Earth).
 */
+ (TGViewerUpdate *)fitBounds:(TGLatLngBounds *)bounds;

/**
 * Returns a TGViewerUpdate that shifts the center of the view by the
 * specified number of points in the x and y directions.
 * X grows to the right, Y grows down.
 */
+ (TGViewerUpdate *)scrollByX:(CGFloat)dX Y:(CGFloat)dY;

/**
 * Zoom with a focus point. The focus point stays fixed on screen.
 */
+ (TGViewerUpdate *)zoomBy:(CGFloat)zoom atPoint:(CGPoint)point;

@end
