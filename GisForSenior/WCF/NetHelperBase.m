//
//  NetHelperBase.m
//  GisForSenior
//
//  Created by 珍珍 on 2016/2/2.
//  Copyright © 2016年 jane. All rights reserved.
//

#import "NetHelperBase.h"

@implementation NetHelperBase

- (NSString*) SvcUrlString
{
//    return @"http://projects.supergeo.com.tw/ltcpa_web/WebService";
    return kServiceUrlForSendAddress;
}

- (NSURL*) url
{
    NSString* strUrl = [NSString stringWithFormat:@"%@/%@", [self SvcUrlString], self.Function];
    
    if(self.parameter)
    {
        strUrl = [strUrl stringByAppendingFormat:@"?%@", self.parameter];
    }
    
    return [NSURL URLWithString: [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

- (id) basicAnalysisJson:(NSData *)jsonData ErrMsg:(NSString**)ErrMsg
{
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
    if(jsonError)
    {
        *ErrMsg = @"Json解析失敗!";
        NSLog(@"%@", *ErrMsg);
        return nil;
    }
    
    return jsonObject;
}

- (void) postWithHttpBody:(NSData*)bodyData OKBlock:(void(^)(id))okBlock FailBlock:(void(^)(id))failBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.url];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:bodyData];
    
    NSLog(@"bodyData %@", [[NSString alloc] initWithData:bodyData encoding:NSUTF8StringEncoding]);
    
    //    __weak NetHelper* wSelf = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError *error = nil;
        NSURLResponse *response = nil;
        
        NSData *sourceData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSLog(@"%@", [[NSString alloc] initWithData:sourceData encoding:NSUTF8StringEncoding]);
        NSString* ErrMsg = @"";
        
        @try {
            
            if (error) {
                ErrMsg = error.localizedDescription;
                dispatch_async(dispatch_get_main_queue(), ^{
                    failBlock(ErrMsg);
                });
                
                return ;
            }
            
            if(sourceData == nil)
            {
                ErrMsg = @"下載過程發生錯誤!!";
                NSLog(@"%@",ErrMsg);
                dispatch_async(dispatch_get_main_queue(), ^{
                    failBlock(ErrMsg);
                });
                return;
            }
            
//            id Json = [self basicAnalysisJson:sourceData ErrMsg:&ErrMsg]; 只會回傳ok與錯誤訊息，不用解析
//            NSString *strFeedback =
            id Json = [NSString stringWithUTF8String:[sourceData bytes]];
            
            if(!Json)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    failBlock(ErrMsg);
                });
                return;
            }
            
            NSLog(@"%@，下載完成!", self.url);
            dispatch_async(dispatch_get_main_queue(), ^{
                okBlock(Json);
            });
            
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
            dispatch_async(dispatch_get_main_queue(), ^{
                failBlock(@"傳輸發生問題，請稍候..");
            });
        }
        
    });
}

@end
