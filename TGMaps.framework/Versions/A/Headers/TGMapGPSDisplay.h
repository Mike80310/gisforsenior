//
//  MapGPSDisplay.h
//  SuperGIS Runtime
//
//  Created by Zonghan Chuang on 12/4/12.
//  Copyright (c) 2012年 supergeo.rd@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreLocation/CoreLocation.h>

@protocol IMapViewListener;
@protocol ILocationListener;

@interface TGMapGPSDisplay : NSObject <IMapViewListener, ILocationListener>

@property (nonatomic, retain) UIView *targetview;

@property (nonatomic, strong) UIImage *icon;

@property (nonatomic, assign) BOOL ShowAccuracyCircle;

@property (nonatomic, strong) UIColor *AccuracyCircleColor;

@property (nonatomic, assign) BOOL ShowDirectionArc;

@property (nonatomic, assign) CGFloat DirectionArcDegree;

@property (nonatomic, assign) CGFloat DirectionArcRadius;

@property (nonatomic, strong) UIColor *DirectionArcFillColor;

@property (nonatomic, strong) UIColor *DirectionArcOutlineColor;

@property (nonatomic, assign) CGFloat DirectionArcOutlineWidth;

@property (nonatomic, assign) BOOL DirectionArcGradientStyle;

@property (nonatomic, assign) BOOL ArcVisibility;

@end
