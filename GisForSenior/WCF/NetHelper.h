//
//  NetHelper.h
//  GisForSenior
//
//  Created by 珍珍 on 2016/2/2.
//  Copyright © 2016年 jane. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetHelper : NSObject

- (void) startConnectWithOKBlock:(void(^)(id Json))okBlock FailBlock:(void(^)(NSString* ErrMsg))failBlock;
- (instancetype)initSendAddressToServerWithPhoneKey:(NSString *) strPhoneKey regionNo:(NSString *)strRegionNo;

@end
