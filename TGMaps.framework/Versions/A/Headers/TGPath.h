//
//  TGOSPath.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/9.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TGPath : NSObject <NSCopying, NSMutableCopying>

/** Convenience constructor for an empty path. */
+ (instancetype)path;

+ (instancetype)pathFromEncodedPath:(NSString *)encodedPath;

/** Initializes a newly allocated path with the contents of another TGOSPath. */
- (id)initWithPath:(TGPath *)path;

/** Get size of path. */
- (NSUInteger)count;

/** Returns kCLLocationCoordinate2DInvalid if |index| >= count. */
- (CLLocationCoordinate2D)coordinateAtIndex:(NSUInteger)index;

- (NSString *)encodedPath;

@end
