//
//  TGOSPolygon.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/10.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TGPath;

@protocol TGPolygon

@property (nonatomic) TGPath *path;

@property (nonatomic, strong) UIColor *strokeColor;

@property (nonatomic, assign) int strokeWidth;

@property (nonatomic, strong) UIColor *fillColor;

@property (nonatomic, assign) BOOL visibility;

- (void)remove;

@end
