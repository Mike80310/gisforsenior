//
//  TGOSProjection.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/11.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <CoreGraphics/CoreGraphics.h>

/**
 * GMSVisibleRegion is returned by visibleRegion on GMSProjection,
 * it contains a set of four coordinates.
 */
typedef struct {
    CLLocationCoordinate2D nearLeft;
    CLLocationCoordinate2D nearRight;
    CLLocationCoordinate2D farLeft;
    CLLocationCoordinate2D farRight;
} TGVisibleRegion;


@interface TGProjection : NSObject

- (CGPoint)pointForCoordinate:(CLLocationCoordinate2D)coordinate;

- (CLLocationCoordinate2D)coordinateForPoint:(CGPoint)point;

- (bool)containsCoordinate:(CLLocationCoordinate2D)coordinate;

- (TGVisibleRegion)visibleRegion;

@end