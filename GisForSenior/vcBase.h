//
//  vcBase.h
//  GisForSenior
//
//  Created by jane on 2015/7/2.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TGMaps/TGMaps.h>
#import "GPSHelper.h"
#import "MMProgressHUD.h"

#define nHudExitCode 2033

@interface vcBase : UIViewController<UIGestureRecognizerDelegate>
{
    UIButton *btnLeft;
    UIButton *btnRight;
    UILabel* lblTitle;
    
    CGRect cgKeyBoard;
    
    CGSize screenSize;
    float fFontSize;
    
    GPSHelper *gH;
    CLLocationCoordinate2D defaultLocation;
    
    int nPvWidth;
    int nPv2Width;
    int nPv3Width;

}

@property (nonatomic, readonly) NSString* strLeftButtonImageName;
@property (nonatomic, readonly) NSString* strRightButtonImageName;

- (void) clickLeftButton:(id)sender;
- (void) clickRightButton:(id)sender;

-(void)resizeHeightForLabel: (UILabel*)label vFrame:(CGRect) cgFrame; 

#pragma mark - GPSHelper

- (void) getNowAddress;
- (void) stopUpdateCurrentLocation;

#pragma mark - 年份轉換
- (NSDateComponents*) getTaiwanDateComponentsWithyyyyMMdd:(NSString*)yyyyMMdd;
- (NSDateComponents*) getTaiwanDateComponentsWithDateString:(NSString*)strDate Format:(NSString*)strFmt;
- (NSDate *) getGregorianDate:(NSString*) strDate;

#pragma mark - HUD
- (void) HudErrMsgWithError:(NSString*)ErrMSg Title:(NSString*)title finishBlock:(void(^)(void))block;
- (void) HudDismissWithError:(NSString*)ErrMsg finishBlock:(void(^)(void))block Title:(NSString*)title;
- (void) HudDismissWithSuccess:(NSString*)Msg finishBlock:(void(^)(void))block Title:(NSString*)title;

- (void) HudShowMsg:(NSString*)Msg;
- (void) HudDissmiss;
- (void) HudErrMsgWithError:(NSString*)ErrMSg finishBlock:(void(^)(void))block;
- (void) HudDismissWithSuccess:(NSString*)Msg finishBlock:(void(^)(void))block;

- (void) HudDebugMsg:(NSString*)Msg OKMsg:(NSString*)okMsg okBlock:(void(^)(void))block;
- (void) HudDebugMsg:(NSString*)Msg ErrMsg:(NSString*)errMsg okBlock:(void(^)(void))block;

#pragma mark - 顏色
-(UIColor *)colorFromHexString:(NSString *)hexString;

@end
