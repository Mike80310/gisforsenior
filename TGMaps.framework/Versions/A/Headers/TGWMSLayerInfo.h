//
//  TGWMSLayerInfo.h
//  TGMaps
//
//  Created by supergeo.rd on 13/7/24.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TGEnvelope;


@interface TGWMSLayerInfo : NSObject

@property (nonatomic, readonly, copy) NSString *title;

@property (nonatomic, readonly, copy) NSString *name;

@property (nonatomic, readonly, copy) NSString *SRS;

@property (nonatomic, readonly, strong) TGEnvelope *envelope;

@property (nonatomic, readonly, copy) NSArray *subLayers;

@end
