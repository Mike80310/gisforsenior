//
//  vcLawCatalog.m
//  GisForSenior
//
//  Created by 珍珍 on 2015/12/17.
//  Copyright © 2015年 jane. All rights reserved.
//

#import "vcLawCatalog.h"
#import "vcLaw.h"

@interface vcLawCatalog ()
{
    NSArray *aryLawList;
    int nCornerRadius;
    int nCellHeight;
    int nFontSize;
}
@end

@implementation vcLawCatalog

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    nCornerRadius = kCornerRadius;
    nCellHeight = kCellHeight;
    nFontSize = fFontSize;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        nCornerRadius += 8;
        nCellHeight += 15;
        nFontSize += 2;
    }

    
    aryLawList = @[@{@"Name":@"居家式",@"PicName":@"introduction_01"},
                   @{@"Name":@"社區式",@"PicName":@"introduction_02"},
                   @{@"Name":@"機構住宿式",@"PicName":@"introduction_03"},
                   @{@"Name":@"家庭照顧者支持服務",@"PicName":@"introduction_04"},
                   ];
    //@{@"Name":@"其它",@"PicName":@"introduction_05"},
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else if (section == 1)
    {
        return 1;
    }
    else
    {
        return aryLawList.count;
    }
    return aryLawList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) return nCellHeight/2;
    if (indexPath.section == 1 || indexPath.section ==2) return nCellHeight + 15;
    return nCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *CellIdentifier;
    
    
    if (indexPath.section == 0)
    {
        CellIdentifier = [NSString stringWithFormat:@"CatalogTitleCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
        vContent.backgroundColor = [UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;

    }
    else if (indexPath.section == 1)
    {
        CellIdentifier = [NSString stringWithFormat:@"IntroduceCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSString *strTitle = @"依104年6月公告之長照服務法，\n長照服務依其提供方式區分如下：";
        
        UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
        lblTitleName.text = strTitle;
        lblTitleName.font = [UIFont boldSystemFontOfSize:nFontSize];
        
        return cell;
    }
    else
    {
        CellIdentifier = [NSString stringWithFormat:@"LawTitleCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
        CGRect cgTmp = vContent.frame;
        cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
        vContent.frame = cgTmp;
        
        vContent.layer.cornerRadius = nCornerRadius;
        vContent.layer.masksToBounds = YES;
        
        NSString *strTitle = [aryLawList[indexPath.row] objectForKey:@"Name"];
        
        UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
        lblTitleName.text = strTitle;
        lblTitleName.font = [UIFont boldSystemFontOfSize:nFontSize];
        
        return cell;
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0|| indexPath.section == 1) return;
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcLaw *vc = [storyBoard instantiateViewControllerWithIdentifier:@"vcLaw"];
    vc.strImageName = [aryLawList[indexPath.row] objectForKey:@"PicName"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction) clickOpenUrl:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://www.mohw.gov.tw/cht/ltc/"];
    [[UIApplication sharedApplication] openURL:url];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
