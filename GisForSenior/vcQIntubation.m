//
//  vcQIntubation.m
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcQIntubation.h"
#import "vcMapMain.h"

@interface vcQIntubation ()

@end

@implementation vcQIntubation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    float fBtnRadius = 20;
    float fVRadius = 5;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.lblQuestionTitle.font =
        [UIFont boldSystemFontOfSize:fFontSize + 5];
        
        CGRect  cgBtn = self.btnNone.frame;
        cgBtn.size.height = barHeightInPlus;
        self.btnNone.frame = cgBtn;
        
        cgBtn = self.btnFirst.frame;
        cgBtn.origin.y += 10;
        cgBtn.size.height = barHeightInPlus;
        self.btnFirst.frame = cgBtn;
        
        cgBtn = self.btnFourth.frame;
        cgBtn.origin.y += 10;
        cgBtn.size.height = barHeightInPlus;
        self.btnFourth.frame = cgBtn;
        
        cgBtn = self.btnSecond.frame;
        cgBtn.origin.y += 10;
        cgBtn.size.height = barHeightInPlus;
        self.btnSecond.frame = cgBtn;
        
        cgBtn = self.btnThird.frame;
        cgBtn.origin.y += 10;
        cgBtn.size.height = barHeightInPlus;
        self.btnThird.frame = cgBtn;
        
        
        
        CGRect cgBtnYes = self.btnYES.frame;
        cgBtnYes.origin.y -= 10;
        cgBtnYes.size.height = barHeightInPlus;
        self.btnYES.frame = cgBtnYes;
        
        fBtnRadius = barRadiusInPlus;
        fVRadius = 8;
    }
    
    self.vQuestionBG.layer.cornerRadius = fVRadius;
    
    self.btnNone.layer.cornerRadius =
    self.btnYES.layer.cornerRadius =
    self.btnFirst.layer.cornerRadius =
    self.btnThird.layer.cornerRadius =
    self.btnSecond.layer.cornerRadius =
    self.btnFourth.layer.cornerRadius =fBtnRadius;
}

- (IBAction) clickBtn:(id)sender
{
    UIButton *btn = (UIButton *) sender;
    [btn setSelected:!btn.selected];
    [self changeBtnColor:btn];
    
    if (btn.tag == 100)
    {
        for (UIView *vTmp in [self.vQuestionBG subviews])
        {
            if (![vTmp isKindOfClass:[UIButton class]] || vTmp.tag == 102 || vTmp.tag == 100) continue;
            
            UIButton *btnTmp = (UIButton *) vTmp;
            btnTmp.selected = NO;
            [self changeBtnColor:btnTmp];
        }
    }
    else if (btn.tag == 101)
    {
        for (UIView *vTmp in [self.vQuestionBG subviews])
        {
            if (![vTmp isKindOfClass:[UIButton class]] || vTmp.tag == 101 || vTmp.tag == 102) continue;
            
            UIButton *btnTmp = (UIButton *) vTmp;
            btnTmp.selected = NO;
            [self changeBtnColor:btnTmp];
        }
    }
    else
    {
        for (UIView *vTmp in [self.vQuestionBG subviews])
        {
            if (![vTmp isKindOfClass:[UIButton class]] || (vTmp.tag != 101 && vTmp.tag != 100)) continue;
            
            UIButton *btnTmp = (UIButton *) vTmp;
            btnTmp.selected = NO;
            [self changeBtnColor:btnTmp];
        }
    
    }

}

- (void) changeBtnColor:(UIButton *) btn
{
    if (btn.selected)
    {
        btn.backgroundColor = clrMaker(255, 205, 0, 1);
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    }
    else
    {
        btn.backgroundColor = clrMaker(220, 221, 221, 1);
    }

}

-(IBAction) clickYES:(id)sender
{
    int nCount = 0;
    for (UIView *vTmp in [self.vQuestionBG subviews])
    {
        if (![vTmp isKindOfClass:[UIButton class]]) continue;
    
        UIButton *btn = (UIButton *)vTmp;
        if (btn.tag == 101 && btn.isSelected)
        {
            nCount = 4;
            break;
        }
        if (btn.tag == 100 && btn.isSelected)
        {
            nCount = 0;
            break;
        }
        else
        {
            if (btn.isSelected) nCount++;
        }
    }
    
    NSLog(@"目前插管數為 %i",nCount);
    
//    if (nCount == 0) return;
//    
//    NSLog(@"插管數為0 不動作");
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%i",nCount] forKeyPath:kDicKeyIntubation];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self pushToNextQuestion];
}

- (void) pushToNextQuestion
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcMapMain* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcMapMain"];
    vc.strPrePage = @"vcQuestion";
    vc.title = @"查詢結果";
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
