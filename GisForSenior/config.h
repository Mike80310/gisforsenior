//
//  Header.h
//  CTBC
//
//  Created by jane on 2015/5/9.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#ifndef GisForSenior_Header_h
#define GisForSenior_Header_h

#define clrFloatGetter(num) (num/255.f > 1 ? 1 : num/255.f)
#define clrMaker(R, G, B, A ) [UIColor colorWithRed:clrFloatGetter(R) green:clrFloatGetter(G) blue:clrFloatGetter(B) alpha:A ]

#define clrOrange clrMaker(250.f, 105.f, 0.f, 1)

#define isUpper9 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0f)

#define strMapOriIcon @"bt_map02"
#define strMapTapIcon @"bt_map01"
#define kBackIcon @"btn_return"
#define kMapIcon @"btn_map"
#define kListIcon @"btn_list"

#define kI6ScreenHeight 667 

#define strDefaultAddr @"現在位置"

#define barHeightInPlus 50
#define barRadiusInPlus 25
#define kCellHeight 40
#define kCornerRadius 20
#define kArrow 25
#define kCellInterval 5
#define kTxtInterval 10

#define kUUID @"UUID"

//#define provisingKey @"JS398N24SG" //衛福部 - dis
#define provisingKey @"A257SRZZE7" //鴻鼎 - dis

#define kDicKeyDisabilities @"Disabilities"//身障
#define kDicKeyAborigines @"Aborigines"//原住民
#define kDicKeyAge @"Age"//年齡
#define kDicKeyCareTime @"CareTime"//照護時段
#define kDicKeyIntubation @"Intubation"//是否有插管
#define kDicKeyAdlLevel @"AdlLevel"//生活能力等級

#define kDefaultRegionKey @"Region"
#define kDefaultCareCenterKey @"CareCenter"
#define kDefaultUserLatLng @"LatLng"

//#define kServiceUrl @"http://60.251.183.20/ltcpa_web/WebService.asmx"
#define kServiceUrl @"http://ltcgis.mohw.gov.tw/WebService.asmx"
#define kServiceUrlForSendAddress @"http://ltcgis.mohw.gov.tw/WebService"

#define kServiceRegionList @"getRegionList"
#define kServiceCareCenterTypeList @"getCareCenterTypeList"
#define kServiceCareCenterList @"getCareCenterList"
#define kServiceSurveyResult @"getSurveyResult"
#define kServiceCareCenter @"getCareCenter"
#define kSerivceCareCenterInfo @"getCareCenterByCenterID"

#define fDelayOfOKMsg 0.5f
#define fDelayOfErrMsg 1.f

#define kBorderColorID @"#0078ff"
#define pickerHeight 180 //picker本身的高度 也是放pk的cell的高度
#define pickerRowHeight 40 //在picker的row高度


#define kArea @"Area" //區域
#define kCity @"City" //縣市

#define kRegionName @"Name"
#define kRegionParentNo @"ParentNo"
#define kRegionNo @"No"

#define kFirstOpenFeedBack @"FirstOpenFeedBack"


#endif
