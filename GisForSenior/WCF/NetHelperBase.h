//
//  NetHelperBase.h
//  GisForSenior
//
//  Created by 珍珍 on 2016/2/2.
//  Copyright © 2016年 jane. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetHelperBase : NSObject

@property (nonatomic, readonly) NSURL* url;

@property (nonatomic, retain) NSString* Function;
@property (nonatomic, retain) NSString* parameter;
@property (nonatomic, readonly) NSString *SvcUrlString;

- (void) postWithHttpBody:(NSData*)bodyData OKBlock:(void(^)(id))okBlock FailBlock:(void(^)(id))failBlock;

@end
