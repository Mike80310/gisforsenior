//
//  vcFilterResult.h
//  GisForSenior
//
//  Created by jane on 2015/9/5.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"
#import "SoapTool.h"
#import <TGMaps/TGMaps.h>

@interface vcFilterResult : vcBase<UITableViewDataSource,UITableViewDelegate,TGMapViewDelegate,UIGestureRecognizerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,SOAPToolDelegate>
{
    UITapGestureRecognizer* gestCenterInfoTap;
}

@property (nonatomic,weak) IBOutlet UIView *vFilter;
@property (nonatomic,weak) IBOutlet UITableView *tvFilter;
@property (nonatomic,weak) IBOutlet UITableView *tvResult;
@property (nonatomic,retain) NSString *strCityArea;
@property (nonatomic,retain) NSArray *aryShowInfo;//篩選後顯示的資料
@property (nonatomic,retain) NSArray *aryServiceList;

@property (nonatomic,weak) IBOutlet UIView *vMap;
@property (nonatomic,weak) IBOutlet UIView *vCenterInfoBG;
@property (nonatomic,weak) IBOutlet UIView *vCenterInfo;
@property (nonatomic,weak) IBOutlet UILabel *lblCenterName;
@property (nonatomic,weak) IBOutlet UIButton *btnPathPlanning;
@property (nonatomic,weak) IBOutlet UIButton *btnFilter;

@property (nonatomic,retain) NSDictionary *dicPrePageSetting;

@end
