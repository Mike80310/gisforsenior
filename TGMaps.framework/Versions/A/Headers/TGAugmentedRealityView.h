//
//  TGOSAugmentedRealityView.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/30.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TGAugmentedRealityView;
@class TGAugmentedRealityMarkerOptions;
@class CLLocation;
@protocol TGAugmentedRealityMarker;

@protocol TGAugmentedRealityViewDelegate <NSObject>

@optional

- (void)ARView:(TGAugmentedRealityView *)ARView
    didTapMarker:(id<TGAugmentedRealityMarker>)marker;

@end



@interface TGAugmentedRealityView : UIView

@property (nonatomic, weak) id<TGAugmentedRealityViewDelegate> delegate;

/** This property is observable using KVO. */
@property (nonatomic, strong, readonly) CLLocation *myLocation;

@property (nonatomic, assign) CGFloat visibleDistance;

@property (nonatomic, assign) BOOL radarVisibility;

@property (nonatomic, strong) UIColor *radarColor;

@property (nonatomic, strong) UIColor *radarArcColor;

@property (nonatomic, assign) BOOL isLocationCustomizable;

@property (nonatomic, assign) CLLocationCoordinate2D userLocation;

+ (TGAugmentedRealityView *)viewWithFrame:(CGRect)frame;

- (id<TGAugmentedRealityMarker>)addMarkerWithOptions:(TGAugmentedRealityMarkerOptions *)options;

- (NSArray *)markers;

@end
