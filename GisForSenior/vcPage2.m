//
//  vcPage2.m
//  GisForSenior
//
//  Created by jane on 2015/9/9.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcPage2.h"
#import "vcPage3.h"

#define FIRST_YEAR 1911
#define pickerRowHeight 40
#define defaultAge 60

#define kDisabileities @"領有殘障手冊/證明"
#define kAborigines @"山地原住民"

@interface vcPage2 ()
{
    NSDateFormatter *formatter;
    NSDateComponents *cmt;
    int nAge;
    
    NSMutableArray *aryYears;
    NSMutableArray *aryMonths;
    NSMutableArray *aryDays;
    
    int todayYear;
    int todayMonth;
    int todayDay;
    
    int rowOfTodayYearFirst;
    int rowOfTodayMonthFirst;
    int rowOfTodayDayFirst;
    
    int nCloseCell;
    
    NSArray *aryID;
    NSArray *arySelectedID;
    
    //cell的layout設定
    int nCellHeight;
    int nCornerRadius;
    int nArrow;
    int nCellInterval;
    
    NSCalendar* calendarForPvMain;
    
    int rowOfYear; // 年を取得
    int rowOfMonth; // 月を取得
    int rowOfDay;
    
    NSString *strNowBirthday;
}
@end

@implementation vcPage2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view;
    
    arySelectedID = [[NSArray alloc] init];
    int nOldAge = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyAge] intValue];
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyAge])
    {
        nAge = nOldAge;
        
        NSMutableArray *aryTmp = [NSMutableArray new];
        BOOL bIsDisabileities = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyDisabilities] boolValue];
        BOOL bIsAborigines = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyAborigines] boolValue];
        
        if (bIsDisabileities)
        {
            [aryTmp addObject:@{@"IdNo":@"2222",@"IdName":kDisabileities}];
        }
        if (bIsAborigines)
        {
            [aryTmp addObject:@{@"IdNo":@"3333",@"IdName":kAborigines}];
        }
        
        arySelectedID = aryTmp;

    }
    else
    {
        nAge = -1;
    }
    
    
    
    CGRect cgBtnYes = self.btnNext.frame;
    cgBtnYes.origin.y -= 10;
    cgBtnYes.size.height = barHeightInPlus;
    self.btnNext.frame = cgBtnYes;
    self.btnNext.layer.cornerRadius = barHeightInPlus/2;
    
    calendarForPvMain = [NSCalendar currentCalendar];
    
    nCloseCell = -1;
    strNowBirthday = @"生日：民國 年 月 日";
    rowOfYear = -1;
    rowOfMonth = -1;
    rowOfDay = -1;
    rowOfTodayDayFirst = -1;
    rowOfTodayMonthFirst = -1;
    rowOfTodayYearFirst = -1;

    aryID = @[@{@"IdNo":@"2222",@"IdName":kDisabileities},
              @{@"IdNo":@"3333",@"IdName":kAborigines},
              @{@"IdNo":@"1111",@"IdName":@"無"}];
    
    
    nCellHeight = kCellHeight;
    nCornerRadius = kCornerRadius;
    nArrow = kArrow;
    nCellInterval = kCellInterval;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        nCellHeight +=15;
        nCornerRadius += 8;
        nArrow += 10;
        nCellInterval += kCellInterval;
    }
    
    formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyyMMdd";
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [self setPickerviewDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushToNextQuestion:(id)sender
{
    
    if (rowOfTodayMonthFirst != -1 && ![[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyAge])
    {
        [self HudErrMsgWithError:@"請設定生日資料" finishBlock:^(void){
            
        }];
        
        return;
    }
    
    BOOL bIsDisabilities = false;
    BOOL bIsAborigines = false;
    
    for (NSDictionary *dicID in arySelectedID)
    {
        if ([[dicID objectForKey:@"IdName"] isEqual:kDisabileities])
        {
            bIsDisabilities = YES;
        }
        else if ([[dicID objectForKey:@"IdName"] isEqual:kAborigines])
        {
            bIsAborigines = YES;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%i",nAge]
                                         forKeyPath:kDicKeyAge];
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%i",bIsDisabilities]
                                         forKeyPath:kDicKeyDisabilities];
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%i",bIsAborigines]
                                         forKeyPath:kDicKeyAborigines];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcPage3* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcPage3"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;//非可隱藏的Cell全部都獨立一個Section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (nCloseCell == -1)
    {
        if (section == 0)
        {
            return 1;
        }
        else if(section == 1)
        {
            return 2;
        }
        else  if (section == 2)
        {
            return 1 + aryID.count;
        }
    }
    else
    {
        if (nCloseCell == section) return 1;
        
        if (section == 1)
        {
            return 2;
        }
        else if (section == 2)
        {
            return 1 + aryID.count;
        }
        
    }
   
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return nCellHeight/2;
    }
    
    if (indexPath.section == 1 || indexPath.section == 6)
    {
        if (indexPath.row == 1 && indexPath.section != 6) return pickerHeight;

        return nCellHeight;
        
    }
    
    return nCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *CellIdentifier;
    
    int nSection = (int)indexPath.section;
    int nRow = (int)indexPath.row;
    
    if (nSection == 0)//關鍵字選擇
    {
        CellIdentifier = [NSString stringWithFormat:@"TitleCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
        vContent.backgroundColor = [UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if (nSection == 1)
    {
        if (nRow == 0)
        {
            CellIdentifier = [NSString stringWithFormat:@"BirthdayTitleCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            NSString *strTitle = [NSString stringWithFormat:@"生日：民國%i年%02i月%02i日",
                                  [[aryYears objectAtIndex:rowOfYear] intValue],
                                  [[aryMonths objectAtIndex:rowOfMonth] intValue],
                                  [[aryDays objectAtIndex:rowOfDay] intValue]];
            
            return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:strTitle];
            
        }
        else
        {
            CellIdentifier = [NSString stringWithFormat:@"BirthdaySCCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.backgroundColor =[UIColor clearColor];
            
            [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:301];
        }
        
    }
    else if (nSection == 2)
    {
        if (nRow == 0)
        {
            CellIdentifier = [NSString stringWithFormat:@"IdTitleCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"請問您是否有以下身份(可複選)"];
        }
        else
        {
            CellIdentifier = [NSString stringWithFormat:@"IdItemCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            [self setItemCell:cell cellForRowAtIndexPath:indexPath];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1 || indexPath.section == 2)
    {
        if (nCloseCell == -1) //Cell都已經展開
        {
            if (indexPath.section == 2 && indexPath.row != 0)
            {
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                arySelectedID = [self setArySelectedIdList:(int)cell.tag];
                [self.tvMain reloadData];
                return;
            }
            
            NSArray *aryServiceItem = [[NSArray alloc] init];
            
            if (indexPath.section == 2)
            {
                aryServiceItem = aryID;
            }
            
            nCloseCell = (int)indexPath.section;
            int nTmp = 1 + (int)aryServiceItem.count;
            [self deleteCell:indexPath amount:nTmp];
            return;
        }
        
        if (indexPath.section == nCloseCell && indexPath.row == 0) //把已經關閉的Cell展開
        {
            
            [self SpreadClassCell:indexPath];
            
            return;
        }
        
        if (indexPath.row == 0)
        {
            [self SpreadClassCell:indexPath];
        }
        
        if (indexPath.section == 2 && indexPath.row != 0)
        {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            arySelectedID = [self setArySelectedIdList:(int)cell.tag];
            [self.tvMain reloadData];
            return;
        }

    }
}

-(NSArray *) setArySelectedIdList:(int) nItemNo
{
    NSMutableArray *aryNewSelected = [arySelectedID mutableCopy];
    BOOL bAlreadySelected = false;
    int nIndex = -1;
    
    if(nItemNo == 1111)
    {
        return [NSArray new];
    }
    else
    {
        for (int i = 0; i < aryNewSelected.count; i++)
        {
            NSDictionary *dicItem = aryNewSelected[i];
            if (nItemNo != [[dicItem objectForKey:@"IdNo"] intValue]) continue;
            
            bAlreadySelected = YES;
            nIndex = i;
            
            [aryNewSelected removeObject:dicItem];
            break;
        }
        
        if (bAlreadySelected)
        {
            return aryNewSelected;
        }
        else
        {
            
            for (NSDictionary *dicItem in aryID)
            {
                if (nItemNo != [[dicItem objectForKey:@"IdNo"] intValue]) continue;
                
                [aryNewSelected addObject:dicItem];
                
            }
            
            NSArray *aryTmp = aryNewSelected;
            
            NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"ItemNo" ascending:YES];
            NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
            aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
            
            return aryTmp;
            
        }

    }
    
    return [NSArray new];
}


#pragma mark - 各式Cell
- (UITableViewCell *) setTitleCell:(UITableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath titleName:(NSString *) strName
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    vContent.layer.cornerRadius = nCornerRadius;
    vContent.layer.masksToBounds = YES;
    vContent.layer.borderWidth = 1;
    vContent.layer.borderColor = [self colorFromHexString:kBorderColorID].CGColor;
    
    UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
    lblTitleName.text = strName ;
    
    UIImageView *imgArrow = (UIImageView *) [cell.contentView viewWithTag:102];
    
    CGRect cgImg = imgArrow.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    cgImg.origin.y = (nCellHeight - nArrow) /2;
    cgImg.origin.x = cgTmp.size.width - nArrow*4/3;
    imgArrow.frame = cgImg;
    
    CGRect cgTitle = lblTitleName.frame;
    cgTitle.origin.y = cgTmp.origin.y;
    cgTitle.origin.x = cgTmp.origin.x;
//    cgTitle.size.width = cgTmp.size.width - (cgTmp.size.width - cgImg.origin.x);
    cgTitle.size.height = cgTmp.size.height;
    lblTitleName.frame = cgTitle;
    lblTitleName.font = [UIFont systemFontOfSize:fFontSize];
    
    return cell;
}

- (UITableViewCell *) setScCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath ScrollViewTag:(int) nTag
{

    UIPickerView *pv303 = (UIPickerView *) [cell.contentView viewWithTag:303];
    pv303.backgroundColor = [UIColor whiteColor];
    
    pv303.delegate = self;
    pv303.dataSource = self;
    CGRect cgPV =pv303.frame;
    cgPV.size.width = cell.contentView.frame.size.width - cgPV.origin.x * 2;
    cgPV.size.height = pickerHeight;

    pv303.frame = cgPV;
    
    if (rowOfTodayMonthFirst != -1)
    {
        [pv303 selectRow:rowOfTodayYearFirst inComponent:0 animated:YES];
        [pv303 selectRow:rowOfTodayMonthFirst inComponent:1 animated:YES];
        [pv303 selectRow:rowOfTodayDayFirst inComponent:2 animated:YES];
        
        [self setYearMonthBarText:[NSString stringWithFormat:@"生日：民國%i年%02i月%02i日",
                                   [[aryYears objectAtIndex:rowOfTodayYearFirst] intValue],
                                   [[aryMonths objectAtIndex:rowOfTodayMonthFirst] intValue],
                                   [[aryDays objectAtIndex:rowOfTodayDayFirst]intValue]]];
        rowOfMonth = rowOfTodayMonthFirst;
        rowOfYear = rowOfTodayYearFirst;
        rowOfDay = rowOfTodayDayFirst;
        
        rowOfTodayDayFirst =
        rowOfTodayMonthFirst =
        rowOfTodayYearFirst = -1;
    }
    
    return cell;
}

- (UITableViewCell *) setItemCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    
    int nRow = (int)indexPath.row;
    
    UIImageView* imgSelect = (UIImageView *) [cell.contentView viewWithTag:101];
    CGRect cgImg = imgSelect.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    imgSelect.frame = cgImg;
    
    UILabel *lblItemName = (UILabel *) [cell.contentView viewWithTag:102];
    CGRect cgLabel = lblItemName.frame;
    cgLabel.size.height = nCellHeight;
    cgLabel.origin.x = cgImg.origin.x + cgImg.size.width + 5;
    cgLabel.origin.y = 0;
    lblItemName.frame = cgLabel;
    [lblItemName setFont:[UIFont systemFontOfSize:fFontSize]];
    
    NSString *strItemName = [[aryID objectAtIndex:nRow-1] objectForKey:@"IdName"];
    [lblItemName setText:strItemName];
    
    int nItemNo = [[[aryID objectAtIndex:nRow-1] objectForKey:@"IdNo"] intValue];
    cell.tag = nItemNo;
    
    BOOL bIsSelected = false;
    
    if (arySelectedID.count == 0 && indexPath.row == 3)
    {
        bIsSelected = YES;
    }
    else
    {
        for (int i = 0; i < arySelectedID.count; i++)
        {
            NSLog(@"%@",arySelectedID[i]);
            
            
            NSLog(@"IdNo %i", [[arySelectedID[i] objectForKey:@"IdNo"] intValue]);
            if (cell.tag != [[arySelectedID[i] objectForKey:@"IdNo"] intValue]) continue;
            
            bIsSelected = YES;
        }

    }
    
    
    if (bIsSelected)
    {
        [imgSelect setImage:[UIImage imageNamed:@"check01"]];
    }
    else
    {
        [imgSelect setImage:[UIImage imageNamed:@"check02"]];
    }
    
    return cell;
}

#pragma mark - bar名稱改變

- (void) changeCellTitle:(NSString *) strNewTitle Section:(int) nSection
{
    
    UITableViewCell *cell = [self.tvMain cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:nSection]];
    
    for (UIView *v100 in cell.contentView.subviews) {
        
        for(UIView *vTmp in [v100 subviews])
        {
            if (![vTmp isKindOfClass:[UILabel class]]) continue;
            if (vTmp.tag == 500) continue;
            
            
            UILabel *lbl = (UILabel *) vTmp;
            lbl.text = strNewTitle;
        }
        
    }
    
}


#pragma mark - cell的展開與關閉

- (void) SpreadClassCell:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == nCloseCell)
    {
        [self imageRotateAction:NO indexPath:indexPath];//NO是展開
        
        nCloseCell = -1;
        if (indexPath.section ==2)
        {
            int nTmp = (int)aryID.count + 1;
            [self creatCell:indexPath amount:nTmp];
        }
        else
        {
            [self creatCell:indexPath amount:1];
        }
    }
    else
    {
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:nCloseCell];
        [self imageRotateAction:NO indexPath:ip];
        [self imageRotateAction:YES indexPath:indexPath];
        
        int nInsertAmount = 1;
        int nRemoveAmount = 1;
        
        if (indexPath.section == 2)
        {
            nRemoveAmount = (int)aryID.count + 1;
        }
        else if(nCloseCell == 2)
        {
            nInsertAmount = (int)aryID.count + 1;
        }
        
        nCloseCell = (int)indexPath.section;
        
        [self updateTableView:indexPath RemoveAmount:nRemoveAmount InsertIndex:ip InsertAmount:nInsertAmount];

    }
    
    
}

- (void) deleteCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    [self imageRotateAction:YES indexPath:indexPath];
    NSMutableArray* aryDelete = [NSMutableArray new];
    if(nAmount == 1)
    {
        [aryDelete addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryDelete addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvMain beginUpdates];
    [self.tvMain deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    [aryDelete removeAllObjects];
}

- (void) creatCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    NSMutableArray* aryInsert = [NSMutableArray new];
    
    if(nAmount == 1)
    {
        [aryInsert addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryInsert addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvMain beginUpdates];
    [self.tvMain insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    
    [aryInsert removeAllObjects];
}

- (void) updateTableView:(NSIndexPath *) removeIndex RemoveAmount:(int) nRemoveAmount
             InsertIndex:(NSIndexPath *)insertIndex InsertAmount:(int) nInsertAmount
{
    NSMutableArray* aryDelete = [NSMutableArray new];
    NSMutableArray* aryInsert = [NSMutableArray new];
    
    NSIndexPath *ip;
    
    if (nRemoveAmount == 1)
    {
        ip = [NSIndexPath indexPathForRow:1 inSection:removeIndex.section];
        [aryDelete addObject:ip];
        
    }
    else
    {
        for (int i = 1; i < nRemoveAmount; i++)
        {
            [aryDelete addObject:[NSIndexPath indexPathForRow:i inSection:removeIndex.section]];
        }
    }
    
    if (nInsertAmount == 1)
    {
        ip = [NSIndexPath indexPathForRow:1 inSection:insertIndex.section];
        [aryInsert addObject:ip];
    }
    else
    {
        for (int i = 1; i < nInsertAmount; i++)
        {
            [aryInsert addObject:[NSIndexPath indexPathForRow:i inSection:insertIndex.section]];
        }
    }
    
    
    [self.tvMain beginUpdates];
    [self.tvMain deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    
//    int nTmp = nSpreadCell;
//    nSpreadCell = 2;
    
    [self changeCellTitle:@"是否有以下身份(可複選)" Section:2];
//    nSpreadCell = nTmp;
    
    [aryInsert removeAllObjects];
    [aryDelete removeAllObjects];
    
}

#pragma mark - Arrow動畫

- (void) imageRotateAction:(BOOL)isRotate indexPath:(NSIndexPath *) indexPath
{
    UITableViewCell *cell = [self.tvMain cellForRowAtIndexPath:indexPath];
    
    for (UIView *v100 in cell.contentView.subviews)
    {
        
        for(UIView *vTmp in [v100 subviews])
        {
            if (![vTmp isKindOfClass:[UIImageView class]]) continue;
            
            UIImageView *imgArrow = (UIImageView *)vTmp;
            
            [UIView animateWithDuration:.35
                             animations:^
                             {
                                 if (isRotate)
                                 {
                                     imgArrow.transform = CGAffineTransformIdentity;
                                     
                                 }else
                                 {
                                     imgArrow.transform = CGAffineTransformMakeRotation(180 * M_PI / 180);//M_PI_2 轉四分之一
                                 }
                                 
                             }
                             completion:^(BOOL finished)
                             {
                                 
                             }];
            
        }
        
    }
    
    
}



#pragma mark - pickerview default

- (void) setPickerviewDefault
{
    NSDateComponents *components = [calendarForPvMain components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                        fromDate:[NSDate date]];
    
    todayYear  = (int)components.year;   // 現在の年を取得
    todayMonth = (int)components.month;  // 現在の月を取得
    todayDay = (int) components.day;
    
    aryYears = [[NSMutableArray alloc] init];
    for (int i = FIRST_YEAR; i <= todayYear; i++) {
        [aryYears addObject:[NSString stringWithFormat:@"%d", i-1911]];
    }
    
    aryMonths = [[NSMutableArray alloc] initWithCapacity:12];
    for (int i = 1; i <= 12; i++) {
        [aryMonths addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    aryDays = [self getDaysOfMonth:todayMonth Year:todayYear];
    
    rowOfYear = rowOfTodayYearFirst  = todayYear  - FIRST_YEAR - defaultAge;    // 年の列数を計算
    rowOfMonth = rowOfTodayMonthFirst = todayMonth - 1;             // 月の列数を計算
    rowOfDay = rowOfTodayDayFirst = todayDay -1;
    
//    [self.pvMain selectRow:rowOfTodayYear  inComponent:0 animated:YES];  // 年を選択
//    [self.pvMain selectRow:rowOfTodayMonth inComponent:1 animated:YES]; // 月を選択
    
    [self setYearMonthBarText:[NSString stringWithFormat:@"生日：民國%i年%02i月%02i日",
                               [[aryYears objectAtIndex:rowOfTodayYearFirst] intValue],
                               [[aryMonths objectAtIndex:rowOfTodayMonthFirst] intValue],
                               [[aryDays objectAtIndex:rowOfTodayDayFirst]intValue]]];
    
    NSDictionary *dicAgeInfo =
    [self getAgeInfoFromYear:[[aryYears objectAtIndex:rowOfTodayYearFirst] intValue]
                       Month:[[aryMonths objectAtIndex:rowOfTodayMonthFirst] intValue]
                         Day:[[aryDays objectAtIndex:rowOfTodayDayFirst] intValue]];
    
    
    
    nAge = [[dicAgeInfo objectForKey:@"Year"] intValue];
    
    NSLog(@"年齡為 %i",nAge);
    
}

- (NSMutableArray *) getDaysOfMonth:(int)nMonth Year:(int) nYear
{
    
    NSDate *dateTmp = [formatter dateFromString:[NSString stringWithFormat:@"%i%02i%02i",
                                                 nYear,
                                                 nMonth,
                                                 1]];

    int nDays = (int) [[NSCalendar currentCalendar] rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit
                                                        forDate:dateTmp].length;
    //  当前时间对应的月份中有几
    
    
    NSMutableArray *aryDay = [[NSMutableArray alloc] init];
    
    for (int i = 1; i < nDays+1; i++)
    {
        [aryDay addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    return aryDay;
}

#pragma mark - 年齡計算

- (NSDictionary* ) AgefromDate:(NSDate*)date
{
    NSString *strMyDate = [formatter stringFromDate:date];
    NSString *strNowDate = [formatter stringFromDate:[NSDate date]];
    
    NSDate *myDate = [formatter dateFromString:strMyDate];
    NSDate *nowDate = [formatter dateFromString:strNowDate];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSRepublicOfChinaCalendar];
    NSDateComponents *comps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit |NSDayCalendarUnit
                                          fromDate:myDate toDate:nowDate options:0];
    
    int nYear = (int)[comps year];
    int nMonth = (int)[comps month];
    int nDay = (int)[comps day];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:
                         [NSNumber numberWithInt:nYear],@"Year",
                         [NSNumber numberWithInt:nMonth],@"Month",
                         [NSNumber numberWithInt:nDay],@"Day",nil];//
    
    //    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:nYear],@"Year",
    //                         [NSNumber numberWithInt:nMonth],@"Month",[NSNumber numberWithInt:nDay],@"Day", nil];
    
    return dic;
    //    [NSString stringWithFormat:@"歲數：%ld年又%ld個月",(long)year,(long)Month];
}


#pragma mark - pickview delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerRowHeight;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [aryYears count];
    }
    else if (component == 1)
    {
        return [aryMonths count];
    }
    else if(component ==2)
    {
        return [aryDays count];
    }
    
    return 0;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row  inComponent:(NSInteger)component
{
    if (component ==1)
    {
        int nYear = (int)[pickerView selectedRowInComponent:0];
        aryDays = [self getDaysOfMonth:[aryMonths[row]intValue]
                                  Year:[aryYears[nYear]intValue]];
        [pickerView reloadComponent:2];
    }
    
    // PickerViewの選択されている年と月の列番号を取得
    rowOfYear  = (int)[pickerView selectedRowInComponent:0]; // 年を取得
    rowOfMonth = (int)[pickerView selectedRowInComponent:1];          // 月を取得
    rowOfDay = (int) [pickerView selectedRowInComponent:2];
    
    NSDictionary *dicAgeInfo = [self getAgeInfoFromYear:[aryYears[rowOfYear]intValue]
                                                  Month:[aryMonths[rowOfMonth]intValue]
                                                    Day:[aryDays[rowOfDay] intValue]];
    nAge = [[dicAgeInfo objectForKey:@"Year"] intValue];
    
    NSLog(@"年齡為 %i",nAge);
    int nMonth = [[dicAgeInfo objectForKey:@"Month"] intValue];
    int nDay = [[dicAgeInfo objectForKey:@"Day"] intValue];
    
    if (nAge <= 0 && nMonth <= 0 && nDay<=0)
    {
        [self HudErrMsgWithError:@"生日年月不可大於本日！" finishBlock:^{
            
        }];
        
        int rowOfTodayYear  = todayYear  - FIRST_YEAR;    // 年の列数を計算
        int rowOfTodayMonth = todayMonth - 1;             // 月の列数を計算
        int rowOfTodayDay = todayDay - 1;
        [pickerView selectRow:rowOfTodayYear inComponent:0 animated:YES];  // 年を選択
        [pickerView selectRow:rowOfTodayMonth inComponent:1 animated:YES]; // 月を選択
        [pickerView selectRow:rowOfTodayDay inComponent:2 animated:YES];
    }
    else
    {
        [self setYearMonthBarText:[NSString stringWithFormat:@"生日：民國%i年%02i月%02i日",
                                   [[aryYears objectAtIndex:rowOfYear] intValue],
                                   [[aryMonths objectAtIndex:rowOfMonth] intValue],
                                   [[aryDays objectAtIndex:rowOfDay] intValue]]];
    }
    
}

- (NSDictionary *) getAgeInfoFromYear:(int)nYear Month:(int) nMonth Day:(int) nDay
{
    NSString *strTmp = [NSString stringWithFormat:@"%i%02i%02i",
                        nYear + 1911,
                        nMonth,
                        nDay];
    NSDate *dtTmp = [formatter dateFromString:strTmp];
    
    
    return [self AgefromDate:dtTmp];
}

-(void) setYearMonthBarText:(NSString *) strBirthdayTitle
{
    strBirthdayTitle = [strBirthdayTitle stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
//    self.lblYearMonthBar.text = strTitle;
//    strNowBirthday = strBirthdayTitle;
    [self changeCellTitle:[NSString stringWithFormat:@"%@",strBirthdayTitle] Section:1];
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return pickerView.frame.size.width/3;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* pickerLabel = (UILabel*)view;
    
    pickerLabel = [[UILabel alloc] init];
    pickerLabel.font = [UIFont systemFontOfSize:fFontSize];
    pickerLabel.textAlignment=NSTextAlignmentCenter;
    
    NSString *title;
    
    if(component == 0)
    {
        title = [NSString stringWithFormat:@"民國%@年",[aryYears objectAtIndex:row]];
    }
    else if(component == 1)
    {
        title = [NSString stringWithFormat:@"%@月",[aryMonths objectAtIndex:row]];
    }
    else if (component ==2)
    {
        title = [NSString stringWithFormat:@"%@日",[aryDays objectAtIndex:row]];
    }

    pickerLabel.text = title;
    
    return pickerLabel;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
