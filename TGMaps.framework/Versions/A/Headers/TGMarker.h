//
//  TGOSMarker.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/10.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TGMarker

@property (nonatomic, assign) CLLocationCoordinate2D position;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *snippet;

@property (nonatomic, strong) UIImage *icon;

@property (nonatomic, assign) CGPoint groundAnchor;

@property (nonatomic, assign) CGPoint infoWindowAnchor;

@property (nonatomic, assign) BOOL tappable;

@property (nonatomic, assign) BOOL draggable;

@property (nonatomic, assign) BOOL visibility;

@property (nonatomic, strong) id userData;

- (BOOL)isInfoWindowShown;

- (void)showInfoWindow;

- (void)hideInfoWindow;

- (void)remove;

@end
