//
//  NetHelper.m
//  GisForSenior
//
//  Created by 珍珍 on 2016/2/2.
//  Copyright © 2016年 jane. All rights reserved.
//

#import "NetHelper.h"
#import "NetHelperBase.h"

@implementation NetHelper
{
    NetHelperBase* nH;
    id postBody;
}


- (void) startConnectWithOKBlock:(void(^)(id Json))okBlock FailBlock:(void(^)(NSString* ErrMsg))failBlock
{
    [nH postWithHttpBody:postBody OKBlock:okBlock FailBlock:failBlock];

}

- (instancetype)initSendAddressToServerWithPhoneKey:(NSString *) strPhoneKey regionNo:(NSString *)strRegionNo
{
    self = [super init];
    if (self) {
        
        nH = [NetHelperBase new];
        
        nH.Function =@"sendAddress.ashx";
        nH.parameter = [NSString stringWithFormat:@"phoneKey=%@&regionNo=%@&phoneType=ios",strPhoneKey,strRegionNo];
    }
    return self;
}

@end
