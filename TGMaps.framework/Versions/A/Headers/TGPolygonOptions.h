//
//  TGOSPolygonOptions.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/10.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class TGPath;

@interface TGPolygonOptions : NSObject

@property (nonatomic) TGPath *path;

@property (nonatomic, strong) UIColor *strokeColor;

@property (nonatomic, assign) int strokeWidth;

@property (nonatomic, strong) UIColor *fillColor;

@property (nonatomic, assign) BOOL visibility;

/** Convenience constructor for default initialized options. */
+ (TGPolygonOptions *)options;

@end
