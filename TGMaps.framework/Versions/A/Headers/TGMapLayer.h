//
//  MapLayer.h
//  Copyright (c) 2013年 supergeo.rd@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TGGeoDataset.h"

@class TGDisplay;


@interface TGMapLayer : TGGeoDataset

@property (nonatomic, copy) NSString *Name;

@property (nonatomic, copy) NSString *Title;

@property (nonatomic, assign) BOOL Visible;

@property (nonatomic, assign) double MaximumScale;

@property (nonatomic, assign) double MinimumScale;

-(void)render :(TGDisplay*) display;

-(void)releaseMemory;

@end
