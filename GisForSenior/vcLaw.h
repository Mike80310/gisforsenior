//
//  vcLaw.h
//  GisForSenior
//
//  Created by jane on 2015/7/17.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcLaw : vcBase

@property (nonatomic,retain) NSString *strImageName;

@property (nonatomic,weak) IBOutlet UIImageView *img;
@property (nonatomic,weak) IBOutlet UIView *vImgBG;
@property (nonatomic,weak) IBOutlet UIPageControl *pcMain;

@property (nonatomic,weak) IBOutlet UIButton *btnPrev;
@property (nonatomic,weak) IBOutlet UIButton *btnNext;
@property (nonatomic,weak) IBOutlet UIButton *btnUrl;
@end
