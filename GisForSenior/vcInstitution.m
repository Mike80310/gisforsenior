//
//  vcInstitution.m
//  GisForSenior
//
//  Created by 珍珍 on 2015/12/14.
//  Copyright © 2015年 jane. All rights reserved.
//

#import "vcInstitution.h"

@interface vcInstitution ()
{
    NSArray *aryImage;
    int nIndex;
}
@end

@implementation vcInstitution

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    aryImage = [[NSArray alloc] initWithObjects:@"resource_01",@"resource_02",
                @"resource_03",@"resource_04", nil];
    
    self.btnPrev.hidden = YES;
    [self.pcMain setNumberOfPages:aryImage.count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) changePage:(id)sender
{
    UISwipeGestureRecognizer *gesture = (UISwipeGestureRecognizer*)sender;
    
    if (gesture.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if(nIndex == aryImage.count - 1) return;
        
        nIndex++;
        [self.pcMain setCurrentPage:nIndex];
        [self.img setImage:[UIImage imageNamed:[aryImage objectAtIndex:nIndex]]];
        [self CurlAnimationIsToNext:YES];
    }
    
    if (gesture.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if(nIndex == 0) return;
        
        nIndex--;
        [self.pcMain setCurrentPage:nIndex];
        [self.img setImage:[UIImage imageNamed:[aryImage objectAtIndex:nIndex]]];
        [self CurlAnimationIsToNext:NO];
    }
    
    if (nIndex == aryImage.count -1)
    {
        self.btnNext.hidden = YES;
    }
    else if(nIndex == 0)
    {
        self.btnPrev.hidden = YES;
    }
    else
    {
        self.btnPrev.hidden = NO;
        self.btnNext.hidden = NO;
    }
    
}

- (IBAction) clickPrev:(id)sender
{
    nIndex--;
    [self.pcMain setCurrentPage:nIndex];
    [self.img setImage:[UIImage imageNamed:[aryImage objectAtIndex:nIndex]]];
    [self CurlAnimationIsToNext:NO];
    
    self.btnPrev.hidden = NO;
    self.btnNext.hidden = NO;
    
    if (nIndex == 0)
    {
        self.btnPrev.hidden = YES;
    }
}

- (IBAction) clickNext:(id)sender
{
    nIndex++;
    [self.pcMain setCurrentPage:nIndex];
    [self.img setImage:[UIImage imageNamed:[aryImage objectAtIndex:nIndex]]];
    [self CurlAnimationIsToNext:YES];
    
    self.btnPrev.hidden = NO;
    self.btnNext.hidden = NO;
    
    if (nIndex == aryImage.count -1)
    {
        self.btnNext.hidden = YES;
    }
}

- (void) CurlAnimationIsToNext:(BOOL)isToNext
{
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.duration = 0.6;
    transition.delegate = self;
    transition.type = kCATransitionPush;
    transition.subtype = isToNext ? kCATransitionFromRight : kCATransitionFromLeft;
    
    [self.vImgBG.layer addAnimation:transition forKey:@""];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
