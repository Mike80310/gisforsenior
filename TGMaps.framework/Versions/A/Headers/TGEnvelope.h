//
//  Envelope.h
//  Copyright (c) 2012年 supergeo.rd@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TGEnvelope : NSObject

@property (nonatomic, assign) double MinX;

@property (nonatomic, assign) double MaxX;

@property (nonatomic, assign) double MinY;

@property (nonatomic, assign) double MaxY;

- (double)getCenterX;

- (double)getCenterY;

- (double)getHeight;

- (double)getWidth;

- (NSString*)toString;

@end
