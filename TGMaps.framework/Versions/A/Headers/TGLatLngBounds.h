//
//  TGOSCoordinateBounds.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/15.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "TGProjection.h"

@class TGPath;
/**
 * GMSCoordinateBounds represents a rectangular bounding box on the Earth's
 * surface. Is is immutable and can't be modified after construction.
 */
@interface TGLatLngBounds : NSObject

@property (readonly) CLLocationCoordinate2D northEast;

@property (readonly) CLLocationCoordinate2D southWest;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord1
           andCoordinate:(CLLocationCoordinate2D)coord2;

/**
 * Inits bounds that encompass |region|.
 */
- (id)initWithRegion:(TGVisibleRegion)region;

/**
 * Inits bounds that encompass |path|.
 */
- (id)initWithPath:(TGPath *)path;

/**
 * Allocates and returns a new GMSCoordinateBounds, representing
 * the current bounds extended to include the passed-in coordinate.
 */
- (TGLatLngBounds *)including:(CLLocationCoordinate2D)coordinate;

/**
 * Returns YES if |coordinate| is contained within the bounds.
 */
- (BOOL)containsCoordinate:(CLLocationCoordinate2D)coordinate;

@end
