//
//  vcQCareTime.h
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcQCareTime : vcBase

@property (nonatomic,weak) IBOutlet UIView *vQuestionBG;
@property (nonatomic,weak) IBOutlet UILabel *lblQuestionTitle;

@property (nonatomic,weak) IBOutlet UIButton *btnHalfDay;
@property (nonatomic,weak) IBOutlet UIButton *btnAllDay;
@property (nonatomic,weak) IBOutlet UIButton *btnAnyTime;

@end
