//
//  vcQuestion2.m
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcQAborigines.h"
//#import "vcQAge.h"
#import "vcQIntubation.h"
#import "vcMapMain.h"

@interface vcQAborigines ()

@end

@implementation vcQAborigines

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    float fBtnRadius = 20;
    float fVRadius = 5;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.lblQuestionTitle.font =
        [UIFont boldSystemFontOfSize:fFontSize + 5];
        
        CGRect cgBtnNo = self.btnNo.frame;
        cgBtnNo.origin.y -= 10;
        cgBtnNo.size.height = barHeightInPlus;
        self.btnNo.frame = cgBtnNo;
        
        CGRect cgBtnYes = self.btnYES.frame;
        cgBtnYes.origin.y -= 10;
        cgBtnYes.size.height = barHeightInPlus;
        self.btnYES.frame = cgBtnYes;
        
        fBtnRadius = barRadiusInPlus;
        fVRadius = 8;
    }
    
    self.vQuestionBG.layer.cornerRadius = fVRadius;
    self.btnNo.layer.cornerRadius = fBtnRadius;
    self.btnYES.layer.cornerRadius = fBtnRadius;
}

-(IBAction) clickYES:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKeyPath:kDicKeyAborigines];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self pushToNextQuestion];
    
//    int nCareTime = [[[NSUserDefaults standardUserDefaults] objectForKey:kDicKeyCareTime] intValue];
//    
//    if (nCareTime == 1)
//    {
//        [self pushToNextQuestion];
//    }
//    else
//    {
//        [self pushToVcMapMain];
//    }
}

-(IBAction) clickNO:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKeyPath:kDicKeyAborigines];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self pushToNextQuestion];
    
//    int nCareTime = [[[NSUserDefaults standardUserDefaults] objectForKey:kDicKeyCareTime] intValue];
//    
//    if (nCareTime == 1)
//    {
//        [self pushToNextQuestion];
//    }
//    else
//    {
//        [self pushToVcMapMain];
//    }
    
}

- (void) pushToNextQuestion
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcQIntubation* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcQIntubation"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) pushToVcMapMain
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcMapMain* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcMapMain"];
    vc.strPrePage = @"vcQuestion";
    vc.title = @"查詢結果";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
