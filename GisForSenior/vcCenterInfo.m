//
//  vcCenterInfo.m
//  GisForSenior
//
//  Created by jane on 2015/7/8.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcCenterInfo.h"
#import "vcPathPlanning.h"

@interface vcCenterInfo ()
{
    UIWebView* wvTel;
    NSString *strBedInfo;
    SoapTool *soap;
    NSDictionary *dicCareCenterInfo;
    
    BOOL bIsSpread;
}
@end

@implementation vcCenterInfo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    bIsSpread = NO;
    
    self.wvMain.layer.cornerRadius = 5;
    self.vTitleBG.layer.cornerRadius = 20;
    self.lblTitleName.text = [self.dicCenterInfo objectForKey:@"Name"];
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.vTitleBG.layer.cornerRadius = 25;
        CGRect cgTmp = self.vTitleBG.frame;
        cgTmp = CGRectMake(cgTmp.origin.x, cgTmp.origin.y - 5, cgTmp.size.width, cgTmp.size.height+ 10);
        self.vTitleBG.frame = cgTmp;
    }

    self.lblTitleName.font = [UIFont boldSystemFontOfSize:fFontSize];
    CGRect cgTmp = self.lblTitleName.frame;
    cgTmp.origin.x += 5;
    cgTmp.origin.y -= 2;
    self.lblTitleName.frame = cgTmp;
    [self resizeHeightForLabel:self.lblTitleName vFrame:self.vTitleBG.frame];
    self.lblTitleName.text = [self.dicCenterInfo objectForKey:@"Name"];
    self.lblTitleName.font = [UIFont boldSystemFontOfSize:fFontSize];
    self.lblTitleName.textColor = [UIColor blackColor];
    
    [self HudShowMsg:@"資料讀取中..."];
    
    soap = [[SoapTool alloc] init];
    soap.delegate = self;
    
    NSString *strCenterID = [self.dicCenterInfo objectForKey:@"CareCenterID"];
    
    NSLog(@"dicCenterInfo %@",self.dicCenterInfo);
    
//    self.dicCenterInfo = @{@"CareCenterID":@"",
//                           @"class":@"無",
//                           @"tax":@"無",
//                           @"Url":@"無",
//                           @"ServiceContent":@"無",
//                           @"Tel":@"無",
//                           @"update":@"無",
//                           @"MemberCount":@"無",
//                           @"Name":@"無",
//                           @"RegionNo":@"無",
//                           @"Lng":@"無",
//                           @"Lat":@"無",
//                           @"ServiceType":@[@"無"],
//                           @"HomeRegionNo":@[@"無"],
//                           @"BedCount":@"無",
//                           @"BenInfo":@[@"無"],
//                           @"ServiceTypeShortName":@[@"無"],
//                           @"email":@"無",
//                           @"No":@"無",
//                           @"Addr":@"無",
//                           @"pli_amount":@"無",
//                           @"classinfo":@"無"
//                           };
    
    if ([strCenterID isEqualToString:@""] || [strCenterID isEqualToString:@"無"])
    {
        dicCareCenterInfo = nil;
        [self loadHtml:nil];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [soap getCareCenterByCenterID:strCenterID];
            
        });
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadHtml:(NSDictionary *) dicShowCareCenterInfo
{
    self.wvMain.delegate = self;
    
    screenSize = [[UIScreen mainScreen] bounds].size;
    
    NSArray *aryServiceTypeShortName = [self.dicCenterInfo objectForKey:@"ServiceTypeShortName"];
    
    NSString *strFontSizeInStyle = @"17";
    NSString *strLeftScale = @"35";
    NSString *strRightScale = @"65";
    NSString *strImageSize = @"22";
    
    
    if (screenSize.height > kI6ScreenHeight)
    {
        strFontSizeInStyle = [NSString stringWithFormat:@"%f",fFontSize];
        strLeftScale = @"30";
        strRightScale = @"70";
        strImageSize = @"25";
    }
    
    //以下style=\"vertical-align:top;\"皆是為了向上對齊
    //word-break:break-all; 讓文字可以斷行
    NSString *strHtmlForImg = @"<td width=%@%% style=\"vertical-align:top;\"><img src=\"%@\" width=%@ height=%@ align=\"top\"></img>";
    
    NSMutableString* html = [NSMutableString new];
    [html appendFormat:@"<html ><head><style>a{text-decoration:none;color:#0D60FF;font-size:%@px} normalStyle{font-size:%@px}</style></head><body>",strFontSizeInStyle,strFontSizeInStyle];
    [html appendString:@"<table width=100% border=0 style=\"background-color:transparent;word-break:break-all;\">"];
    [html appendString:@"<tr><tr>"];
    
//    NSString *strContact = [self.dicCenterInfo objectForKey:@"contact"];
//    [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%% ><normalStyle><b>聯 絡 人&nbsp;&nbsp;｜</b></normalStyle></td>",strLeftScale];
//    [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%% ><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strContact];
//    [html appendString:@"<tr><tr><tr><tr>"];
    
    NSURL *urlImg = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"b_phone@2x" ofType:@"png"]];
    [html appendFormat:strHtmlForImg,strLeftScale ,urlImg,strImageSize,strImageSize];
    NSString *strTel = [self.dicCenterInfo objectForKey:@"Tel"];;
    [html appendString:@"<normalStyle><b> 電 話&nbsp;｜</b></normalStyle></td>"];
    if([strTel isEqualToString:@""]||[strTel isEqualToString:@"無"])
    {
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,@"無"];
    }
    else
    {
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"telprompt:%@\"><b><u>%@</u></b></a></td>",strRightScale,strTel,strTel];
    }
    
    [html appendString:@"<tr><tr><tr><tr>"];
    
    urlImg = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"b_site@2x" ofType:@"png"]];
    [html appendFormat:strHtmlForImg,strLeftScale,urlImg,strImageSize,strImageSize];
    NSString *strUrl = [self.dicCenterInfo objectForKey:@"Url"];
    [html appendString:@"<normalStyle><b> 網 址&nbsp;｜</b></normalStyle></td>"];
    
    if ([strUrl rangeOfString:@"http"].length != 0)
    {
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"%@\"><b><u>%@</u></b></a></td>",strRightScale,strUrl,strUrl];
    }
    else if([strUrl isEqualToString:@""]||[strUrl isEqualToString:@"無"])
    {
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,@"無"];
    }
    else
    {
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"http:%@\"><b><u>%@</u></b></a></td>",strRightScale,strUrl,strUrl];
    }
    
    [html appendString:@"<tr><tr><tr><tr>"];
    
    urlImg = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"b_address@2x" ofType:@"png"]];
    [html appendFormat:strHtmlForImg,strLeftScale,urlImg,strImageSize,strImageSize];
    NSString *strAddr = [self.dicCenterInfo objectForKey:@"Addr"];
    [html appendString:@"<normalStyle><b> 地 址&nbsp;｜</b></normalStyle></td>"];
    if([strAddr isEqualToString:@""]||[strAddr isEqualToString:@"無"])
    {
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,@"無"];
    }
    else
    {
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"maps:\"><b><u>%@</u></b></a></td>",strRightScale,strAddr];
    }
    [html appendString:@"<tr><tr><tr><tr>"];
    
    NSString *strTax = [self.dicCenterInfo objectForKey:@"tax"];
    [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>傳真&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;｜</b></normalStyle></td>"];
    [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strTax];
    [html appendString:@"<tr><tr><tr><tr>"];
    
    NSString *strMail = [self.dicCenterInfo objectForKey:@"email"];
    [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>E-Mail&nbsp;&nbsp;&nbsp;&nbsp;｜</b></normalStyle></td>"];
    [html appendFormat:@"<td style=\"vertical-align:top;text-decoration:none\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strMail];
    [html appendString:@"<tr><tr><tr><tr>"];

    if (![aryServiceTypeShortName[0] isEqual:@"居家式"] && ![aryServiceTypeShortName[0] isEqual:@"社區式"])
    {
        NSArray *aryBedInfo = [self.dicCenterInfo objectForKey:@"BedInfo"];
        
        if (aryBedInfo.count != 0 && ![aryBedInfo[0] isEqualToString:@"無"])
        {
            NSMutableString *strInfo = [[NSMutableString alloc] init];
            for (NSString *strOneBedInfo in aryBedInfo)
            {
                [strInfo appendFormat:@"%@\n",strOneBedInfo];
            }
            
            strBedInfo = strInfo;
            
            [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>床位資訊｜</b></normalStyle></td>"];
            [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"alert:\"><b><u>%@</u></b></a></td>",strRightScale,@"床位資訊"];
            [html appendString:@"<tr><tr><tr><tr>"];

        
        }
        
    }
    
    NSMutableString *strServiceType = [[NSMutableString alloc] init];
    NSArray *aryType = [self.dicCenterInfo objectForKey:@"ServiceType"];
    for (int i = 0; i < aryType.count; i++)
    {
        NSString *strType = aryType[i];
        [strServiceType appendString:[NSString stringWithFormat:@"%@ ",strType]];
    }
    
    //    NSString *strServiceContent = [self.dicCenterInfo objectForKey:@"ServiceContent"];
    [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>服務內容｜</b></normalStyle></td>"];
    [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strServiceType];
    [html appendString:@"<tr><tr><tr><tr>"];
    
    if ([aryServiceTypeShortName[0] isEqual:@"機構住宿式"] || [aryServiceTypeShortName[0] isEqualToString:@"其他"])
    {
        NSString *strClass = @"";
        NSArray *aryClass = [[self.dicCenterInfo objectForKey:@"class"] componentsSeparatedByString:@"、"];

        NSLog(@"%@",[self.dicCenterInfo objectForKey:@"class"]);
        if (aryClass != 0)
        {
            strClass = aryClass[0];
            strClass = [strClass stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSRange rgYear = [strClass rangeOfString:@"年"];
            NSRange rgClass = [strClass rangeOfString:@"級"];
            
            if (rgYear.length != 0 && rgClass.length !=0) {
                
                NSString *strYear = [strClass substringToIndex:rgYear.location];
                strClass = [strClass substringWithRange:NSMakeRange(rgYear.location+1, 1)];
            
                strClass = [NSString stringWithFormat:@"%@(%@年辦理)",strClass,strYear];
            }
        }
        

        if (![strClass isEqualToString:@""])
        {
            [html appendFormat:@"<td colspan=\"2\" style=\"vertical-align:top;\"><normalStyle><b>最近評鑑等第｜%@</b></normalStyle></td>",strClass];
            [html appendString:@"<tr><tr><tr><tr>"];
        }
        else
        {
            
            [html appendFormat:@"<td colspan=\"2\" style=\"vertical-align:top;\"><normalStyle><b>最近評鑑等第｜%@</b></normalStyle></td>",@"無"];
            [html appendString:@"<tr><tr><tr><tr>"];
            NSString *strNoClassReason = [self.dicCenterInfo objectForKey:@"classinfo"];
            [html appendFormat:@"<td colspan=\"2\" style=\"vertical-align:top;\"><normalStyle><b>未評鑑原因｜%@</b></normalStyle></td>",strNoClassReason];
            [html appendString:@"<tr><tr><tr><tr>"];

        }
        
    }

    
//    NSString *strMemberCount = [self.dicCenterInfo objectForKey:@"MemberCount"];
//    [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>員工資訊｜</b></normalStyle></td>"];
//    [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strMemberCount];
//    [html appendString:@"<tr><tr><tr><tr>"];
    
//    NSArray *aryServiceTypeShortName = [self.dicCenterInfo objectForKey:@"ServiceTypeShortName"];
//    NSString *strServiceTypeShortName = aryServiceTypeShortName[0];
//    [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>服務類型｜</b></normalStyle></td>"];
//    [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strServiceTypeShortName];
//    [html appendString:@"<tr><tr><tr><tr>"];
    

//    NSString *strPli = [self.dicCenterInfo objectForKey:@"pli_amount"];
//    [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>公共意外險｜</b></normalStyle></td>"];
//    [html appendFormat:@"<td style=\"vertical-align:top;\" width=75%%><normalStyle><b>%@</b></normalStyle></td>",strPli];
//    [html appendString:@"<tr><tr><tr><tr>"];
    
//    NSString *strBedCount = [self.dicCenterInfo objectForKey:@"BedCount"];
//    [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>床位資訊｜</b></normalStyle></td>"];
//    [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strBedCount];
//    [html appendString:@"<tr><tr><tr><tr>"];
    
    
    if (dicCareCenterInfo != nil)
    {
        
        [html appendFormat:@"<td colspan=\"2\" style=\"vertical-align:top;color:#0D60FF;\"><a href=\"spread:\"><b>如果有長照相關問題，可以洽詢當地照管中心</b></a></td>"];
        
        if (dicShowCareCenterInfo != nil)
        {
            [html appendString:@"<tr><tr><tr><tr>"];
            
            NSString *strUnitName = [dicShowCareCenterInfo objectForKey:@"UnitName"];
            [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>單位&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;｜</b></normalStyle></td>"];
            [html appendFormat:@"<td style=\"vertical-align:top;text-decoration:none\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strUnitName];
            [html appendString:@"<tr><tr><tr><tr>"];
            
            NSString *strUnitAddr = [dicShowCareCenterInfo objectForKey:@"Addr"];
            [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>地址&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;｜</b></normalStyle></td>"];
            [html appendFormat:@"<td style=\"vertical-align:top;text-decoration:none\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strUnitAddr];
            [html appendString:@"<tr><tr><tr><tr>"];
            
            NSString *strUnitTel = [dicShowCareCenterInfo objectForKey:@"Tel"];
            [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>電話&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;｜</b></normalStyle></td>"];
            [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"telprompt:%@\"><b><u>%@</u></b></a></td>",strRightScale,strUnitTel,strUnitTel];
            [html appendString:@"<tr><tr><tr><tr>"];
            
            NSString *strUnitFax = [dicShowCareCenterInfo objectForKey:@"Fax"];
            [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>傳真&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;｜</b></normalStyle></td>"];
            [html appendFormat:@"<td style=\"vertical-align:top;text-decoration:none\" width=%@%%><normalStyle><b>%@</b></normalStyle></td>",strRightScale,strUnitFax];
            [html appendString:@"<tr><tr><tr><tr>"];
            
            NSString *strUnitUrl = [dicShowCareCenterInfo objectForKey:@"URL"];
            [html appendString:@"<td style=\"vertical-align:top;\"><normalStyle><b>網址&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;｜</b></normalStyle></td>"];
            
            if ([strUnitUrl rangeOfString:@"http"].length != 0)
            {
                [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"%@\"><b><u>%@</u></b></a></td>",strRightScale,strUnitUrl,strUnitUrl];
            }
            else
            {
                [html appendFormat:@"<td style=\"vertical-align:top;\" width=%@%%><a href=\"http:%@\"><b><u>%@</u></b></a></td>",strRightScale,strUnitUrl,strUnitUrl];
            }
            
            [html appendString:@"<tr><tr><tr><tr>"];
        }

    }

    [html appendString:@"</table>"];
    
    [html appendString:@"</body></html>"];
    

    [self.wvMain loadHTMLString:html baseURL:nil];
    
    self.wvMain.backgroundColor = [UIColor whiteColor];
    self.wvMain.opaque = NO;
    self.wvMain.clipsToBounds = YES;
    
    
//    wvMain.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_dot.png"]];
}


#pragma mark - webview delegate

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = [request URL];
        NSLog(@"scheme %@",url.scheme);
        
        if ([url.scheme isEqualToString:@"spread"])
        {
            bIsSpread = !bIsSpread;
            
            if (bIsSpread)
            {
                [self loadHtml:dicCareCenterInfo];
            }
            else
            {
                [self loadHtml:nil];
            }
            
        }
        else if ([url.scheme isEqualToString:@"alert"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:strBedInfo delegate:nil cancelButtonTitle:@"確定" otherButtonTitles: nil];
            [alert show];
        }
        else if ([url.scheme isEqualToString:@"telprompt"])
        {
            if(![self makePhoneCallTo:url.absoluteString])
            {
                [self HudErrMsgWithError:@"本裝置不支援撥打電話!" finishBlock:^{
                    
                }];
            }
            return NO;
        }
        else if([url.scheme isEqualToString:@"maps"])
        {
            [self pushToVcPathPlanning:nil];
            return NO;
        }
        else if(![url.scheme isEqualToString:@"tel"])
        {
            [[UIApplication sharedApplication] openURL:url];
            return NO;
            
        }
        else if([url.scheme isEqualToString:@"x-apple-data-detectors"])
        {
            
            [self pushToVcPathPlanning:nil];
            return NO;
        }
        else
        {
            if(![self makePhoneCallTo:url.relativeString])
            {
                [self HudErrMsgWithError:@"本裝置不支援撥打電話!" finishBlock:^{
                    
                }];
            }
            return NO;
        }

    }
    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self HudDissmiss];
}

- (IBAction)pushToVcPathPlanning:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcPathPlanning* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcPathPlanning"];
    vc.dicCenterInfo = self.dicCenterInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL) makePhoneCallTo : (NSString*) phoneNumber
{
//    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
    NSLog(@"phoneNumber %@",phoneNumber);
    NSURL *phoneNum = [[NSURL alloc] initWithString:phoneNumber];
    if ( [[UIApplication sharedApplication] canOpenURL: phoneNum] )
    {
        if(!wvTel)
        {
            wvTel = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
            wvTel.alpha = 0.0;
            [self.view addSubview:wvTel];
        }
        
        [wvTel loadRequest:[NSURLRequest requestWithURL:phoneNum]];
    }
    else
    {
        return NO;
    }
    
    return YES;
}

- (void) loadFinishRespose:(NSArray *) resposeData userInfo:(NSString *)UserInfo error:(NSError *)error
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (error)
        {
            [self HudErrMsgWithError:error.localizedDescription finishBlock:^{
//                UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:UserInfo message:error.localizedDescription delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
//                [alertError show];
                
                dicCareCenterInfo = nil;
                [self loadHtml:nil];
            }];
        }
        else if (resposeData.count == 0)
        {
            dicCareCenterInfo = nil;
        }
        else
        {
            if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kSerivceCareCenterInfo] ])
            {
                dicCareCenterInfo = resposeData[0];
                [self loadHtml:nil];
            }
        }

        
    });
}

@end
