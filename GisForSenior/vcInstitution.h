//
//  vcInstitution.h
//  GisForSenior
//
//  Created by 珍珍 on 2015/12/14.
//  Copyright © 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcInstitution : vcBase
@property (nonatomic,weak) IBOutlet UIImageView *img;
@property (nonatomic,weak) IBOutlet UIView *vImgBG;
@property (nonatomic,weak) IBOutlet UIPageControl *pcMain;

@property (nonatomic,weak) IBOutlet UIButton *btnPrev;
@property (nonatomic,weak) IBOutlet UIButton *btnNext;

@end
