//
//  vcLawCatalog.h
//  GisForSenior
//
//  Created by 珍珍 on 2015/12/17.
//  Copyright © 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcLawCatalog : vcBase<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView *tvMain;


@end
