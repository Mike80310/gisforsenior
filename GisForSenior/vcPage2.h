//
//  vcPage2.h
//  GisForSenior
//
//  Created by jane on 2015/9/9.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcPage2 : vcBase<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView *tvMain;
@property (nonatomic,weak) IBOutlet UIButton *btnNext;

@end
