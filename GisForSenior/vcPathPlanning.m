//
//  vcPathPlanning.m
//  GisForSenior
//
//  Created by jane on 2015/7/3.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcPathPlanning.h"
#import <TGMaps/TGMaps.h>
#import <QuartzCore/QuartzCore.h>

#define errorCode "<td style=\"vertical-align:top;\" width=900%><normalStyle>&nbsp;&nbsp;&nbsp;前行</normalStyle></td><tr><td style=\"vertical-align:top;\" width=90%%><hr size=0.5 align=center noshade width=90%% color=#dcdddd></td><tr>"

#define fAnimationSec 0.5

#define kOtherName @"map_b02"
#define kOrganizationName @"map_g02"
#define kHomeServiceName @"map_o02"
#define kCommunityName @"map_p02"
#define kDisabledName @"map_r02"

NSArray *decodePoly(NSString *encoded)
{
    NSMutableArray *poly = [[NSMutableArray alloc]init];
    int index = 0;
    int len = encoded.length;
    int lat = 0, lng = 0;
    
    while (index < len)
    {
        int b, shift = 0, result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        
        shift = 0;
        result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        
        CLLocationCoordinate2D p = CLLocationCoordinate2DMake((CGFloat) lat / 1E6, (CGFloat) lng / 1E6);
        [poly addObject:[[NSData alloc] initWithBytes:&p length:sizeof(p)]];
    }
    return poly;
}

@interface vcPathPlanning ()
{
    CGRect cgMaskOri;
    
    BOOL bIsChange;
    BOOL bIsEdit;
    float fEmptyCellHeight;
    float fNormalCellHeight;
    float fCenterCellHeight;
    
    NSString *strAddrText;
    NSString *strStartAddr;
    NSString *strEndAddr;
    
#pragma mark - 地圖用
    TGMapView *mapView_;
    id<TGMarker> mkUserLocation;
    
    NSString *serviceUrl;
    NSMutableArray * MarkerList;
    NSMutableArray * LineList;
    
    CLLocationCoordinate2D locationStart;
    CLLocationCoordinate2D locationEnd;
    
    int nRetryCount;
    
    UIImage *imgOther;
    UIImage *imgOrganization;
    UIImage *imgHomeService;
    UIImage *imgCommunity;
    UIImage *imgDisabled;
}
@end

@implementation vcPathPlanning

- (void) viewWillAppear:(BOOL)animated
{
    [self getNowAddress];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    
    
    NSDictionary *dicLatLng = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserLatLng];
    CLLocationCoordinate2D locationUser =
                            CLLocationCoordinate2DMake([[dicLatLng objectForKey:@"Lat"] doubleValue],
                                                        [[dicLatLng objectForKey:@"Lng"] doubleValue]);
    
    [self downloadListWithLocation:locationUser];

}

- (void) viewDidDisappear:(BOOL)animated
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    nRetryCount = 0;
    
    imgOther = [UIImage imageNamed:kOtherName];
    imgOrganization = [UIImage imageNamed:kOrganizationName];
    imgHomeService = [UIImage imageNamed:kHomeServiceName];
    imgCommunity = [UIImage imageNamed:kCommunityName];
    imgDisabled = [UIImage imageNamed:kDisabledName];
    
    [self setVMapLayout];
    [self HudShowMsg:@"路徑規劃中"];
    [self startPathPlanning];
    [self setPathPlanningSearch];
    [self setLblTimeAnDistanceLayout];
}

- (void) setVMapLayout
{
    self.vTitleBG.layer.cornerRadius = 20;
    
    self.lblCenterName.text = [self.dicCenterInfo objectForKey:@"Name"];
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        self.vTitleBG.layer.cornerRadius = 25;
        CGRect cgTmp = self.vTitleBG.frame;
        cgTmp = CGRectMake(cgTmp.origin.x, cgTmp.origin.y - 5, cgTmp.size.width, cgTmp.size.height+ 10);
        self.vTitleBG.frame = cgTmp;
        self.lblCenterName.font = [UIFont boldSystemFontOfSize:20];
    }
    
    self.lblCenterName.font = [UIFont boldSystemFontOfSize:fFontSize];
    [self resizeHeightForLabel:self.lblCenterName vFrame:self.vTitleBG.frame];
    self.lblCenterName.text = [self.dicCenterInfo objectForKey:@"Name"];
    self.lblCenterName.font = [UIFont boldSystemFontOfSize:fFontSize];
    self.lblCenterName.textColor = [UIColor blackColor];
}

- (void) startPathPlanning
{
    serviceUrl = @"http://gis.tgos.nat.gov.tw/TGRoute/TGRoute.aspx?waypoints=MULTIPOINT(%@)&avoidhighway=false&format=json&pathtype=best&keystr=defaulthash";
    
    mapView_ = [[TGMapView alloc] initWithFrame:self.vMap.bounds];
    [self.vMap insertSubview:mapView_ atIndex:0];
    mapView_.delegate = self;
    
    MarkerList = [[NSMutableArray alloc]init];
    LineList = [[NSMutableArray alloc]init];
    [self getNowAddress];
}

- (void) setPathPlanningSearch
{
#pragma mark - 整個路徑規劃view
    
    [self.view bringSubviewToFront:self.vMask];
    
    gestTapMask = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeMaskPostion:)];
    [self.vMask addGestureRecognizer:gestTapMask];
    gestTapMask.delegate = self;
    
    gestTapTv = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeMaskPostion:)];
    [self.vPathPlanningBG addGestureRecognizer:gestTapTv];
    gestTapMask.delegate = self;
    
    self.vPathPlanningBG.backgroundColor = [UIColor clearColor];
    
#pragma mark - 查詢條件view
    self.tvMain.scrollEnabled = NO;
    self.tvMain.layer.masksToBounds = NO;
    self.tvMain.layer.shadowRadius = 5;
    self.tvMain.layer.shadowOpacity = 0.5;
    
    CGRect cgTv = self.tvMain.bounds;
    cgTv.size.height -= 100;
    self.tvMain.layer.shadowPath = [UIBezierPath bezierPathWithRect:cgTv].CGPath;
    
    strStartAddr = strAddrText = strDefaultAddr;
    
    strEndAddr =[NSString stringWithFormat:@"%@",[self.dicCenterInfo objectForKey:@"Addr"]] ;
    
    bIsEdit = NO;
    bIsChange = NO;
    
    fEmptyCellHeight = 10;
    fNormalCellHeight = 50;
    fCenterCellHeight = 30;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        fEmptyCellHeight += 10;
        fNormalCellHeight += 10;
        fCenterCellHeight += 5;
    }
    
#pragma mark - html


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setLblTimeAnDistanceLayout
{
    self.lblTimeAnDistance.clipsToBounds = YES;
    self.lblTimeAnDistance.layer.cornerRadius = 5;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        CGRect cgLabel = self.lblTimeAnDistance.frame;
        cgLabel.size.height += 5;
        self.lblTimeAnDistance.frame = cgLabel;
        
        self.lblTimeAnDistance.layer.cornerRadius = 15;
        self.lblTimeAnDistance.font = [UIFont boldSystemFontOfSize:fFontSize];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return fEmptyCellHeight;
    }
    
    if (indexPath.row == 2)
    {
        if (!bIsEdit) return fCenterCellHeight;
    }
    
    return fNormalCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell;
    NSString *CellIdentifier;
    
    if (indexPath.row == 0)
    {
        CellIdentifier = [NSString stringWithFormat:@"emptyCell"];
    }
    else if (indexPath.row == 1)
    {
        CellIdentifier = [NSString stringWithFormat:@"startCell"];
    }
    else if (indexPath.row == 2)
    {
//        if (bIsEdit)
//        {
//            CellIdentifier = [NSString stringWithFormat:@"nowLocationCell"];
//        }
//        else
//        {
//            CellIdentifier = [NSString stringWithFormat:@"centerCell"];
//        }
        
         CellIdentifier = [NSString stringWithFormat:@"centerCell"];
    }
    else if (indexPath.row == 3)
    {
        CellIdentifier = [NSString stringWithFormat:@"endCell"];
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    
    if (indexPath.row == 0)
    {
        cgTmp.origin.x = cgTmp.origin.y = 0;
        cgTmp.size.width = cell.contentView.frame.size.width;
        cgTmp.size.height = cell.contentView.frame.size.height + 5;
        vContent.frame = cgTmp;
        vContent.layer.cornerRadius = 5;
        
    }
    else
    {
        cgTmp.origin.x = cgTmp.origin.y = 0;
        cgTmp.size.width = cell.contentView.frame.size.width;
        cgTmp.size.height = cell.contentView.frame.size.height;
        vContent.frame = cgTmp;
    }
    
    
    if (indexPath.row == 2)
    {
//        if (bIsEdit)
//        {
//            UILabel *lblUpLine = (UILabel *) [cell.contentView viewWithTag:107];
//            CGRect cgUpLine = lblUpLine.frame;
//            cgUpLine.origin.y = 1;
//            
//            
//            UILabel *lblDownLine = (UILabel *) [cell.contentView viewWithTag:108];
//            CGRect cgDownLine = lblDownLine.frame;
//            cgDownLine.origin.y = fNormalCellHeight - 3;
//            
//            cgUpLine.size.width = cgDownLine.size.width = cgTmp.size.width - cgUpLine.origin.x - 15;
//            
//            lblUpLine.frame = cgUpLine;
//            lblDownLine.frame = cgDownLine;
//        }
//        else
//        {
//            UIButton *btnExchange = (UIButton *) [cell.contentView viewWithTag:103];
//            [btnExchange addTarget:nil action:@selector(clickExChange:) forControlEvents:UIControlEventTouchUpInside];
//            CGRect cgBtn = btnExchange.frame;
//            cgBtn = CGRectMake(cgTmp.size.width - cgBtn.size.width, 0,
//                               fCenterCellHeight, fCenterCellHeight);
//       
//            btnExchange.frame = cgBtn;
//            
//            UILabel *lblLine = (UILabel *) [cell.contentView viewWithTag:106];
//            CGRect cgLine = lblLine.frame;
//            cgLine.origin.y = fCenterCellHeight/2;
//            lblLine.frame = cgLine;
//        }
        UIButton *btnExchange = (UIButton *) [cell.contentView viewWithTag:103];
        [btnExchange addTarget:nil action:@selector(clickExChange:) forControlEvents:UIControlEventTouchUpInside];
        CGRect cgBtn = btnExchange.frame;
        cgBtn = CGRectMake(cgTmp.size.width - cgBtn.size.width , 0,
                           fCenterCellHeight, fCenterCellHeight);
        btnExchange.frame = cgBtn;
        
        UILabel *lblLine = (UILabel *) [cell.contentView viewWithTag:106];
        CGRect cgLine = lblLine.frame;
        cgLine.origin.y = fCenterCellHeight/2;
        lblLine.frame = cgLine;
    }
    else
    {
        UILabel *lblLocation = (UILabel *)[cell.contentView viewWithTag:101];
        UITextField *txtLocation = (UITextField *)[cell.contentView viewWithTag:102];
        
        if (screenSize.height >= kI6ScreenHeight)
        {
            lblLocation.font = txtLocation.font = [UIFont systemFontOfSize:fFontSize];
        }
        
        CGRect cgLabel = lblLocation.frame;
        CGRect cgTxt = txtLocation.frame;
        cgLabel.size.height = cgTxt.size.height = fNormalCellHeight;
        cgLabel.size.width = cgTxt.size.width = cgTmp.size.width - cgLabel.origin.x - 15;
        lblLocation.frame = cgLabel;
        txtLocation.frame = cgTxt;
        
        if (indexPath.row == 1)
        {
            lblLocation.text = strStartAddr;
            lblLocation.hidden = NO;
            txtLocation.hidden = YES;
//            lblLocation.hidden = !bIsChange;
//            txtLocation.hidden = bIsChange;
//            txtLocation.delegate = self;
//            
//            [txtLocation addTarget:self action:@selector(changeEditState:) forControlEvents:UIControlEventEditingDidBegin];
            
        }
        else if (indexPath.row == 3)
        {
            lblLocation.text = strEndAddr;
            lblLocation.hidden = NO;
            txtLocation.hidden = YES;
//            lblLocation.hidden = bIsChange;
//            txtLocation.hidden = !bIsChange;
//            txtLocation.delegate = self;
//            
//            [txtLocation addTarget:self action:@selector(changeEditState:) forControlEvents:UIControlEventEditingDidBegin];
            
        }
        
        UILabel *lblTitleName = (UILabel *)[cell.contentView viewWithTag:105];
        UIImageView *imgTitle = (UIImageView *)[cell.contentView viewWithTag:104];
        
        CGRect cgLblTitle = lblTitleName.frame;
        CGRect cgImg = imgTitle.frame;
        cgLblTitle.size.height = cgImg.size.height = fNormalCellHeight;
        lblTitleName.frame = cgLblTitle;
        imgTitle.frame = cgImg;
        
        if (indexPath.row == 3)
        {
            cgTmp.origin.y -= 5;
            vContent.frame = cgTmp;
            vContent.layer.shadowRadius = 5;
            vContent.layer.shadowOpacity = 0.5;
            
            cgLabel.size.height = cgTxt.size.height = fNormalCellHeight - 2;
            
            if (screenSize.height >= kI6ScreenHeight)
            {
                cgLabel.size.height = cgTxt.size.height = fNormalCellHeight - 12;
            }
            lblLocation.frame = cgLabel;
            txtLocation.frame = cgTxt;
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (!bIsEdit) return;
//    if (indexPath.row != 2) return;
//    
//    NSIndexPath *ip;
//    if (bIsChange)
//    {
//        ip = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:0];
//    }
//    else
//    {
//        ip = [NSIndexPath indexPathForRow:indexPath.row-1 inSection:0];
//    }
//    
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:ip];
//
//    UITextField *txtLocation = (UITextField *)[cell.contentView viewWithTag:102];
//    txtLocation.text = strAddrText = strDefaultAddr;
//    
//    [txtLocation resignFirstResponder];
//    
//    [self changeEditState:nil];
//    
//}


- (IBAction) clickExChange:(id)sender
{
    bIsChange = !bIsChange;
    NSString *strTmp = strStartAddr;
    strStartAddr = strEndAddr;
    strEndAddr = strTmp;
    
    CLLocationCoordinate2D clcTmp = locationStart;
    locationStart = locationEnd;
    locationEnd = clcTmp;
    
    [self RouteClick:nil];
    [self.tvMain reloadData];
}

- (void) changeEditState:(id) sender
{
    bIsEdit = !bIsEdit;
    [self.tvMain reloadData];
    
    if(sender)
    {
        UITextField *txt = (UITextField *) sender;
        NSLog(@"txt %@",txt.text);
        
        if ([txt.text isEqualToString:strDefaultAddr])
        {
            txt.text = @"";
        }
        else
        {
            txt.text = strAddrText;
        }
        
        [txt becomeFirstResponder];
        
    }
    
}

#pragma mark - 移動vMask

- (void) changeMaskPostion:(id)sender
{
    float fNewY;
    UITapGestureRecognizer *tgTmp = (UITapGestureRecognizer *) sender;
    
    
    if (tgTmp == gestTapTv)
    {
        fNewY = 0;
        [self.vPathPlanningBG removeGestureRecognizer:gestTapMask];
        
        [self.vMask addGestureRecognizer:gestTapMask];
        gestTapMask.delegate = self;
        
        gestTapTv.delegate = nil;
        [self.vPathPlanningBG removeGestureRecognizer:gestTapTv];
        
    }
    else if(tgTmp == gestTapMask)
    {
        float fPathPlanningHeight = self.vPathPlanningBG.frame.size.height;
        fNewY = fPathPlanningHeight - fEmptyCellHeight - fNormalCellHeight - self.lblTimeAnDistance.frame.size.height;
        
        [self.vPathPlanningBG addGestureRecognizer:gestTapTv];
        gestTapTv.delegate = self;
        
        gestTapMask.delegate = nil;
        [self.vMask removeGestureRecognizer:gestTapMask];
    }
    
    [UIView animateWithDuration:fAnimationSec
                     animations:^{
                         self.vMask.transform = CGAffineTransformMakeTranslation(0, fNewY);
                         if (fNewY == 0)
                         {
                             self.vMask.backgroundColor = clrMaker(0, 0, 0, 0.5);
                             
                         }
                         else
                         {
                             self.vMask.backgroundColor = [UIColor clearColor];
                         }
                     }
     ];
    
    
}

#pragma mark - tap事件

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    if ([gestureRecognizer isEqual:gestTapMask] &&
        (touch.view.tag == 100 ||
         [NSStringFromClass([touch.view class]) isEqualToString:@"UIWebBrowserView"] ||
         [touch.view isKindOfClass:[UIButton class]]))//bIsShowPathPlanning &&
        return NO;
    
    [self changeMaskPostion:gestureRecognizer];
    
    return YES;
}


#pragma mark - keyboard delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    strAddrText = textField.text;
    [self changeEditState:nil];
    return YES;
}

#pragma mark - TGOS相關

- (void) downloadListWithLocation:(CLLocationCoordinate2D)loc
{
    [self stopUpdateCurrentLocation];
    
    [mapView_ animateToLocation:loc];
    
//    UIImage *imgUserLocationIcon = [UIImage imageNamed:@"bt_car"];
    
    if(loc.latitude == 0 && loc.longitude == 0)
    {
        mapView_.viewer = [TGViewerPosition viewerWithLatitude:defaultLocation.latitude longitude:defaultLocation.longitude zoom:10];
        [mapView_ startRendering];
        loc = defaultLocation;
    }
    else
    {
        mapView_.viewer = [TGViewerPosition viewerWithLatitude:loc.latitude longitude:loc.longitude zoom:10];
        [mapView_ startRendering];
    }
    
    locationStart =  loc;//CLLocationCoordinate2DMake(25.051888, 121.554501); //
    
    locationEnd = CLLocationCoordinate2DMake([[self.dicCenterInfo objectForKey:@"Lat"] doubleValue], [[self.dicCenterInfo objectForKey:@"Lng"] doubleValue]);
    
    [self RouteClick:nil];

}

-(void)clearList
{
    for (id<TGMarker> marker in MarkerList) {
        [marker remove];
    }
    [MarkerList removeAllObjects];
    for (id<TGPolyline> line in LineList) {
        [line remove];
    }
    [LineList removeAllObjects];
}

- (IBAction)RouteClick:(id)sender
{
    [self clearList];
    
    TGPointD * ori = [TGTransformation wgs84ToTWD97:locationStart];//設定起點
    TGPointD * dest = [TGTransformation wgs84ToTWD97:locationEnd];//設定終點
    NSString * waypoints =[NSString stringWithFormat:@"%@,%@",[self getPointToString:ori],[self getPointToString:dest]];
    NSString * weburl = [NSString stringWithFormat:serviceUrl,waypoints];
    
    NSURL * url = [NSURL URLWithString:weburl];
    
    NSLog(@"url %@",url);
    NSError *error;
    NSMutableDictionary *ServiceJson = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url]
                                                                       options:NSJSONReadingMutableLeaves
                                                                         error:&error];
    

    if (error == nil && ServiceJson != nil) {
        
        NSMutableDictionary * NavigationJson = [ServiceJson objectForKey:@"Navigation"];
        
        NSDictionary *dicRoute = [NavigationJson objectForKey:@"route"];
        NSDictionary *dicRoutePath = [dicRoute objectForKey:@"path"];
        
        NSString *strDistance = [[dicRoutePath objectForKey:@"distance"]objectForKey:@"text"];
        
        int nDistance = [[[dicRoutePath objectForKey:@"distance"]objectForKey:@"value"] intValue];
        
        if (nDistance < 1000)
        {
            strDistance = [NSString stringWithFormat:@"%i m",nDistance];
        }
        
        NSString *strDuration = [[dicRoutePath objectForKey:@"duration"]objectForKey:@"text"];
        
        self.lblTimeAnDistance.text = [NSString stringWithFormat:@"%@ / %@",[self removeSpaceAndNewline:strDistance] ,
                                                                            [self removeSpaceAndNewline:strDuration]];
        
        NSMutableArray *aryPath=[dicRoutePath objectForKey:@"segment"];
        
        NSString *strRoute = @"";
        NSMutableString* html = [NSMutableString new];
        [html appendFormat:@"<td style=\"vertical-align:top;\" width=900%%><normalStyle>&nbsp;&nbsp;&nbsp;前行</normalStyle></td>"];
        [html appendFormat:@"<tr>"];
        [html appendString:@"<td style=\"vertical-align:top;\" width=90%%><hr size=0.5 align=center noshade width=90%% color=#dcdddd></td>"];
        [html appendFormat:@"<tr>"];
        for (int i2 = 0; i2 < aryPath.count; i2++) {
            if (strRoute.length > 0) {
                strRoute = [strRoute stringByAppendingString:@"\n"];
            }
            NSDictionary *dicPathInfo = [aryPath objectAtIndex:i2];
            
            NSDictionary *dicDistanceInfo = [dicPathInfo objectForKey:@"distance"];
            NSString *distance = [dicDistanceInfo objectForKey:@"text"];
            
            int oneRoadDistance = [[dicDistanceInfo objectForKey:@"value"] intValue];
            
            if (oneRoadDistance < 1000)
            {
                distance = [NSString stringWithFormat:@"%i m",oneRoadDistance];
            }
            
            
            NSString *instructions = [dicPathInfo objectForKey:@"instructions"];
            
            strRoute = [strRoute stringByAppendingFormat:@"%@ %@",distance,instructions];
            
            [html appendFormat:@"<td style=\"vertical-align:top;\" width=90%%><normalStyle>&nbsp;&nbsp;&nbsp;%@ %@</normalStyle></td>",distance,instructions];
            [html appendFormat:@"<tr>"];
            [html appendString:@"<td style=\"vertical-align:top;\" width=90%%><hr size=0.5 align=center noshade width=90%% color=#dcdddd></td>"];
            [html appendFormat:@"<tr>"];
        }
        
        NSString *overview_polylineStr = [[dicRoute objectForKey:@"overview_polyline"]objectForKey:@"points"];
        NSArray * overview_pts = decodePoly(overview_polylineStr);
        
        TGMutablePath *path = [TGMutablePath path];//建立TGMutablePath物件
        for (NSData * ndt in overview_pts)
        {
            CLLocationCoordinate2D c;
            [ndt getBytes:&c];
            [path addCoordinate:c];
        }
        
        TGPolylineOptions *options= [TGPolylineOptions options];//建立TGPolylineOptions物件
        options.color = clrMaker(0, 40, 180, 1) ;//設定顏色
        options.width = 4.f;                                                //設定寬度
        options.path = path;                                                //設定Polyline的path
        [LineList addObject:[mapView_ addPolylineWithOptions:options]];               //將Polyline加入地圖
        
        TGMarkerOptions *makStart = [[TGMarkerOptions alloc] init];
        makStart.position = [path coordinateAtIndex:0];
        makStart.icon = [UIImage imageNamed:@"bt_car02"];
        [MarkerList addObject:[mapView_ addMarkerWithOptions:makStart]];
        
        TGMarkerOptions *makEnd = [[TGMarkerOptions alloc] init];
        makEnd.position = [path coordinateAtIndex:path.count-1];
        
        
        NSString *strType = [self.dicCenterInfo objectForKey:@"ServiceTypeShortName"][0];
        makEnd.icon = [self getIconImage:strType];
        [MarkerList addObject:[mapView_ addMarkerWithOptions:makEnd]];
        
        [self MoveToBounds];
        [self writeHtml:html];
        
    }
}

-(void) writeHtml:(NSString *) strMiddle
{
    NSLog(@"strMiddle %@",strMiddle );
    
    if ([strMiddle isEqualToString:@errorCode])
    {
        nRetryCount++;
        if (nRetryCount>=3)
        {
            
            [self HudDismissWithError:@"無法規劃此路徑" finishBlock:^{
                [self.navigationController popViewControllerAnimated:YES];
            } Title:nil];
            return;
        }
        [self RouteClick:nil];
    }
    else
    {
        NSMutableString* html = [NSMutableString new];
        [html appendFormat:@"<html ><head><style>a{text-decoration:none;color:#0D60FF;font-size:%fpx} normalStyle{font-size:%fpx}</style></head><body>",fFontSize,fFontSize];
        [html appendString:@"<br><br><br>"];
        
        [html appendString:@"<table width=100% border=0 style=\"background-color:transparent;word-break:break-all;vertical-align:center;\">"];
        [html appendString:strMiddle];
        
        [html appendString:@"</table>"];
        [html appendString:@"</body></html>"];
        
        self.wvMain.delegate = self;
        [self.wvMain loadHTMLString:html baseURL:nil];
        
        self.wvMain.backgroundColor = [UIColor whiteColor];
        self.wvMain.opaque = NO;
        self.wvMain.clipsToBounds = YES;

    }
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self HudDissmiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self HudDismissWithError:@"路徑規劃失敗" finishBlock:^{
    } Title:@"提示"];
}

-(void)MoveToBounds
{
    if (MarkerList.count > 0) {
        TGMutablePath *path;
        
        path = [TGMutablePath path];//建立TGMutablePath物件
        
        for (id<TGMarker> marker in MarkerList)
        {
            [path addCoordinate:[marker position]];
        }
        TGLatLngBounds *bounds = [[TGLatLngBounds alloc] initWithPath:path];
        TGViewerUpdate *update = [TGViewerUpdate fitBounds:bounds];//依照TGLatLngBounds的設置顯示於地圖上
        [mapView_ moveViewer:update];
    }
}

-(NSString *)getPointToString:(TGPointD*)pt
{
    return [[NSString stringWithFormat:@"(%f %f)",pt.X,pt.Y] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)removeSpaceAndNewline:(NSString *)str
{
    NSString *temp = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return temp;
}

#pragma mark - tgosDelegate
- (BOOL)mapView:(TGMapView *)mapView didTapMarker:(id<TGMarker>)marker
{
    return YES;
}

- (UIImage *) getIconImage:(NSString *) strType
{
    if ([strType isEqualToString:@"機構住宿式"])
    {
        return imgOrganization;
    }
    else if ([strType isEqualToString:@"社區式"])
    {
        return imgCommunity;
    }
    else if ([strType isEqualToString:@"居家式"])
    {
       return imgHomeService;
    }
    else if ([strType isEqualToString:@"其他"])
    {
        return imgOther;
    }
    else if ([strType isEqualToString:@"身障資源"])
    {
        return imgDisabled;
    }
    
    
    return [UIImage new];
}

@end
