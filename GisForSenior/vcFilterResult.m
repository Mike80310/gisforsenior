//
//  vcFilterResult.m
//  GisForSenior
//
//  Created by jane on 2015/9/5.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcFilterResult.h"
#import "vcCenterInfo.h"
#import "vcPathPlanning.h"
#import "vcFilter.h"

#define kStrLostResource @"尚未選擇服務方式"

#define kOrganizationColor @"#419600"
#define kCommunityColor @"#6e50d2"
#define kHomeServiceColor @"#ff6e00"
#define kOtherColor @"#826450"
#define kDisabled @"#dc0000"

#define kOtherName @"map_b01"
#define kOrganizationName @"map_g01"
#define kHomeServiceName @"map_o01"
#define kCommunityName @"map_p01"
#define kDisabledName @"map_r01"

#define kOtherPressName @"map_b02"
#define kOrganizationPressName @"map_g02"
#define kHomeServicePressName @"map_o02"
#define kCommunityPressName @"map_p02"
#define kDisabledPressName @"map_r02"

#define kAreaTypeIndexDefault 0

#define nFilterTag 525

@interface vcFilterResult ()
{
    int nWidthInterval;
    int nHeightInterval;
    
    int nCellHeight;
    int nContentWidth;
    int nCellRadius;
    
    NSDictionary *dicNowCenterInfo;
    
    TGMapView *mapView_;
    id<TGMarker> mkUserLocation;
    id<TGMarker> mkNowSelected;
    NSString* addresslocator2;//行政區定位service url
    NSMutableArray * aryMarkerList;//地圖上的圖釘列表
    CLLocationCoordinate2D locationUser;
    
    UIImage *imgOther;
    UIImage *imgOrganization;
    UIImage *imgHomeService;
    UIImage *imgCommunity;
    UIImage *imgDisabled;
    
    UIImage *imgOtherPress;
    UIImage *imgOrganizationPress;
    UIImage *imgHomeServicePress;
    UIImage *imgCommunityPress;
    UIImage *imgDisabledPress;
    
    SoapTool *soap;
    
#pragma mark - 篩選器

    UITapGestureRecognizer* gestVFilterTap;
    
    NSMutableArray *aryKeyBoard;//放置tableview裡的keyboard
    UITapGestureRecognizer* tapKeyBoard;//收起keyboard的手勢
    
    BOOL bFromVcMainLogin;
    
    //cell的layout設定
    int nFilterCellHeight;
    int nCornerRadius;
    int nArrow;
    int nCellInterval;
    int nFontSize;
    int nTxtInterval;
    
    int nSpreadCell;//被展開的Section
    
    //地區SC選擇的index
    int nFirstKeywordRegionIndex;//LBS的縣市
    int nFirstKeywordAreaIndex;//LBS的地區
    int nRegionKeywordSelectedIndex;//現在所選擇的縣市
    int nAreaKeywordSelectedIndex;//現在所選擇的地區
    
    int nFirstRegionIndex;//LBS的縣市
    int nFirstAreaIndex;//LBS的地區
    int nRegionSelectedIndex;//現在所選擇的縣市
    int nAreaSelectedIndex;//現在所選擇的地區
    NSArray *aryAreaType;//for居服用
    int nAreaTypeIndex;
    
    NSString *strKeywordCityArea;
//    NSString *strCityArea;//現在的所在位置
    NSString *strKeyword;
    
//    CLLocationCoordinate2D locationUser;
    NSString *strRegionAreaNo;
    NSString *strKeywordregionAreaNo;
    
    int nServiceType;
    
//    NSArray *aryServiceList;//所有資源
    
    NSArray *aryCityList;//所以地區
    
    NSMutableArray *aryArea;//地區列表
    
    NSArray *aryResult;
    NSArray *arySelectedServiceItemNo;//目前選取的資源分類列表
    
    int nHomestayNo;
}
@end

@implementation vcFilterResult

#pragma mark - 查詢筆數文案

- (void) showResultAlert:(int) nCount
{
    NSString *str = [NSString stringWithFormat:@"符合筆數共%i筆",nCount];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:str delegate:nil cancelButtonTitle:@"確定" otherButtonTitles: nil];
    [alert show];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self pageLayoutDefaltValue];
    
    aryMarkerList = [[NSMutableArray alloc] init];
    
    [self setMap];
    
    [self showResultAlert:(int)self.aryShowInfo.count];
    
#pragma mark - 篩選器
    
    
    aryKeyBoard = [NSMutableArray new];
    
    [self defaltFilterValue];
    
    soap = [[SoapTool alloc] init];
    soap.delegate = self;
    
    nHomestayNo = [self getHomestay];
}

- (void) pageLayoutDefaltValue
{
    [self.tvResult setBackgroundColor:[UIColor clearColor]];
    btnRight.hidden = NO;
    
    imgOther = [UIImage imageNamed:kOtherName];
    imgOrganization = [UIImage imageNamed:kOrganizationName];
    imgHomeService = [UIImage imageNamed:kHomeServiceName];
    imgCommunity = [UIImage imageNamed:kCommunityName];
    imgDisabled = [UIImage imageNamed:kDisabledName];
    
    imgOtherPress = [UIImage imageNamed:kOtherPressName];
    imgOrganizationPress = [UIImage imageNamed:kOrganizationPressName];
    imgHomeServicePress = [UIImage imageNamed:kHomeServicePressName];
    imgCommunityPress = [UIImage imageNamed:kCommunityPressName];
    imgDisabledPress = [UIImage imageNamed:kDisabledPressName];
    
    nCellHeight = 85;
    nHeightInterval = 10;
    nWidthInterval = 5;
    if (screenSize.height > kI6ScreenHeight)
    {
        nContentWidth = 350;
        
    }
    else if (screenSize.height == kI6ScreenHeight)
    {
        nContentWidth = 320;
    }
    else
    {
        nContentWidth = 300;
    }
    nCellRadius = MIN(nContentWidth,nCellHeight)/2;
    
#pragma mark - 篩選器
    nFilterCellHeight = kCellHeight;
    nCornerRadius = kCornerRadius;
    nArrow = kArrow;
    nCellInterval = kCellInterval;
    nTxtInterval = kTxtInterval;
    nFontSize = fFontSize;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        nFilterCellHeight +=15;
        nCornerRadius += 8;
        nArrow += 10;
        nCellInterval += kCellInterval;
        nFontSize += 2;
        nTxtInterval += 5;
    }
}

- (void) defaltFilterValue
{
    self.vFilter.tag = nFilterTag;
    
    bFromVcMainLogin = [[_dicPrePageSetting objectForKey:@"bFromVcMainLogin"] boolValue];
    nServiceType = [[_dicPrePageSetting objectForKey:@"nServiceType"] intValue];
    nAreaTypeIndex = [[_dicPrePageSetting objectForKey:@"nAreaTypeIndex"] intValue];
    strKeyword = [_dicPrePageSetting objectForKey:@"strKeyword"];
    arySelectedServiceItemNo = [_dicPrePageSetting objectForKey:@"arySelectedNo"];
    
    aryAreaType = @[@"單位地址",@"服務範圍"];
    aryArea = [[NSUserDefaults standardUserDefaults] objectForKey:kServiceRegionList];
    aryCityList = [self getRegionCityList];
    
    NSArray *aryTmp = [self.strCityArea componentsSeparatedByString:@"/"];
    nFirstRegionIndex = [self getRegionIndex:aryTmp[0]];
    nFirstAreaIndex = [self getAreaIndex:aryTmp[1] CityName:aryTmp[0]];
    strRegionAreaNo = [self getRegionAreaNo:aryTmp[1] CityName:aryTmp[0]];

    if([strRegionAreaNo isEqualToString: @"-1"])
    {
        NSDictionary *dict = [aryCityList objectAtIndex:nFirstRegionIndex];
        NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
        [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
        strRegionAreaNo = [aryChildRegion[0] objectForKey:kRegionNo];
    }
}

- (int) getHomestay
{
    
    for (int i = 0; i <self.aryServiceList.count; i++)
    {
        NSDictionary *dicInfo = self.aryServiceList[i];
        NSString *strName = [dicInfo objectForKey:@"ServiceTypeName"];
        
        if ([strName isEqualToString:@"居家式"])
        {
            return i;
        }
    }
    
    return -1;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) HiddenVfilter:(id) sender
{
    UIGestureRecognizer *gr = (UIGestureRecognizer *)sender;
    
    if (gr.view.tag == nFilterTag)
    {
        [self clickOpenFilter:nil];
    }
    
}

- (IBAction) clickOpenFilter:(id)sender
{
    self.vFilter.hidden = !self.vFilter.hidden;
    
    if (!self.vFilter.hidden)
    {
        nSpreadCell = -1;
        [self.view bringSubviewToFront:self.vFilter];
        [self.tvFilter reloadData];
        
        gestVFilterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HiddenVfilter:)];
        gestVFilterTap.delegate = self;
        [self.vFilter addGestureRecognizer:gestVFilterTap];
    }
    else
    {
        if(aryMarkerList.count != 0) [self clearMarker];
        [self addMarker];
        [self RegionClick:nil];
        
        [self.vFilter removeGestureRecognizer:gestVFilterTap];
    }
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (![gestureRecognizer isEqual:gestVFilterTap]) return YES;
    
    if (touch.view.tag == nFilterTag)
    {
        return YES;
    }
    else if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableView"])
    {
        return YES;
    }
    
    return NO;
}


- (void) setMap
{
    
    addresslocator2 = @"http://gis.tgos.nat.gov.tw/addresslocator/locate.aspx?op=dist&format=json&nogeometry=true&keystr=defaulthash&district=";//行政區定位
    
    self.vCenterInfoBG.hidden = YES;
    [self setMapCenterInfo];
    
    
    mapView_ = [[TGMapView alloc] initWithFrame:self.vMap.bounds];
    [self.vMap insertSubview:mapView_ atIndex:0];
    mapView_.delegate = self;
    
    
    //以下用來顯示使用者位置
    TGMapGPSDisplay *mapGPSDisplay_ = [[TGMapGPSDisplay alloc]init];
    mapGPSDisplay_.DirectionArcFillColor = [UIColor redColor];
    mapGPSDisplay_.DirectionArcDegree = 60.f;
    mapGPSDisplay_.DirectionArcGradientStyle = YES;
    mapGPSDisplay_.DirectionArcRadius = 150;
    mapGPSDisplay_.ArcVisibility = NO;
    [mapView_ addMapGPSDisplay:mapGPSDisplay_];
    
    [self HudShowMsg:@"資料讀取中"];
    [self addMarker];
    [self setMapNowRegion];
    //下一步到viewdidappear做
}

- (void) setMapCenterInfo
{
    self.lblCenterName.font = [UIFont boldSystemFontOfSize:nFontSize];
    
    self.vCenterInfoBG.backgroundColor = [UIColor clearColor];
    
    self.vCenterInfo.layer.masksToBounds = NO;
    self.vCenterInfo.layer.cornerRadius = 8;
    self.vCenterInfo.layer.shadowRadius = 5;
    self.vCenterInfo.layer.shadowOpacity = 0.5;
    
    self.vCenterInfo.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.vCenterInfo.bounds].CGPath;
    
    gestCenterInfoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushToVcCenterInfo)];
    [self.vCenterInfoBG addGestureRecognizer:gestCenterInfoTap];
}

#pragma mark - tgos

- (IBAction)RegionClick:(id)sender
{
    
    NSString *strTmp = [self.strCityArea stringByReplacingOccurrencesOfString:@"/" withString:@""];
    strTmp = [strTmp stringByReplacingOccurrencesOfString:@"全區" withString:@""];
    NSString * seachStr = [addresslocator2 stringByAppendingString:[strTmp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL * url = [NSURL URLWithString:seachStr];
    
    NSError *error;
    NSDictionary *ServiceJson = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url]
                                                                options:NSJSONReadingMutableLeaves
                                                                  error:&error];
    if (error == nil) {
        NSMutableDictionary *Info =[ServiceJson objectForKey:@"Information"];
        TGLatLngBounds *bounds = [[TGLatLngBounds alloc] initWithCoordinate: CLLocationCoordinate2DMake([[Info objectForKey:@"maxN"] doubleValue],[[Info objectForKey:@"minE"] doubleValue])
                                                              andCoordinate: CLLocationCoordinate2DMake([[Info objectForKey:@"minN"] doubleValue],[[Info objectForKey:@"maxE"] doubleValue])];
        TGViewerUpdate *update = [TGViewerUpdate fitBounds:bounds];//依照TGLatLngBounds的設置顯示於地圖上
        [mapView_ moveViewer:update];
    }
}

-(void)clearMarker
{
    self.lblCenterName.text = @"";
    self.vCenterInfoBG.hidden = YES;
    
    for (id<TGMarker> marker in aryMarkerList) {
        [marker setUserData:nil];
        [marker setIcon:nil];
        [marker remove];
    }
    [mapView_ clear];
    [mapView_ startRendering];
    [aryMarkerList removeAllObjects];
}

- (void) addMarker
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        for (NSDictionary *dic in self.aryShowInfo)
        {
            double dLat = [[dic objectForKey:@"Lat"] doubleValue];
            double dLng = [[dic objectForKey:@"Lng"] doubleValue];
            
            TGMarkerOptions *options1 = [[TGMarkerOptions alloc] init];
            options1.position = CLLocationCoordinate2DMake(dLat, dLng);
            options1.userData = dic;
            
            NSString *strType = [dic objectForKey:@"ServiceTypeShortName"][0];
            
            options1.icon = [self getIconImage:strType isPress:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // 更新界面
                [aryMarkerList addObject:[mapView_ addMarkerWithOptions:options1]];
                
            });
        }
    });
}


- (void) setMapNowRegion
{
    NSString *strTmp = [self.strCityArea stringByReplacingOccurrencesOfString:@"/" withString:@""];
    strTmp = [strTmp stringByReplacingOccurrencesOfString:@"全區" withString:@""];
    NSString * seachStr = [addresslocator2 stringByAppendingString:[strTmp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL * url = [NSURL URLWithString:seachStr];
    
    NSError *error;
    NSDictionary *ServiceJson = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url]
                                                                options:NSJSONReadingMutableLeaves
                                                                  error:&error];
    if (error == nil)
    {
        NSMutableDictionary *Info =[ServiceJson objectForKey:@"Information"];
        TGLatLngBounds *bounds = [[TGLatLngBounds alloc] initWithCoordinate: CLLocationCoordinate2DMake([[Info objectForKey:@"maxN"] doubleValue],[[Info objectForKey:@"minE"] doubleValue])
                                                              andCoordinate: CLLocationCoordinate2DMake([[Info objectForKey:@"minN"] doubleValue],[[Info objectForKey:@"maxE"] doubleValue])];
        

        locationUser = CLLocationCoordinate2DMake([[Info objectForKey:@"N"] doubleValue],
                                                  [[Info objectForKey:@"E"] doubleValue]);
        mapView_.viewer = [TGViewerPosition viewerWithLatitude:locationUser.latitude
                                                     longitude:locationUser.longitude
                                                          zoom:8];
        [mapView_ startRendering];

        
        TGViewerUpdate *update = [TGViewerUpdate fitBounds:bounds];//依照TGLatLngBounds的設置顯示於地圖上
        [mapView_ moveViewer:update];
    }
    
    [self HudDissmiss];
}

#pragma mark - tgosDelegate
- (BOOL)mapView:(TGMapView *)mapView didTapMarker:(id<TGMarker>)marker
{
    
    NSDictionary *dic = [marker userData];
    
    if (dic == nil ) return NO;
    
    dicNowCenterInfo = dic;
    NSDictionary *dicOldCenterInfo = [mkNowSelected userData];
    
    self.lblCenterName.text = [dic objectForKey:@"Name"];
    self.vCenterInfoBG.hidden = NO;

    NSString *strNewType = [dic objectForKey:@"ServiceTypeShortName"][0];
    NSString *strOldType = [dicOldCenterInfo objectForKey:@"ServiceTypeShortName"][0];
    
    
    [marker setIcon:[self getIconImage:strNewType isPress:YES]];//將新選取的marker換成被點選的icon
    
    [mkNowSelected setIcon:[self getIconImage:strOldType isPress:NO]];//將舊的被選取的marker icon復原
    //    [marker setPosition:defaultLocation]; //這樣可以換位置
    mkNowSelected = marker;//將新選取的marker記錄下來
    
    return YES;
}

//當畫面變動時觸發
- (void)mapView:(TGMapView *)mapView didChangeViewerPosition:(TGViewerPosition *)position
{
    
}

//當點擊地圖時觸發
- (void)mapView:(TGMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
    self.vCenterInfoBG.hidden = YES;
    
    NSString *strType = [dicNowCenterInfo objectForKey:@"ServiceTypeShortName"][0];
    [mkNowSelected setIcon:[self getIconImage:strType isPress:NO]];
}

- (void)mapView:(TGMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
}

#pragma mark - tableviewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView isEqual:self.tvFilter])
    {
        if (bFromVcMainLogin)
        {
            return 6;//非可隱藏的Cell全部都獨立一個Section
        }
        else
        {
            return 5;
        }
    }
    else
    {
        return 1;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if ([tableView isEqual:self.tvFilter])
    {
        if (bFromVcMainLogin)
        {
            if (section == nSpreadCell)
            {
                if (section == 3)
                {
                    NSArray *aryItem = [[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
                    return 1 + aryItem.count;
                }
                return 2;//展開的那個section要兩個row
            }
            
        }
        else
        {
            if (section == nSpreadCell)
            {
                if (section == 2)
                {
                    NSArray *aryItem = [[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
                    return 1 + aryItem.count;
                }
                return 2;//展開的那個section要兩個row
            }
        }
    }
    else
    {
        return self.aryShowInfo.count;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tvFilter])
    {
        if (bFromVcMainLogin)
        {
            if (indexPath.section == 0 )
            {
                return nFilterCellHeight/2;
            }
            
            if (indexPath.section == 1 ||
                indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4)
            {
                if (indexPath.row == 1 && indexPath.section != 3) return pickerHeight;
                
                if (indexPath.section == nSpreadCell)
                {
                    return nFilterCellHeight;
                }
                else
                {
                    return nFilterCellHeight + nCellInterval;
                }
                
            }
            
            if (indexPath.section == 5)
            {
                return nFilterCellHeight*2;
            }
            
        }
        else
        {
            if (indexPath.section == 0)
            {
                return nFilterCellHeight/2;
            }
            
            
            if (indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3)
            {
                if (indexPath.row == 1 && indexPath.section != 2) return pickerHeight;
                
                if (indexPath.section == nSpreadCell)
                {
                    return nFilterCellHeight;
                }
                else
                {
                    return nFilterCellHeight + nCellInterval;
                }
                
            }
            
            if (indexPath.section == 4)
            {
                return nFilterCellHeight*2;
            }
            
            
        }
    }
    else
    {
        return nCellHeight + nHeightInterval;
    }
    return nFilterCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *CellIdentifier;
    

    if ([tableView isEqual:self.tvFilter])
    {
        int nSection = (int)indexPath.section;
        int nRow = (int)indexPath.row;
        
        if (bFromVcMainLogin)
        {
            if (nSection == 0)//關鍵字選擇
            {
                CellIdentifier = [NSString stringWithFormat:@"FilterTitleCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
                vContent.backgroundColor = [UIColor clearColor];
                cell.backgroundColor =[UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.hidden = YES;
                
                return cell;
            }
            else if (nSection == 1)
            {
                CellIdentifier = [NSString stringWithFormat:@"keywordTextCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                cell.backgroundColor =[UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
                CGRect cgTmp = vContent.frame;
                cgTmp.origin.y = 0;
                cgTmp.size.height = nFilterCellHeight;
                cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
                vContent.frame = cgTmp;
                vContent.backgroundColor = [UIColor whiteColor];
                vContent.layer.cornerRadius = nCornerRadius;
                vContent.layer.masksToBounds = YES;
                vContent.layer.borderWidth = 1;
                vContent.layer.borderColor = [self colorFromHexString:kBorderColorID].CGColor;
                
                UITextField *txt = (UITextField *) [cell.contentView viewWithTag:101];
                txt.leftView = [[UIView alloc] initWithFrame:CGRectMake(0,0,nTxtInterval,txt.frame.size.height)];
                txt.leftViewMode = UITextFieldViewModeAlways;
                txt.frame = cgTmp;
                txt.font = [UIFont boldSystemFontOfSize:nFontSize];
                txt.delegate = self;
                txt.returnKeyType = UIReturnKeyDone;
                
                [txt setText:strKeyword];
                
                [aryKeyBoard addObject:txt];
                
                return cell;
            }
            else if (nSection == 2)
            {
                if (nRow == 0)
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterServiceTypeTitleCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    NSString *str = @"服務方式";
                    
                    if (nServiceType != -1)
                    {
                        str = [_aryServiceList[nServiceType] objectForKey:@"ServiceTypeName"];
                        str = [NSString stringWithFormat:@"服務方式：%@",str];
                    }
                    
                    return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:str];
                }
                else
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterServiceTypeSCCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    cell.backgroundColor =[UIColor clearColor];
                    
                    [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:301];
                }
                
            }
            else if (nSection == 3)
            {
                if (nRow == 0)
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterTypeItemTitleCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"分類選擇(可複選)"];
                }
                else
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterServiceItemCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    [self setItemCell:cell cellForRowAtIndexPath:indexPath];
                }
            }
            else if (nSection == 4)
            {
                if (nRow == 0)
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterAreaCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    if (nServiceType == nHomestayNo && nAreaTypeIndex != -1)
                    {
                        return [self setTitleCell:cell
                            cellForRowAtIndexPath:indexPath
                                        titleName:[NSString stringWithFormat:@"%@/%@",aryAreaType[nAreaTypeIndex],_strCityArea]];
                    }
                    else if(nServiceType == nHomestayNo && nHomestayNo != -1)
                    {
                        return [self setTitleCell:cell
                            cellForRowAtIndexPath:indexPath
                                        titleName:[NSString stringWithFormat:@"%@/%@",aryAreaType[1],_strCityArea]];
                    }
                    else
                    {
                        return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:_strCityArea];
                    }
                    
                    
                }
                else
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterAreaSCCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    cell.backgroundColor =[UIColor clearColor];
                    
                    [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:302];
                }
                
            }
            else if(nSection == 5)
            {
                CellIdentifier = [NSString stringWithFormat:@"FilterSearchCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                cell.backgroundColor =[UIColor clearColor];
                
                UIButton *btn = (UIButton *) [cell.contentView viewWithTag:101];
                [btn addTarget:self action:@selector(clickContitionResult:) forControlEvents:UIControlEventTouchUpInside];
            }
            
        }
        else
        {
            nSection +=4;
            
            if (nSection == 4)
            {
                CellIdentifier = [NSString stringWithFormat:@"FilterTitleCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
                vContent.backgroundColor = [UIColor clearColor];
                cell.backgroundColor =[UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.hidden = YES;
                return cell;
            }
            else if (nSection == 5)
            {
                if (nRow == 0)
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterServiceTypeTitleCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    NSString *str = @"服務方式";
                    
                    if (nServiceType != -1)
                    {
                        str = [_aryServiceList[nServiceType] objectForKey:@"ServiceTypeName"];
                        str = [NSString stringWithFormat:@"服務方式：%@",str];
                    }
                    
                    return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:str];
                }
                else
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterServiceTypeSCCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    cell.backgroundColor =[UIColor clearColor];
                    
                    [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:301];
                }
                
            }
            else if (nSection == 6)
            {
                if (nRow == 0)
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterTypeItemTitleCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"分類選擇(可複選)"];
                }
                else
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterServiceItemCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    [self setItemCell:cell cellForRowAtIndexPath:indexPath];
                }
            }
            else if (nSection == 7)
            {
                if (nRow == 0)
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterAreaCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    if (nServiceType == nHomestayNo && nAreaTypeIndex != -1)
                    {
                        return [self setTitleCell:cell
                            cellForRowAtIndexPath:indexPath
                                        titleName:[NSString stringWithFormat:@"%@/%@",aryAreaType[nAreaTypeIndex],_strCityArea]];
                    }
                    else if(nServiceType == nHomestayNo && nHomestayNo != -1)
                    {
                        return [self setTitleCell:cell
                            cellForRowAtIndexPath:indexPath
                                        titleName:[NSString stringWithFormat:@"%@/%@",aryAreaType[1],_strCityArea]];
                    }
                    else
                    {
                        return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:_strCityArea];
                    }
                    
                    
                }
                else
                {
                    CellIdentifier = [NSString stringWithFormat:@"FilterAreaSCCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    cell.backgroundColor =[UIColor clearColor];
                    
                    [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:302];
                }
                
            }
            else if(nSection == 8)
            {
                CellIdentifier = [NSString stringWithFormat:@"FilterSearchCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                cell.backgroundColor =[UIColor clearColor];
                
                UIButton *btn = (UIButton *) [cell.contentView viewWithTag:101];
                [btn addTarget:self action:@selector(clickContitionResult:) forControlEvents:UIControlEventTouchUpInside];
            }
            
        }

    }
    else
    {
        CellIdentifier = [NSString stringWithFormat:@"infoCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        return [self setInfoCell:cell cellForRowAtIndexPath:indexPath];
    
    }
    return cell;
}


- (UITableViewCell *) setInfoCell:(UITableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    NSDictionary *dicInfo = [self.aryShowInfo objectAtIndex:indexPath.row];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.x = (self.view.frame.size.width - nContentWidth) /2;
    cgTmp.origin.y = 0;
    cgTmp.size.width = nContentWidth;
    cgTmp.size.height = nCellHeight;
    
    vContent.frame = cgTmp;
    vContent.layer.cornerRadius = nCellRadius;
    vContent.layer.masksToBounds = YES;
    
    vContent.backgroundColor = [UIColor whiteColor];
    
    UILabel *lblCenterTitle = (UILabel *) [cell.contentView viewWithTag:101];
    UILabel *lblCenterTel = (UILabel *) [cell.contentView viewWithTag:102];
    
    
    lblCenterTitle.font = [UIFont boldSystemFontOfSize:nFontSize];
    lblCenterTitle.text = [dicInfo objectForKey:@"Name"];
    lblCenterTitle.frame = CGRectMake(vContent.frame.origin.x ,
                                      vContent.frame.origin.y + vContent.frame.size.height/2 - lblCenterTitle.frame.size.height /3*4,
                                      self.view.frame.size.width - vContent.frame.origin.x *4, lblCenterTitle.frame.size.height);
    
    lblCenterTel.font = [UIFont boldSystemFontOfSize:nFontSize];
    lblCenterTel.text = [NSString stringWithFormat:@"電話｜%@",[dicInfo objectForKey:@"Tel"]];
    lblCenterTel.frame = CGRectMake(lblCenterTitle.frame.origin.x,
                                    lblCenterTitle.frame.origin.y + lblCenterTitle.frame.size.height /3*4,
                                    lblCenterTel.frame.size.width, lblCenterTel.frame.size.height);
    
    UIView *vTagContent = (UIView *) [cell.contentView viewWithTag:103];
    CGRect cgTag = vTagContent.frame;
    cgTag.origin.x = 0;
    cgTag.origin.y = 0;
    vTagContent.backgroundColor = [UIColor clearColor];
    vTagContent.frame = cgTag;
    //    vTagContent.layer.cornerRadius = 13;
    //    vTagContent.layer.masksToBounds = YES;
    
    NSArray *aryTag = [dicInfo objectForKey:@"ServiceType"];
    
    for (UIView *vTmp in vTagContent.subviews)
    {
        if (vTmp.tag != 1214) continue;
        
        [vTmp removeFromSuperview];
    }
    
    int nLastX = 0;
    
    for (int i = 0; i < aryTag.count; i++)
    {
        UILabel *lbl = [[UILabel alloc] init];
        NSDictionary *dicServiceTypeItemInfo = [self getServiceTypeItemShortName:aryTag[i]];
        lbl.text = [dicServiceTypeItemInfo objectForKey:@"ItemName"];
        lbl.textColor = [UIColor whiteColor];
        [lbl sizeToFit];
        lbl.tag = 1214;
        lbl.textAlignment = NSTextAlignmentCenter;
        
        if (nLastX != 0)
        {
            nLastX = nLastX - lbl.frame.size.width - 15;
        }
        else
        {
            nLastX = vContent.frame.origin.x + vContent.frame.size.width - lbl.frame.size.width - lblCenterTitle.frame.origin.x;
        }
        
        lbl.frame = CGRectMake(nLastX,
                               vContent.frame.size.height - lbl.frame.size.height,
                               lbl.frame.size.width + 10, lbl.frame.size.height + 5);
        
        

        NSLog(@"vTagContent width %f",vTagContent.frame.size.width);
        NSLog(@"lbl width %f", lbl.frame.size.width);
        NSLog(@"1214 Label %f %f %f %f",lbl.frame.origin.x,lbl.frame.origin.y,lbl.frame.size.width,lbl.frame.size.height);
        lbl.layer.cornerRadius = 13;
        lbl.layer.masksToBounds = YES;
        lbl.backgroundColor = [self colorFromHexString:[dicServiceTypeItemInfo objectForKey:@"ItemColor"]];
        [vTagContent addSubview:lbl];
    }
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        cgTmp.size.height -= 5;
        vContent.frame = cgTmp;
        vContent.layer.cornerRadius = 35;
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if([tableView isEqual:self.tvFilter])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        int nSection = (int)indexPath.section;
        int nRow = (int)indexPath.row;
        
        int nItemSection;
        if (bFromVcMainLogin)
        {
            nItemSection = 3;
            
            if (nSection == 2 || nSection == 3 || nSection == 4)//nSection == 2 ||
            {
                if (nSpreadCell == -1) //還沒有展開的Cell
                {
                    if (indexPath.section == nItemSection && nServiceType == -1)//還沒有選資源，分類不會展開
                    {
                        [self HudErrMsgWithError:kStrLostResource finishBlock:^{
                            
                        }];
                        
                        return;
                    }
                    
                    [self SpreadClassCell:indexPath];
                    return;
                }
                
                if (indexPath.section == nSpreadCell && nRow == 0) //把已經展開的Cell關起來
                {
                    NSArray *aryServiceItem = [[NSArray alloc] init];
                    
                    if (indexPath.section == nItemSection)
                    {
                        aryServiceItem =[[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
                    }
                    
                    nSpreadCell = -1;
                    int nTmp = 1 + (int)aryServiceItem.count;
                    [self deleteCell:indexPath amount:nTmp];
                    return;
                }
                
                if (indexPath.row == 0)//展開其他cell
                {
                    if (indexPath.section == nItemSection && nServiceType == -1) //還沒有選資源，分類不會展開
                    {
                        [self HudErrMsgWithError:kStrLostResource finishBlock:^{
                            
                        }];
                        
                        return;
                    }
                    
                    [self SpreadClassCell:indexPath];
                }
                
                if (indexPath.section == nItemSection && nRow != 0)
                {
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    arySelectedServiceItemNo = [self setArySelectedServiceItemList:(int)cell.tag];
                }
                
            }
            
        }
        else
        {
            nSection +=4;
            nItemSection = 2;
            
            
            
            
            if (nSection == 2 || nSection == 5 || nSection == 6 || nSection == 7)
            {
                if (nSpreadCell == -1) //還沒有展開的Cell
                {
                    if (indexPath.section == nItemSection && nServiceType == -1)//還沒有選資源，分類不會展開
                    {
                        [self HudErrMsgWithError:kStrLostResource finishBlock:^{
                            
                        }];
                        
                        return;
                    }
                    
                    [self SpreadClassCell:indexPath];
                    return;
                }
                
                if (indexPath.section == nSpreadCell && nRow == 0) //把已經展開的Cell關起來
                {
                    NSArray *aryServiceItem = [[NSArray alloc] init];
                    
                    if (indexPath.section == nItemSection)
                    {
                        aryServiceItem =[[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
                    }
                    
                    nSpreadCell = -1;
                    int nTmp = 1 + (int)aryServiceItem.count;
                    [self deleteCell:indexPath amount:nTmp];
                    return;
                }
                
                if (indexPath.row == 0)//展開其他cell
                {
                    if (indexPath.section == nItemSection && nServiceType == -1) //還沒有選資源，分類不會展開
                    {
                        [self HudErrMsgWithError:kStrLostResource finishBlock:^{
                            
                        }];
                        
                        return;
                    }
                    
                    [self SpreadClassCell:indexPath];
                }
                
                if (indexPath.section == nItemSection && nRow != 0)
                {
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    arySelectedServiceItemNo = [self setArySelectedServiceItemList:(int)cell.tag];
                }
                
            }
            
        }

    }
    else
    {
        dicNowCenterInfo = self.aryShowInfo[indexPath.row];
        [self pushToVcCenterInfo];
    }
    
   
}

#pragma mark - 資料整理

-(NSArray *) setArySelectedServiceItemList:(int) nItemNo
{
    NSMutableArray *aryNewSelected = [arySelectedServiceItemNo mutableCopy];
    BOOL bAlreadySelected = false;
    
    for (NSDictionary *dicItem in aryNewSelected)
    {
        if (nItemNo != [[dicItem objectForKey:@"ItemNo"] intValue]) continue;
        
        bAlreadySelected = YES;
        
        [aryNewSelected removeObject:dicItem];
        
        break;
    }
    
    if (bAlreadySelected)
    {
        [self.tvFilter reloadData];
        return aryNewSelected;
    }
    else
    {
        [aryNewSelected addObject:@{@"ItemNo":[NSString stringWithFormat:@"%i",nItemNo],
                                    @"ItemName":@""}];
        
        NSArray *aryTmp = aryNewSelected;
        
        NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"ItemNo" ascending:YES];
        NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
        aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
        
        [self.tvFilter reloadData];
        return aryTmp;
    }
}



- (NSDictionary *) getServiceTypeItemShortName:(NSString *) strOriName
{
    
    if ([strOriName isEqualToString:@"養護型機構"])
    {
        return @{@"ItemName":@"養護機構",@"ItemColor":kOrganizationColor};
    }
    else if ([strOriName isEqualToString:@"長期照護型機構"])
    {
         return @{@"ItemName":@"長照機構",@"ItemColor":kOrganizationColor};
    }
    else if ([strOriName isEqualToString:@"失智照顧型機構"])
    {
        return @{@"ItemName":@"失智機構",@"ItemColor":kOrganizationColor};
    }
    else if ([strOriName isEqualToString:@"護理之家"])
    {
        return @{@"ItemName":@"護理之家",@"ItemColor":kOrganizationColor};
    }
    else if ([strOriName isEqualToString:@"榮民之家"])
    {
        return @{@"ItemName":@"榮民之家",@"ItemColor":kOrganizationColor};
    }
    else if ([strOriName isEqualToString:@"日間照顧服務"])
    {
        return @{@"ItemName":@"日照服務",@"ItemColor":kCommunityColor};
    }
    else if ([strOriName isEqualToString:@"家庭托顧服務"])
    {
        return @{@"ItemName":@"家托服務",@"ItemColor":kCommunityColor};
    }
    else if ([strOriName isEqualToString:@"失智症老人團體家屋"])
    {
        return @{@"ItemName":@"失智家屋",@"ItemColor":kCommunityColor};
    }
    else if ([strOriName isEqualToString:@"多元照顧中心（小規模多機能）"])
    {
        return @{@"ItemName":@"多元照顧",@"ItemColor":kCommunityColor};
    }
    else if ([strOriName isEqualToString:@"交通接送服務"])
    {
        return @{@"ItemName":@"交通接送",@"ItemColor":kCommunityColor};
    }
    else if ([strOriName isEqualToString:@"餐飲服務（社區共餐）"])
    {
        return @{@"ItemName":@"社區共餐",@"ItemColor":kCommunityColor};
    }
    else if ([strOriName isEqualToString:@"社區復健"])
    {
        return @{@"ItemName":@"社區復健",@"ItemColor":kCommunityColor};
    }
    else if ([strOriName isEqualToString:@"居家服務"])
    {
        return @{@"ItemName":@"居家服務",@"ItemColor":kHomeServiceColor};
    }
    else if ([strOriName isEqualToString:@"居家喘息"])
    {
        return @{@"ItemName":@"居家喘息",@"ItemColor":kHomeServiceColor};
    }
    else if ([strOriName isEqualToString:@"居家護理"])
    {
        return @{@"ItemName":@"居家護理",@"ItemColor":kHomeServiceColor};
    }
    else if ([strOriName isEqualToString:@"居家復健"])
    {
        return @{@"ItemName":@"居家復健",@"ItemColor":kHomeServiceColor};
    }
    else if ([strOriName isEqualToString:@"居家無障礙環境改善"])
    {
        return @{@"ItemName":@"居無障礙",@"ItemColor":kHomeServiceColor};
    }
    else if ([strOriName isEqualToString:@"餐飲服務（送餐）"])
    {
        return @{@"ItemName":@"送餐服務",@"ItemColor":kHomeServiceColor};
    }
    else if ([strOriName isEqualToString:@"輔具服務"])
    {
        return @{@"ItemName":@"輔具服務",@"ItemColor":kHomeServiceColor};
    }
    else if ([strOriName isEqualToString:@"安養服務"])
    {
        return @{@"ItemName":@"安養服務",@"ItemColor":kOtherColor};
    }
    else if ([strOriName isEqualToString:@"老人公寓"])
    {
        return @{@"ItemName":@"老人公寓",@"ItemColor":kOtherColor};
    }
    else if ([strOriName isEqualToString:@"社區照顧關懷據點"])
    {
        return @{@"ItemName":@"社照據點",@"ItemColor":kOtherColor};
    }
    else if ([strOriName isEqualToString:@"身障居家服務"])
    {
        return @{@"ItemName":@"身障居服",@"ItemColor":kDisabled};
    }
    else if ([strOriName isEqualToString:@"身障家庭托顧（ICF系統）"])
    {
        return @{@"ItemName":@"身障家托",@"ItemColor":kDisabled};
    }
    else if ([strOriName isEqualToString:@"身障臨短托（ICF系統）"])
    {
        return @{@"ItemName":@"身障臨托",@"ItemColor":kDisabled};
    }
    else if ([strOriName isEqualToString:@"縣市身障輔具資源中心（輔具入口網）"])
    {
        return @{@"ItemName":@"輔具中心",@"ItemColor":kDisabled};
    }
    else if ([strOriName isEqualToString:@"身障機構（日間服務）"])
    {
        return @{@"ItemName":@"身障日間",@"ItemColor":kDisabled};
    }
    else if ([strOriName isEqualToString:@"身障機構（全日住宿）"])
    {
        return @{@"ItemName":@"身障全日",@"ItemColor":kDisabled};
    }
    else if ([strOriName isEqualToString:@"身障機構（夜宿型）"])
    {
        return @{@"ItemName":@"身障夜宿",@"ItemColor":kDisabled};
    }
    
    
    return @{@"ItemName":strOriName,@"ItemColor":kDisabled};
}

- (UIImage *) getIconImage:(NSString *) strType isPress:(BOOL) isPress
{
    if ([strType isEqualToString:@"機構住宿式"])
    {
        if (isPress)
        {
            return imgOrganizationPress;
        }
        else
        {
            return imgOrganization;
        }
    }
    else if ([strType isEqualToString:@"社區式"])
    {
        if (isPress)
        {
            return imgCommunityPress;
        }
        else
        {
            return imgCommunity;
        }
    }
    else if ([strType isEqualToString:@"居家式"])
    {
        if (isPress)
        {
            return imgHomeServicePress;
        }
        else
        {
            return imgHomeService;
        }
    }
    else if ([strType isEqualToString:@"其他"])
    {
        if (isPress)
        {
            return imgOtherPress;
        }
        else
        {
            return imgOther;
        }
    }
    else if ([strType isEqualToString:@"身障資源"])
    {
        if (isPress)
        {
            return imgDisabledPress;
        }
        else
        {
            return imgDisabled;
        }
    }
    
    
    return [UIImage new];
}

#pragma mark - push到其他頁面

- (void) pushToVcCenterInfo
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcCenterInfo* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcCenterInfo"];
    vc.dicCenterInfo = dicNowCenterInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)pushToVcPathPlanning:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcPathPlanning* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcPathPlanning"];
    vc.dicCenterInfo = dicNowCenterInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - vcBase

- (void) clickRightButton:(id)sender
{
    if (self.tvResult.hidden)
    {
        
        [btnRight setBackgroundImage:[UIImage imageNamed:kMapIcon] forState:UIControlStateNormal];
    }
    else
    {
        [btnRight setBackgroundImage:[UIImage imageNamed:kListIcon] forState:UIControlStateNormal];
    }
    
    self.tvResult.hidden = !self.tvResult.hidden;
    self.vMap.hidden = !self.vMap.hidden;
}

#pragma mark - 各式Cell
- (UITableViewCell *) setTitleCell:(UITableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath titleName:(NSString *) strName
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nFilterCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    vContent.layer.cornerRadius = nCornerRadius;
    vContent.layer.masksToBounds = YES;
    vContent.layer.borderWidth = 1;
    vContent.layer.borderColor = [self colorFromHexString:kBorderColorID].CGColor;
    
    UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
    lblTitleName.text = strName;
    
    UIImageView *imgArrow = (UIImageView *) [cell.contentView viewWithTag:102];
    
    CGRect cgImg = imgArrow.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    cgImg.origin.y = (nFilterCellHeight - nArrow) /2;
    cgImg.origin.x = cgTmp.size.width - nArrow*4/3;
    imgArrow.frame = cgImg;
    
    CGRect cgTitle = lblTitleName.frame;
    cgTitle.origin.y = cgTmp.origin.y;
    cgTitle.origin.x = cgTmp.origin.x + kTxtInterval;
    cgTitle.size.height = cgTmp.size.height;
    lblTitleName.frame = cgTitle;
    lblTitleName.font = [UIFont systemFontOfSize:nFontSize];
    
    return cell;
}

- (UITableViewCell *) setScCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath ScrollViewTag:(int) nTag
{
    if (nTag == 300)
    {
        UIPickerView *pv300 = (UIPickerView *) [cell.contentView viewWithTag:nTag];
        pv300.delegate = self;
        pv300.dataSource = self;
        pv300.backgroundColor = [UIColor whiteColor];
        
        CGRect cgPV =pv300.frame;
        cgPV.size.width = cell.contentView.frame.size.width - cgPV.origin.x * 2;
        cgPV.size.height = pickerHeight;
        pv300.frame = cgPV;
        
        if (nFirstKeywordRegionIndex != -1)
        {
            nRegionKeywordSelectedIndex = nFirstKeywordRegionIndex;
            nAreaKeywordSelectedIndex = nFirstKeywordAreaIndex;
            
            [pv300 selectRow:nFirstKeywordRegionIndex inComponent:0 animated:YES];
            [pv300 selectRow:nFirstKeywordAreaIndex inComponent:1 animated:YES];
            
            nFirstKeywordRegionIndex = -1;
            nFirstKeywordAreaIndex = -1;
        }
        else
        {
            [pv300 selectRow:nRegionKeywordSelectedIndex inComponent:0 animated:YES];
            [pv300 selectRow:nAreaKeywordSelectedIndex inComponent:1 animated:YES];
        }
        
        
    }
    else if (nTag == 301)
    {
        
        
        UIPickerView *pv301 = (UIPickerView *) [cell.contentView viewWithTag:nTag];
        pv301.delegate = self;
        pv301.dataSource = self;
        pv301.backgroundColor = [UIColor whiteColor];
        
        if (isUpper9)
        {
            [pv301 setTranslatesAutoresizingMaskIntoConstraints:NO];
            [pv301.centerXAnchor constraintEqualToAnchor:cell.layoutMarginsGuide.centerXAnchor].active = YES;
        }

        
        CGRect cgPV =pv301.frame;
        cgPV.size.width = cell.contentView.frame.size.width - cgPV.origin.x * 2;
        cgPV.size.height = pickerHeight;
        pv301.frame = cgPV;
        
        if (bFromVcMainLogin && nServiceType == -1)
        {
            nServiceType = 0;
        }
        else
        {
            [pv301 selectRow:nServiceType inComponent:0 animated:YES];
        }
        
    }
    else if(nTag == 302)
    {
        UIPickerView *pv302 = (UIPickerView *) [cell.contentView viewWithTag:302];
        pv302.backgroundColor = [UIColor whiteColor];
        
        if (isUpper9)
        {
            [pv302 setTranslatesAutoresizingMaskIntoConstraints:NO];
            [pv302.centerXAnchor constraintEqualToAnchor:cell.layoutMarginsGuide.centerXAnchor].active = YES;
        }

        
        UIPickerView *pv303 = (UIPickerView *) [cell.contentView viewWithTag:303];//居服使用
        pv303.backgroundColor = [UIColor whiteColor];
        
        if (isUpper9)
        {
            [pv303 setTranslatesAutoresizingMaskIntoConstraints:NO];
            [pv303.centerXAnchor constraintEqualToAnchor:cell.layoutMarginsGuide.centerXAnchor].active = YES;
        }

        
        if (nServiceType == nHomestayNo && nHomestayNo != -1)
        {
            pv302.hidden = YES;
            pv303.hidden = NO;
            pv303.delegate = self;
            pv303.dataSource = self;
            pv302.delegate = nil;
            pv302.dataSource = nil;
        }
        else
        {
            pv302.hidden = NO;
            pv303.hidden = YES;
            pv303.delegate = nil;
            pv303.dataSource = nil;
            pv302.delegate = self;
            pv302.dataSource = self;
            
        }
        CGRect cgPV =pv302.frame;
        cgPV.size.width = cell.contentView.frame.size.width - cgPV.origin.x * 2;
        cgPV.size.height = pickerHeight;
        
        pv302.frame = cgPV;
        pv303.frame = cgPV;
        
        if (nFirstAreaIndex != -1)
        {
            nRegionSelectedIndex = nFirstRegionIndex;
            nAreaSelectedIndex = nFirstAreaIndex;
            
            if (nServiceType == nHomestayNo && nHomestayNo != -1)
            {
                [pv303 selectRow:kAreaTypeIndexDefault inComponent:0 animated:YES];
                [pv303 selectRow:nFirstRegionIndex inComponent:1 animated:YES];
                [pv303 selectRow:nFirstAreaIndex inComponent:2 animated:YES];
            }
            else
            {
                [pv302 selectRow:nFirstRegionIndex inComponent:0 animated:YES];
                [pv302 selectRow:nFirstAreaIndex inComponent:1 animated:YES];
            }
            
            nFirstRegionIndex = -1;
            nFirstAreaIndex = -1;
        }
        else
        {
            if (nServiceType == nHomestayNo && nHomestayNo != -1)
            {
                if (nAreaTypeIndex == -1)
                {
                    nAreaTypeIndex = kAreaTypeIndexDefault;
                }
                
                [pv303 selectRow:nAreaTypeIndex inComponent:0 animated:YES];
                [pv303 selectRow:nRegionSelectedIndex inComponent:1 animated:YES];
                [pv303 selectRow:nAreaSelectedIndex inComponent:2 animated:YES];
            }
            else
            {
                nAreaTypeIndex = -1;
                [pv302 selectRow:nRegionSelectedIndex inComponent:0 animated:YES];
                [pv302 selectRow:nAreaSelectedIndex inComponent:1 animated:YES];
            }
        }
        
    }
    
    return cell;
}

- (UITableViewCell *) setItemCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nFilterCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    
    int nRow = (int)indexPath.row;
    
    UIImageView* imgSelect = (UIImageView *) [cell.contentView viewWithTag:101];
    CGRect cgImg = imgSelect.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    imgSelect.frame = cgImg;
    
    NSArray *aryServiceItem =[[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
    UILabel *lblItemName = (UILabel *) [cell.contentView viewWithTag:102];
    CGRect cgLabel = lblItemName.frame;
    cgLabel.size.height = nFilterCellHeight;
    cgLabel.origin.x = cgImg.origin.x + cgImg.size.width + 5;
    cgLabel.origin.y = 0;
    lblItemName.frame = cgLabel;
    [lblItemName setFont:[UIFont systemFontOfSize:nFontSize]];
    
    NSString *strItemName = [[aryServiceItem objectAtIndex:nRow-1] objectForKey:@"ItemName"];
    [lblItemName setText:strItemName];
    
    int nItemNo = [[[aryServiceItem objectAtIndex:nRow-1] objectForKey:@"ItemNo"] intValue];
    cell.tag = nItemNo;
    
    BOOL bIsSelected = false;
    
    for (int i = 0; i < arySelectedServiceItemNo.count; i++)
    {
        if (cell.tag != [[arySelectedServiceItemNo[i] objectForKey:@"ItemNo"] intValue]) continue;
        
        bIsSelected = YES;
    }
    
    if (bIsSelected)
    {
        [imgSelect setImage:[UIImage imageNamed:@"check01"]];
    }
    else
    {
        [imgSelect setImage:[UIImage imageNamed:@"check02"]];
    }
    
    
    return cell;
}

#pragma mark - picker

//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
//{
//    if (pickerView.tag == 303)
//    {
//        if (component != 0) return pickerView.frame.size.width/3 - 2;
//        
//        return pickerView.frame.size.width/3 + 4;
//    }
//    
//    return pickerView.frame.size.width/2;
//}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerRowHeight;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if(pickerView.tag == 300)
    {
        return 2;
    }
    else if (pickerView.tag == 301)
    {
        return 1;
    }
    else if(pickerView.tag == 302)
    {
        return 2;
    }
    else if(pickerView.tag == 303) //居服地區選擇用
    {
        return 3;
    }
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView.tag == 300)
    {
        if (component == 0)
        {
            return aryCityList.count;
        }
        else
        {
            NSDictionary *dict = [aryCityList objectAtIndex:nRegionKeywordSelectedIndex];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            return [aryChildRegion count];
        }
    }
    else if (pickerView.tag == 301)
    {
        return _aryServiceList.count;
    }
    else  if(pickerView.tag == 302)
    {
        if (component == 0)
        {
            return aryCityList.count;
        }
        else
        {
            NSDictionary *dict = [aryCityList objectAtIndex:nRegionSelectedIndex];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            return [aryChildRegion count];
        }
    }
    else if (pickerView.tag == 303)
    {
        if (component == 0)
        {
            return aryAreaType.count;
        }
        else if (component == 1)
        {
            return aryCityList.count;
        }
        else if(component == 2)
        {
            NSDictionary *dict = [aryCityList objectAtIndex:nRegionSelectedIndex];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            return [aryChildRegion count] ;
        }
        
    }
    return _aryServiceList.count;
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* pickerLabel = (UILabel*)view;
    
    pickerLabel = [[UILabel alloc] init];
    pickerLabel.font = [UIFont systemFontOfSize:nFontSize];
    pickerLabel.textAlignment=NSTextAlignmentCenter;
    
    if (pickerView.tag == 300)
    {
        if (component == 0)
        {
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSString *strCity = [dict objectForKey:kRegionName];
            
            pickerLabel.text = strCity;
            
            return pickerLabel;
        }
        else
        {
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:0]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            pickerLabel.text = strArea;
            
            return pickerLabel;
        }
        
    }
    else if (pickerView.tag == 301)
    {
        if (arySelectedServiceItemNo.count == 0 || !arySelectedServiceItemNo)
        {
            arySelectedServiceItemNo = [[_aryServiceList objectAtIndex:row] objectForKey:@"Item"];
        }
        
        NSString *strName = [_aryServiceList[row] objectForKey:@"ServiceTypeName"];
        [self changeCellTitle:[NSString stringWithFormat:@"服務方式：%@",strName]];
        
        pickerLabel.text = [_aryServiceList[row] objectForKey:@"ServiceTypeName"];
        
        return pickerLabel;
    }
    else if (pickerView.tag == 302)
    {
        if (component == 0)
        {
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSString *strCity = [dict objectForKey:kRegionName];
            
            pickerLabel.text = strCity;
            
            return pickerLabel;
        }
        else
        {
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:0]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            pickerLabel.text = strArea;
            
            return pickerLabel;
            
        }
    }
    else if (pickerView.tag == 303)
    {
        if (component == 0)
        {
            pickerLabel.text = aryAreaType[row];
            
            return pickerLabel;
        }
        else if (component == 1)
        {
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSString *strCity = [dict objectForKey:kRegionName];
            
            pickerLabel.text = strCity;
            
            return pickerLabel;
        }
        else
        {
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:1]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            pickerLabel.text = strArea;
            
            return pickerLabel;
        }
    }
    
    return pickerLabel;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 300)
    {
        if (component == 0) {
            nRegionKeywordSelectedIndex = (int)row;
            
            [pickerView reloadComponent:1];
            [pickerView selectRow:0 inComponent:1 animated:YES];
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[0] objectForKey:kRegionName];
            strKeywordCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            nAreaKeywordSelectedIndex = 0;
            strKeywordregionAreaNo = [aryChildRegion[nAreaKeywordSelectedIndex] objectForKey:kRegionNo];
            
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@",strCity,strArea]];
            
        }
        else if(component == 1)
        {
            nAreaKeywordSelectedIndex = (int)row;
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:0]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            strKeywordCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strKeywordregionAreaNo = [aryChildRegion[nAreaKeywordSelectedIndex] objectForKey:kRegionNo];
            
            [self changeCellTitle:strKeywordCityArea];
            
        }
        
    }
    else if (pickerView.tag == 301)
    {
        nAreaTypeIndex = -1;
        nServiceType = (int)row;
        arySelectedServiceItemNo = [[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
        
        NSString *strName = [[_aryServiceList objectAtIndex:row] objectForKey:@"ServiceTypeName"];
        [self changeCellTitle:[NSString stringWithFormat:@"服務方式：%@",strName]];
        
        int nCityAreaCell;
        if (bFromVcMainLogin)
        {
            nCityAreaCell = 4;
        }
        else
        {
            nCityAreaCell = 3;
        }
        
        if ([[[_aryServiceList objectAtIndex:row] objectForKey:@"ServiceTypeName"] isEqual:@"居家式"])
        {
            int nTmp = nSpreadCell;
            
            nSpreadCell = nCityAreaCell;
            nAreaTypeIndex = kAreaTypeIndexDefault;
            
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@",
                                   aryAreaType[nAreaTypeIndex],
                                   _strCityArea]];
            nSpreadCell = nTmp;
        }
        else
        {
            nAreaTypeIndex = -1;
            int nTmp = nSpreadCell;
            nSpreadCell = nCityAreaCell;
            
            [self changeCellTitle:_strCityArea];
            nSpreadCell = nTmp;
        }
    }
    else if(pickerView.tag == 302)
    {
        if (component == 0) {
            nRegionSelectedIndex = (int)row;
            
            [pickerView reloadComponent:1];
            [pickerView selectRow:0 inComponent:1 animated:YES];
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[0] objectForKey:kRegionName];
            _strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            nAreaSelectedIndex = 0;
            strRegionAreaNo = [aryChildRegion[nAreaSelectedIndex] objectForKey:kRegionNo];
            
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@",strCity,strArea]];
            
        }
        else if(component == 1)
        {
            nAreaSelectedIndex = (int)row;
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:0]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            _strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[row] objectForKey:kRegionNo];
            
            
            [self changeCellTitle:_strCityArea];
            
        }
        
    }
    else if (pickerView.tag == 303)
    {
        if (component == 0)
        {
            nAreaTypeIndex =(int) row;
            
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:1]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[[pickerView selectedRowInComponent:2]] objectForKey:kRegionName];
            
            _strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[[pickerView selectedRowInComponent:2]] objectForKey:kRegionNo];
            
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@/%@",aryAreaType[row],strCity,strArea]];
        }
        else if (component == 1)
        {
            nRegionSelectedIndex = (int)row;
            
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:2 animated:YES];
            
            NSDictionary *dict = [aryCityList objectAtIndex:row];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[0] objectForKey:kRegionName];
            
            _strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            nAreaSelectedIndex = 0;
            strRegionAreaNo = [aryChildRegion[nAreaSelectedIndex] objectForKey:kRegionNo];
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@/%@",aryAreaType[[pickerView selectedRowInComponent:0]],
                                   strCity,strArea]];
            
        }else if(component == 2)
        {
            nAreaSelectedIndex = (int)row;
            
            NSDictionary *dict = [aryCityList objectAtIndex:[pickerView selectedRowInComponent:1]];
            NSMutableArray *aryChildRegion = [[self getOneRegionListAtParentNo:[dict objectForKey:kRegionNo]] mutableCopy];
            [aryChildRegion insertObject:[self makeAllChildRegionDicFromRegionNo:[dict objectForKey:kRegionNo]] atIndex:0];
            
            NSString *strCity = [dict objectForKey:kRegionName];
            NSString *strArea = [aryChildRegion[row] objectForKey:kRegionName];
            
            _strCityArea = [NSString stringWithFormat:@"%@/%@",strCity,strArea];
            strRegionAreaNo = [aryChildRegion[row] objectForKey:kRegionNo];
            [self changeCellTitle:[NSString stringWithFormat:@"%@/%@/%@",
                                   aryAreaType[[pickerView selectedRowInComponent:0]],
                                   strCity,strArea]];
        }
    }
    
}


#pragma mark - IBAction

- (IBAction) clickContitionResult:(id)sender
{
    [self closeKeyBoard];
    
    if (nServiceType == -1 && [strKeyword isEqualToString:@""])
    {
        [self HudErrMsgWithError:kStrLostResource finishBlock:^{}];
        
        return;
    }
    
    [self HudShowMsg:@"資料下載中..."];
    
    NSArray *arySelectedNo = [arySelectedServiceItemNo valueForKey:@"ItemNo"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        if (nAreaTypeIndex == -1)
        {
            [soap getCareCenterData:arySelectedNo RegionNo:strRegionAreaNo IsHomeServiceRegion:false CenterName:strKeyword];
        }
        else
        {
            [soap getCareCenterData:arySelectedNo RegionNo:strRegionAreaNo IsHomeServiceRegion:(BOOL)nAreaTypeIndex CenterName:strKeyword];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // 更新界面
            
            
        });
    });
}

#pragma mark - keyboard from Base

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    tapKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyBoard)];
    tapKeyBoard.delegate = self;
    [self.vFilter addGestureRecognizer:tapKeyBoard];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self closeKeyBoard];
    //    [self clickSearchText:textField];
//    [self setNowKeyword:textField.text];
    return YES;
}

- (void) closeKeyBoard
{
    UIView* vTextField = [self getNowKeyBoardTxt];
    if(vTextField == nil) return;
    
    UITextField *txt = (UITextField *) vTextField;
    
    [self setNowKeyword:txt.text];
    [txt resignFirstResponder];
}

- (UIView*) getNowKeyBoardTxt
{
    for (UIView* vTmp in aryKeyBoard) {
        if([vTmp isFirstResponder])
            return  vTmp;
    }
    
    return nil;
}

- (void) keyboardWillChange:(NSNotification *)notification
{
    
    cgKeyBoard = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    cgKeyBoard = [self.view convertRect:cgKeyBoard fromView:nil]; //this is it!
    
    //    [self moveKeyboardPosition:cgKeyBoard];
}

-(IBAction) setNowKeyword:(NSString *) strNewKeyword
{
    
    [self.vFilter removeGestureRecognizer:tapKeyBoard];
    
    NSLog(@"strNewKeyword %@",strNewKeyword);
    strKeyword = strNewKeyword;
    
}

#pragma mark - 資料整理

- (NSDictionary *) makeAllChildRegionDicFromRegionNo:(NSString *) strRegionNo
{
    return @{kRegionNo:strRegionNo,
             kRegionName:@"全區",
             kRegionParentNo:strRegionNo};
//    用longlong值 0開頭的No會不正確
//    return @{kRegionNo:[NSNumber numberWithLongLong:[strRegionNo longLongValue]],
//             kRegionName:@"全區",
//             kRegionParentNo:[NSNumber numberWithLongLong:[strRegionNo longLongValue]]};
}

- (NSArray *) getResetServiceType:(NSArray *) aryOri
{
    NSArray *aryAll = aryOri;
    NSMutableArray *ary = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in aryAll)
    {
        if (ary.count == 0)
        {
            [ary addObject:[dic objectForKey:@"ServiceTypeName"]];
        }
        else
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(self CONTAINS %@)", [dic objectForKey:@"ServiceTypeName"]];
            NSArray *aryTmp = [ary filteredArrayUsingPredicate:pred];
            
            if (aryTmp.count == 0) [ary addObject:[dic objectForKey:@"ServiceTypeName"]];
        }
    }
    
    NSMutableArray *aryFinally = [[NSMutableArray alloc] init];
    
    
    for (int i =0; i<ary.count; i++)
    {
        NSString *strType = ary[i];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(ServiceTypeName CONTAINS %@)", strType];
        NSArray *aryTmp = [aryAll filteredArrayUsingPredicate:pred];
        
        NSMutableArray *aryOneTypeItem = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dicTmp in aryTmp)
        {
            [aryOneTypeItem addObject:@{
                                        @"ItemNo":[dicTmp objectForKey:@"ItemNo"],
                                        @"ItemName":[dicTmp objectForKey:@"ItemName"]
                                        }];
            
        }
        
        if ([strType isEqualToString:@"居家式"]) nHomestayNo = i;
        
        [aryFinally addObject:@{
                                @"ServiceTypeName":strType,
                                @"Item":aryOneTypeItem
                                }];
        
    }
    
    return aryFinally;
}

- (NSArray *) getRegionCityList
{
    NSString *strPred = [NSString stringWithFormat:@"ParentNo == '-1'"];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    NSMutableArray* aryNew = [NSMutableArray new];
    for (NSDictionary *dic in aryTmp)
    {
        NSMutableDictionary *dicNew = [dic mutableCopy];
        NSString *strSort = [dicNew objectForKey:@"Sort"];
        if(strSort.length == 1)
        {
            NSString *strInt = [NSString stringWithFormat:@"%02i",[strSort intValue]];
            [dicNew setObject:strInt forKey:@"Sort"];
        }
        [aryNew addObject:[dicNew copy]];
    }
    aryTmp = aryNew;
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"Sort" ascending:YES];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
    
    return aryTmp;
}

- (NSArray *) getFilterRegionListWithResposeData:(NSArray *) aryResposeData
{
    NSMutableArray *aryNew = [NSMutableArray new];
    
    BOOL bUseParentNo = ([strKeywordCityArea rangeOfString:@"全區"].location != NSNotFound);
    
    if (bUseParentNo)
    {
        NSArray *aryCityTmp = [self getOneRegionListAtParentNo:strKeywordregionAreaNo];
        
        for (NSDictionary *dicRespose in aryResposeData)
        {
            for (NSDictionary *dicCityInfo in aryCityTmp)
            {
                if (![[dicRespose objectForKey:@"RegionNo"] isEqualToString:[dicCityInfo objectForKey:kRegionNo]]) continue;
                
                [aryNew addObject:dicRespose];
                break;
            }
        }
    }
    else
    {
        for (NSDictionary *dic in aryResposeData)
        {
            NSMutableDictionary *dicNew = [dic mutableCopy];
            if (![[dicNew objectForKey:@"RegionNo"] isEqual:strKeywordregionAreaNo]) continue;
            
            [aryNew addObject:dicNew];
        }
    }
    
    return aryNew;
}

- (NSArray *) getOneRegionListAtParentNo:(NSString *) strParentNo
{
    NSString *strPred = [NSString stringWithFormat:@"ParentNo == '%@'",strParentNo];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    NSMutableArray* aryNew = [NSMutableArray new];
    for (NSDictionary *dic in aryTmp)
    {
        NSMutableDictionary *dicNew = [dic mutableCopy];
        NSString *strSort = [dicNew objectForKey:@"Sort"];
        if(strSort.length == 1)
        {
            NSString *strInt = [NSString stringWithFormat:@"%02i",[strSort intValue]];
            [dicNew setObject:strInt forKey:@"Sort"];
        }
        [aryNew addObject:[dicNew copy]];
    }
    aryTmp = aryNew;
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"Sort" ascending:YES];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
    
    return aryTmp;
}

- (NSString *) getRegionAreaNo:(NSString *) strArea CityName:(NSString *) strCity
{
    NSString *strPred = [NSString stringWithFormat:@"Name == '%@'",strArea];
    NSPredicate *pred = [NSPredicate predicateWithFormat:strPred];
    
    NSArray *aryTmp = [aryArea filteredArrayUsingPredicate:pred];
    NSArray *aryCityTmp = [self getRegionCityList];
    
    NSString *strNo = @"-1";
    
    for (int i = 0; i<aryTmp.count; i++)
    {
        NSDictionary *dicAreaTmp =aryTmp[i];
        
        for (int j = 0; j < aryCityTmp.count; j++)
        {
            
            NSString *strCityNoTmp =[aryCityTmp[j] objectForKey:kRegionNo];
            NSString *strAreaCityNoTmp = [dicAreaTmp objectForKey:kRegionParentNo];
            NSString *strCityNameTmp = [aryCityTmp[j] objectForKey:kRegionName];
            
            if ([strAreaCityNoTmp isEqualToString:strCityNoTmp]
                && [strCityNameTmp isEqualToString:strCity])
            {
                strNo = [dicAreaTmp objectForKey:kRegionNo];
            }
            
            
        }
        
    }
    
    return strNo;
}

- (int) getRegionIndex:(NSString *) strRegion
{
    
    NSArray *aryCityTmp = [self getRegionCityList];
    int nIndex = -1;
    
    for (int i = 0; i < aryCityTmp.count; i++)
    {
        NSDictionary *dicCity = aryCityTmp[i];
        if (![[dicCity objectForKey:kRegionName] isEqual:strRegion]) continue;
        
        nIndex = i;
    }
    
    return nIndex;
    
}

- (int) getAreaIndex:(NSString *) strArea CityName:(NSString *) strCity
{
    
    NSArray *aryTmp = [self getRegionCityList];
    NSDictionary *dicRegion = aryTmp[nFirstRegionIndex];
    NSMutableArray *aryCityTmp = [[self getOneRegionListAtParentNo:[dicRegion objectForKey:kRegionNo]] mutableCopy];
    [aryCityTmp insertObject:[self makeAllChildRegionDicFromRegionNo:[dicRegion objectForKey:kRegionNo]] atIndex:0];
    
    int nIndex = -1;
    for (int i = 0; i < aryCityTmp.count; i++)
    {
        NSDictionary *dicArea = aryCityTmp[i];
        if (![[dicArea objectForKey:kRegionName] isEqual:strArea]) continue;
        nIndex = i;
        
    }
    
    return nIndex;
}

#pragma mark - bar名稱改變

- (void) changeCellTitle:(NSString *) strNewTitle
{
    
    UITableViewCell *cell = [self.tvFilter cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:nSpreadCell]];
    
    for (UIView *v100 in cell.contentView.subviews) {
        
        for(UIView *vTmp in [v100 subviews])
        {
            if (![vTmp isKindOfClass:[UILabel class]]) continue;
            if (vTmp.tag == 500) continue;
            
            
            UILabel *lbl = (UILabel *) vTmp;
            lbl.text = strNewTitle;
        }
        
    }
    
}

#pragma mark - Soap Delegate

- (void) loadFinishRespose:(NSArray *) resposeData userInfo:(NSString *)UserInfo error:(NSError *)error
{
    if (error)
    {
        [self HudErrMsgWithError:@"" finishBlock:^{
            UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:UserInfo message:error.localizedDescription delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [alertError show];
        }];
    }
    else if (resposeData.count == 0)
    {
        if (bFromVcMainLogin)
        {
            if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenterList] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{} Title:nil];
                
            }
            else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenter] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{
                } Title:nil];
            }
            return;
        }
        else
        {
            if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceSurveyResult] ])
            {
                
                [self HudDismissWithError:@"" finishBlock:^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"查詢結果" message:@"無適合的資源" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles: nil];
                    alert.delegate = self;
                    [alert show];
                    
                } Title:nil];
                
                
            }
            else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenterList] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{
                } Title:nil];
                
                return;
            }
            else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenter] ])
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{
                } Title:nil];
                return;
            }
        }
    }
    else
    {
        
        if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenterList] ])
        {
            aryResult = resposeData;
            
            [self HudDismissWithSuccess:@"下載完成" finishBlock:^{
//                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//                vcFilterResult* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcFilterResult"];
//                vc.aryShowInfo = aryResult;
//                vc.strCityArea = _strCityArea;
//                [self.navigationController pushViewController:vc animated:YES];
                self.aryShowInfo = aryResult;
                [self showResultAlert:(int)self.aryShowInfo.count];

                [self.tvResult reloadData];
                [self clickOpenFilter:nil];
                
            }];
            
        }
        else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceSurveyResult] ])
        {
            _aryServiceList = [self getResetServiceType:resposeData];
            
            [self HudDismissWithSuccess:@"下載完成" finishBlock:^{
                
                [self.tvFilter reloadData];
                
                
            }];
            
        }
        else if ([UserInfo isEqualToString:[NSString stringWithFormat:@"%@Result",kServiceCareCenter] ])
        {
            aryResult = [self getFilterRegionListWithResposeData:resposeData];
            
            if (aryResult.count == 0)
            {
                [self HudDismissWithError:@"查無資料，請重新查詢" finishBlock:^{
                } Title:nil];
            }
            else
            {
                
                [self HudDismissWithSuccess:@"下載完成" finishBlock:^{
//                    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//                    vcFilterResult* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcFilterResult"];
//                    vc.aryShowInfo = aryResult;
//                    vc.strCityArea = strKeywordCityArea;
//                    [self.navigationController pushViewController:vc animated:YES];
                    
                    self.aryShowInfo = aryResult;
                    [self showResultAlert:(int)self.aryShowInfo.count];

                    [self.tvResult reloadData];
                    [self clickOpenFilter:nil];
                }];
            }
            
            
        }
        
    }
    
}


#pragma mark - cell的展開與關閉

- (void) SpreadClassCell:(NSIndexPath *)indexPath
{
    int nItemCelllSection;
    
    if (bFromVcMainLogin)
    {
        nItemCelllSection = 3;
    }
    else
    {
        nItemCelllSection = 2;
    }
    
    
    if (nSpreadCell == -1)
    {
        [self imageRotateAction:NO indexPath:indexPath];//NO是展開
        nSpreadCell = (int)indexPath.section;
        
        
        
        if (nSpreadCell == nItemCelllSection)
        {
            NSArray *aryServiceItem =[[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
            int nTmp = (int)aryServiceItem.count + 1;
            [self creatCell:indexPath amount:nTmp];
        }
        else
        {
            [self creatCell:indexPath amount:1];
        }
        
    }
    else if(indexPath.section != nSpreadCell)
    {
        NSIndexPath * ip;
        
        [self imageRotateAction:NO indexPath:indexPath];
        ip = [NSIndexPath indexPathForRow:0 inSection:nSpreadCell];
        [self imageRotateAction:YES indexPath:ip];
        
        int nInsertAmount = 1;
        int nRemoveAmount = 1;
        
        //        aryServiceList = [self getAryServiceList];
        
        if (indexPath.section == nItemCelllSection)
        {
            NSArray *aryServiceItem =[[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
            nInsertAmount = (int)aryServiceItem.count + 1;
        }
        else if(nSpreadCell == nItemCelllSection)
        {
            NSArray *aryServiceItem =[[_aryServiceList objectAtIndex:nServiceType] objectForKey:@"Item"];
            nRemoveAmount = (int)aryServiceItem.count + 1;
        }
        
        
        nSpreadCell = (int)indexPath.section;
        
        
        [self updateTableView:ip RemoveAmount:nRemoveAmount InsertIndex:indexPath InsertAmount:nInsertAmount];
        
    }
    
}

- (void) deleteCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    [self imageRotateAction:YES indexPath:indexPath];
    NSMutableArray* aryDelete = [NSMutableArray new];
    if(nAmount == 1)
    {
        [aryDelete addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryDelete addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvFilter beginUpdates];
    [self.tvFilter deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationTop];
    [self.tvFilter endUpdates];
    [aryDelete removeAllObjects];
}

- (void) creatCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    NSMutableArray* aryInsert = [NSMutableArray new];
    
    if(nAmount == 1)
    {
        [aryInsert addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryInsert addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvFilter beginUpdates];
    [self.tvFilter insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tvFilter endUpdates];
    
    [self.tvFilter scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:nSpreadCell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [aryInsert removeAllObjects];
}

- (void) updateTableView:(NSIndexPath *) removeIndex RemoveAmount:(int) nRemoveAmount
             InsertIndex:(NSIndexPath *)insertIndex InsertAmount:(int) nInsertAmount
{
    NSMutableArray* aryDelete = [NSMutableArray new];
    NSMutableArray* aryInsert = [NSMutableArray new];
    
    NSIndexPath *ip;
    
    if (nRemoveAmount == 1)
    {
        ip = [NSIndexPath indexPathForRow:1 inSection:removeIndex.section];
        [aryDelete addObject:ip];
        
    }
    else
    {
        for (int i = 1; i < nRemoveAmount; i++)
        {
            [aryDelete addObject:[NSIndexPath indexPathForRow:i inSection:removeIndex.section]];
        }
    }
    
    if (nInsertAmount == 1)
    {
        ip = [NSIndexPath indexPathForRow:1 inSection:insertIndex.section];
        [aryInsert addObject:ip];
    }
    else
    {
        for (int i = 1; i < nInsertAmount; i++)
        {
            [aryInsert addObject:[NSIndexPath indexPathForRow:i inSection:insertIndex.section]];
        }
    }
    
    
    [self.tvFilter beginUpdates];
    [self.tvFilter deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationTop];
    [self.tvFilter insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tvFilter endUpdates];
    
    [self.tvFilter scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:nSpreadCell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    
    int nTmp = nSpreadCell;
    if (bFromVcMainLogin)
    {
        nSpreadCell = 3;
    }
    else
    {
        nSpreadCell = 2;
    }
    
    [self changeCellTitle:@"分類選擇(可複選)"];
    nSpreadCell = nTmp;
    
    [aryInsert removeAllObjects];
    [aryDelete removeAllObjects];
    
}

#pragma mark - Arrow動畫

- (void) imageRotateAction:(BOOL)isRotate indexPath:(NSIndexPath *) indexPath
{
    UITableViewCell *cell = [self.tvFilter cellForRowAtIndexPath:indexPath];
    
    for (UIView *v100 in cell.contentView.subviews)
    {
        
        for(UIView *vTmp in [v100 subviews])
        {
            if (![vTmp isKindOfClass:[UIImageView class]]) continue;
            
            UIImageView *imgArrow = (UIImageView *)vTmp;
            [UIView animateWithDuration:.35
                             animations:^{
                                 if (isRotate)
                                 {
                                     imgArrow.transform = CGAffineTransformIdentity;
                                     
                                 }else
                                 {
                                     imgArrow.transform = CGAffineTransformMakeRotation(180 * M_PI / 180);//M_PI_2 轉四分之一
                                 }
                                 
                             }completion:^(BOOL finished) {
                             }];
            
        }
        
    }
    
    
}


@end
