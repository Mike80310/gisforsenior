//
//  vcFilter.h
//  GisForSenior
//
//  Created by jane on 2015/9/4.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"
#import "SoapTool.h"
#import <TGMaps/TGMaps.h>

@interface vcFilter : vcBase<UITableViewDataSource,UITableViewDelegate,TGMapViewDelegate,UIGestureRecognizerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,SOAPToolDelegate,UIAlertViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView *tvMain;
@property (nonatomic,retain) NSString *strPrePage;
@end
