//
//  vcMain.h
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"
#import "SoapTool.h"

@interface vcMain : vcBase<SOAPToolDelegate,UITextFieldDelegate,UIAlertViewDelegate>

@property (nonatomic,weak) IBOutlet UIButton *btnHowto;
@property (nonatomic,weak) IBOutlet UIButton *btnSearch;
@property (nonatomic,weak) IBOutlet UIButton *btnLaw;
@property (nonatomic,weak) IBOutlet UILabel *lblAppTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblVer;
@end
