//
//  AppDelegate.m
//  GisForSenior
//
//  Created by jane on 2015/7/1.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "AppDelegate.h"
#import "KeychainItemWrapper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self getUUID];
//    [self removeUserDefaults];問卷式被取消後就不需要了
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
   
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) removeUserDefaults
{
    //    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"phoneToken"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kServiceRegionList];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kServiceCareCenterTypeList];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyAborigines];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyAge];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyCareTime];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyDisabilities];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDicKeyIntubation];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDefaultUserLatLng];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDefaultRegionKey];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDefaultCareCenterKey];

    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - 取得與存UUID

- (NSString *) getUUID
{
    NSString *strBundleID = [NSBundle mainBundle].bundleIdentifier;
    
    //keychainItem指定UUID的存放位置
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc]
                                         initWithIdentifier:@"UUID"
                                         accessGroup:[NSString stringWithFormat:@"%@.%@",provisingKey,strBundleID]];
    
    
    NSString *strUUID = [keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
    
    if( strUUID == nil || strUUID.length == 0 || [strUUID isEqualToString:@"iosdevice"])
    {
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        strUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
        
        [keychainItem setObject:strUUID forKey:(__bridge id)kSecAttrAccount];
    }
    
    NSLog(@"UUID為 %@",strUUID);
    
    [[NSUserDefaults standardUserDefaults] setObject:strUUID forKey:kUUID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return strUUID;
    
}



@end
