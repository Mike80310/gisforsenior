//
//  vcQIntubation.h
//  GisForSenior
//
//  Created by jane on 2015/7/16.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcBase.h"

@interface vcQIntubation : vcBase

@property (nonatomic,weak) IBOutlet UIView *vQuestionBG;
@property (nonatomic,weak) IBOutlet UILabel *lblQuestionTitle;

@property (nonatomic,weak) IBOutlet UIButton *btnNone;
@property (nonatomic,weak) IBOutlet UIButton *btnFirst;
@property (nonatomic,weak) IBOutlet UIButton *btnSecond;
@property (nonatomic,weak) IBOutlet UIButton *btnThird;
@property (nonatomic,weak) IBOutlet UIButton *btnFourth;

@property (nonatomic,weak) IBOutlet UIButton *btnYES;

@end
