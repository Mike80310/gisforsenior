//
//  vcPage3.m
//  GisForSenior
//
//  Created by jane on 2015/9/9.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcPage3.h"
#import "vcFilter.h"
#import "vcPage4.h"

@interface vcPage3 ()
{
    NSArray *arySelectedIntubation;
    NSArray *aryIntubation;
    NSArray *aryCareTime;
    
    int nCloseCell;
    
    //cell的layout設定
    int nCellHeight;
    int nCornerRadius;
    int nArrow;
    int nCellInterval;
    int nSelectedCareTimeIndex;
    
    

}
@end

@implementation vcPage3

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    nSelectedCareTimeIndex = 0;
    arySelectedIntubation = [NSArray new];
    int nCareTime = [[[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyCareTime] intValue];
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyCareTime])
    {
        nSelectedCareTimeIndex = nCareTime;
        NSArray *aryTmp = [[NSUserDefaults standardUserDefaults] valueForKey:kDicKeyIntubation];
        if (aryTmp.count != 0)
        {
            arySelectedIntubation = aryTmp;
        }
        
    }
   
    
    CGRect cgBtnYes = self.btnNext.frame;
    cgBtnYes.origin.y -= 10;
    cgBtnYes.size.height = barHeightInPlus;
    self.btnNext.frame = cgBtnYes;
    self.btnNext.layer.cornerRadius = barHeightInPlus/2;
    
    nCloseCell = -1;
   
    aryCareTime = @[@"白天",@"全日",@"彈性"];
    aryIntubation = @[
                      @{@"IntubationNo":@"2222",@"IntubationName":@"鼻胃管"},
                      @{@"IntubationNo":@"3333",@"IntubationName":@"尿管"},
                      @{@"IntubationNo":@"4444",@"IntubationName":@"氣切"},
                      @{@"IntubationNo":@"5555",@"IntubationName":@"引流管"},
                      @{@"IntubationNo":@"1111",@"IntubationName":@"無"},
                    ];
    
    nCellHeight = kCellHeight;
    nCornerRadius = kCornerRadius;
    nArrow = kArrow;
    nCellInterval = kCellInterval;
    
    if (screenSize.height >= kI6ScreenHeight)
    {
        nCellHeight +=15;
        nCornerRadius += 8;
        nArrow += 10;
        nCellInterval += kCellInterval;
    }
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushToNextQuestion:(id)sender
{
    if (nSelectedCareTimeIndex == -1)
    {
        [self HudErrMsgWithError:@"尚未選擇服務需求時段" finishBlock:^(void){
            
        }];
        
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:@(nSelectedCareTimeIndex) forKeyPath:kDicKeyCareTime];
    [[NSUserDefaults standardUserDefaults] setValue:arySelectedIntubation forKeyPath:kDicKeyIntubation];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcPage4* vc = [storyBoard instantiateViewControllerWithIdentifier:@"vcPage4"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;//非可隱藏的Cell全部都獨立一個Section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (nCloseCell == -1)
    {
        if (section == 0)
        {
            return 1;
        }
        else if(section == 1)
        {
            return 2;
        }
        else  if (section == 2)
        {
            return 1 + aryIntubation.count;
        }
    }
    else
    {
        if (nCloseCell == section) return 1;
        
        if (section == 1)
        {
            return 2;
        }
        else if (section == 2)
        {
            return 1 + aryIntubation.count;
        }
        
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return nCellHeight/2;
    }
    
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0) return nCellHeight;
        
        return pickerHeight;
    }
    else if (indexPath.section == 2)
    {
        return nCellHeight;
    }
    
    
    return nCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *CellIdentifier;
    
    int nSection = (int)indexPath.section;
    int nRow = (int)indexPath.row;
    
    if (nSection == 0)
    {
        CellIdentifier = [NSString stringWithFormat:@"TitleCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
        vContent.backgroundColor = [UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if (nSection == 1)
    {
        if (nRow == 0)
        {
            CellIdentifier = [NSString stringWithFormat:@"CareTimeTitleCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (nSelectedCareTimeIndex == -1)
            {
                return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"請問您需求服務時段"];
                
            }
            else
            {
                NSString *strTitle = [NSString stringWithFormat:@"請問您需求服務時段：%@",aryCareTime[nSelectedCareTimeIndex]];
                return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:strTitle];
            }
        }
        else
        {
            CellIdentifier = [NSString stringWithFormat:@"CareTimeSCCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.backgroundColor =[UIColor clearColor];
            
            [self setScCell:cell cellForRowAtIndexPath:indexPath ScrollViewTag:301];
        }
        
    }
    else if (nSection == 2)
    {
        if (nRow == 0)
        {
            CellIdentifier = [NSString stringWithFormat:@"IntubationTitleCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return [self setTitleCell:cell cellForRowAtIndexPath:indexPath titleName:@"是否有管路留置在身上(可複選)"];
        }
        else
        {
            CellIdentifier = [NSString stringWithFormat:@"IntubationItemCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            [self setItemCell:cell cellForRowAtIndexPath:indexPath];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1 || indexPath.section == 2)
    {
        if (nCloseCell == -1) //Cell都已經展開
        {
            if (indexPath.section == 2 && indexPath.row != 0)
            {
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                arySelectedIntubation = [self setArySelectedIdList:(int)cell.tag];
                [self.tvMain reloadData];
                return;
            }
            
            NSArray *aryServiceItem = [[NSArray alloc] init];
            
            if (indexPath.section == 2)
            {
                aryServiceItem = aryIntubation;
            }
            
            nCloseCell = (int)indexPath.section;
            int nTmp = 1 + (int)aryServiceItem.count;
            [self deleteCell:indexPath amount:nTmp];
            return;
        }
        
        if (indexPath.section == nCloseCell && indexPath.row == 0) //把已經關閉的Cell展開
        {
            
            [self SpreadClassCell:indexPath];
            
            return;
        }
        
        if (indexPath.row == 0)
        {
            [self SpreadClassCell:indexPath];
        }
        
        if (indexPath.section == 2 && indexPath.row != 0)
        {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            arySelectedIntubation = [self setArySelectedIdList:(int)cell.tag];
            [self.tvMain reloadData];
            return;
        }

        
    }
}

-(NSArray *) setArySelectedIdList:(int) nItemNo
{
    NSMutableArray *aryNewSelected = [arySelectedIntubation mutableCopy];
    BOOL bAlreadySelected = false;
    
    if(nItemNo == 1111)
    {
        return [NSArray new];
    }
    else
    {
        for (int i = 0; i < aryNewSelected.count; i++)
        {
            NSDictionary *dicItem = aryNewSelected[i];
            
            if (nItemNo != [[dicItem objectForKey:@"IntubationNo"] intValue]) continue;
            
            bAlreadySelected = YES;
            [aryNewSelected removeObject:dicItem];
            break;
        }
        
        if (bAlreadySelected)
        {
            return aryNewSelected;
        }
        else
        {
            for (NSDictionary *dicItem in aryIntubation)
            {
                if (nItemNo != [[dicItem objectForKey:@"IntubationNo"] intValue]) continue;
                
                [aryNewSelected addObject:dicItem];
                
            }
            
            NSArray *aryTmp = aryNewSelected;
            
            NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"IntubationNo" ascending:YES];
            NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
            aryTmp = [aryTmp sortedArrayUsingDescriptors:sortDescriptors];
            
            return aryTmp;
        }

    }
    return [NSArray new];
}


#pragma mark - 各式Cell
- (UITableViewCell *) setTitleCell:(UITableViewCell *)cell
             cellForRowAtIndexPath:(NSIndexPath *)indexPath titleName:(NSString *) strName
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    vContent.layer.cornerRadius = nCornerRadius;
    vContent.layer.masksToBounds = YES;
    vContent.layer.borderWidth = 1;
    vContent.layer.borderColor = [self colorFromHexString:kBorderColorID].CGColor;
    
    UILabel *lblTitleName = (UILabel *) [cell.contentView viewWithTag:101];
    lblTitleName.text = strName;
    
    UIImageView *imgArrow = (UIImageView *) [cell.contentView viewWithTag:102];
    
    CGRect cgImg = imgArrow.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    cgImg.origin.y = (nCellHeight - nArrow) /2;
    cgImg.origin.x = cgTmp.size.width - nArrow*4/3;
    imgArrow.frame = cgImg;
    
    CGRect cgTitle = lblTitleName.frame;
    cgTitle.origin.y = cgTmp.origin.y;
    cgTitle.origin.x = cgTmp.origin.x;
    //    cgTitle.size.width = cgTmp.size.width - (cgTmp.size.width - cgImg.origin.x);
    cgTitle.size.height = cgTmp.size.height;
    lblTitleName.frame = cgTitle;
    lblTitleName.font = [UIFont systemFontOfSize:fFontSize];
    
    return cell;
}

- (UITableViewCell *) setScCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath ScrollViewTag:(int) nTag
{
    
    UIPickerView *pv300 = (UIPickerView *) [cell.contentView viewWithTag:nTag];
    pv300.delegate = self;
    pv300.dataSource = self;
    pv300.backgroundColor = [UIColor whiteColor];
    
    CGRect cgPV =pv300.frame;
    cgPV.size.width = cell.contentView.frame.size.width - cgPV.origin.x * 2;
    cgPV.size.height = pickerHeight;
    pv300.frame = cgPV;
    
    if(nSelectedCareTimeIndex == -1)
    {
        nSelectedCareTimeIndex = 0;
        [self changeCellTitle:[NSString stringWithFormat:@"請問您需求服務時段：%@",aryCareTime[nSelectedCareTimeIndex]] Section:1];
    }
    else
    {
        [pv300 selectRow:nSelectedCareTimeIndex inComponent:0 animated:YES];
        
    }
    
    
    return cell;
}

- (UITableViewCell *) setItemCell:(UITableViewCell *) cell cellForRowAtIndexPath:(NSIndexPath *) indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *vContent = (UIView *) [cell.contentView viewWithTag:100];
    CGRect cgTmp = vContent.frame;
    cgTmp.origin.y = 0;
    cgTmp.size.height = nCellHeight;
    cgTmp.size.width = cell.contentView.frame.size.width - cgTmp.origin.x * 2;
    vContent.frame = cgTmp;
    vContent.backgroundColor = [UIColor whiteColor];
    
    int nRow = (int)indexPath.row;
    
    UIImageView* imgSelect = (UIImageView *) [cell.contentView viewWithTag:101];
    CGRect cgImg = imgSelect.frame;
    cgImg.size = CGSizeMake(nArrow, nArrow);
    imgSelect.frame = cgImg;
    
    UILabel *lblItemName = (UILabel *) [cell.contentView viewWithTag:102];
    CGRect cgLabel = lblItemName.frame;
    cgLabel.size.height = nCellHeight;
    cgLabel.origin.x = cgImg.origin.x + cgImg.size.width + 5;
    cgLabel.origin.y = 0;
    lblItemName.frame = cgLabel;
    [lblItemName setFont:[UIFont systemFontOfSize:fFontSize]];
    
    NSString *strItemName = [[aryIntubation objectAtIndex:nRow-1] objectForKey:@"IntubationName"];
    [lblItemName setText:strItemName];
    
    int nItemNo = [[[aryIntubation objectAtIndex:nRow-1] objectForKey:@"IntubationNo"] intValue];
    cell.tag = nItemNo;
    
    BOOL bIsSelected = false;
    
    if (arySelectedIntubation.count == 0 && indexPath.row == aryIntubation.count)
    {
        bIsSelected = YES;
    }
    else
    {
        for (int i = 0; i < arySelectedIntubation.count; i++)
        {
            if (cell.tag != [[arySelectedIntubation[i] objectForKey:@"IntubationNo"] intValue]) continue;
            
            bIsSelected = YES;
        }
    }
    
    if (bIsSelected)
    {
        [imgSelect setImage:[UIImage imageNamed:@"check01"]];
    }
    else
    {
        [imgSelect setImage:[UIImage imageNamed:@"check02"]];
    }
    
    return cell;
}

#pragma mark - bar名稱改變

- (void) changeCellTitle:(NSString *) strNewTitle Section:(int) nSection
{
    
    UITableViewCell *cell = [self.tvMain cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:nSection]];
    
    for (UIView *v100 in cell.contentView.subviews) {
        
        for(UIView *vTmp in [v100 subviews])
        {
            if (![vTmp isKindOfClass:[UILabel class]]) continue;
            if (vTmp.tag == 500) continue;
            
            
            UILabel *lbl = (UILabel *) vTmp;
            lbl.text = strNewTitle;
        }
        
    }
    
}


#pragma mark - cell的展開與關閉

- (void) SpreadClassCell:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == nCloseCell)
    {
        [self imageRotateAction:NO indexPath:indexPath];//NO是展開
        
        nCloseCell = -1;
        if (indexPath.section ==2)
        {
            int nTmp = (int)aryIntubation.count + 1;
            [self creatCell:indexPath amount:nTmp];
        }
        else
        {
            [self creatCell:indexPath amount:1];
        }
    }
    else
    {
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:nCloseCell];
        [self imageRotateAction:NO indexPath:ip];
        [self imageRotateAction:YES indexPath:indexPath];
        
        int nInsertAmount = 1;
        int nRemoveAmount = 1;
        
        if (indexPath.section == 2)
        {
            nRemoveAmount = (int)aryIntubation.count + 1;
        }
        else if(nCloseCell == 2)
        {
            nInsertAmount = (int)aryIntubation.count + 1;
        }
        
        nCloseCell = (int)indexPath.section;
        
        [self updateTableView:indexPath RemoveAmount:nRemoveAmount InsertIndex:ip InsertAmount:nInsertAmount];
        
    }

}

- (void) deleteCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    [self imageRotateAction:YES indexPath:indexPath];
    NSMutableArray* aryDelete = [NSMutableArray new];
    if(nAmount == 1)
    {
        [aryDelete addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryDelete addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvMain beginUpdates];
    [self.tvMain deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    [aryDelete removeAllObjects];
}

- (void) creatCell:(NSIndexPath*) indexPath amount:(int) nAmount
{
    NSMutableArray* aryInsert = [NSMutableArray new];
    
    if(nAmount == 1)
    {
        [aryInsert addObject:[NSIndexPath indexPathForRow:nAmount inSection:indexPath.section]];
    }
    else
    {
        for (int i = 1; i < nAmount; i++)
        {
            [aryInsert addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
    }
    
    [self.tvMain beginUpdates];
    [self.tvMain insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    
    [aryInsert removeAllObjects];
}

- (void) updateTableView:(NSIndexPath *) removeIndex RemoveAmount:(int) nRemoveAmount
             InsertIndex:(NSIndexPath *)insertIndex InsertAmount:(int) nInsertAmount
{
    NSMutableArray* aryDelete = [NSMutableArray new];
    NSMutableArray* aryInsert = [NSMutableArray new];
    
    NSIndexPath *ip;
    
    if (nRemoveAmount == 1)
    {
        ip = [NSIndexPath indexPathForRow:1 inSection:removeIndex.section];
        [aryDelete addObject:ip];
        
    }
    else
    {
        for (int i = 1; i < nRemoveAmount; i++)
        {
            [aryDelete addObject:[NSIndexPath indexPathForRow:i inSection:removeIndex.section]];
        }
    }
    
    if (nInsertAmount == 1)
    {
        ip = [NSIndexPath indexPathForRow:1 inSection:insertIndex.section];
        [aryInsert addObject:ip];
    }
    else
    {
        for (int i = 1; i < nInsertAmount; i++)
        {
            [aryInsert addObject:[NSIndexPath indexPathForRow:i inSection:insertIndex.section]];
        }
    }
    
    
    [self.tvMain beginUpdates];
    [self.tvMain deleteRowsAtIndexPaths:aryDelete withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain insertRowsAtIndexPaths:aryInsert withRowAnimation:UITableViewRowAnimationTop];
    [self.tvMain endUpdates];
    
    [self changeCellTitle:@"是否有插管治療(可複選)" Section:2];
    
    [aryInsert removeAllObjects];
    [aryDelete removeAllObjects];
    
}

#pragma mark - Arrow動畫

- (void) imageRotateAction:(BOOL)isRotate indexPath:(NSIndexPath *) indexPath
{
    UITableViewCell *cell = [self.tvMain cellForRowAtIndexPath:indexPath];
    
    for (UIView *v100 in cell.contentView.subviews)
    {
        
        for(UIView *vTmp in [v100 subviews])
        {
            if (![vTmp isKindOfClass:[UIImageView class]]) continue;
            
            UIImageView *imgArrow = (UIImageView *)vTmp;
            
            [UIView animateWithDuration:.35
                             animations:^
             {
                 if (isRotate)
                 {
                     imgArrow.transform = CGAffineTransformIdentity;
                     
                 }else
                 {
                     imgArrow.transform = CGAffineTransformMakeRotation(180 * M_PI / 180);//M_PI_2 轉四分之一
                 }
                 
             }
                             completion:^(BOOL finished)
             {
                 
             }];
            
        }
        
    }
    
    
}

#pragma mark - picker

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerRowHeight;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return aryCareTime.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return aryCareTime[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    nSelectedCareTimeIndex = (int) row;
    [self changeCellTitle:[NSString stringWithFormat:@"請問您需求服務時段：%@",aryCareTime[nSelectedCareTimeIndex]] Section:1];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
