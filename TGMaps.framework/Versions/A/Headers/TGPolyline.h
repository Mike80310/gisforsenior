//
//  TGOSPolyline.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/9.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TGPolylineOptions.h"

@class TGPath;

@protocol TGPolyline

@property (nonatomic) TGPath *path;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, assign) int width;

@property (nonatomic, assign) TGLineStyle style;

@property (nonatomic, assign) BOOL visibility;

- (void)remove;

@end
