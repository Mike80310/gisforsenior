//
//  vcPage1.m
//  GisForSenior
//
//  Created by jane on 2015/9/9.
//  Copyright (c) 2015年 jane. All rights reserved.
//

#import "vcPage1.h"
#import "vcPage2.h"

@interface vcPage1 ()

@end

@implementation vcPage1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.txtMain setFont:[UIFont boldSystemFontOfSize:fFontSize]];
}

- (IBAction)pushToNextQuestion:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    vcPage2* vc= [storyBoard instantiateViewControllerWithIdentifier:@"vcPage2"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
