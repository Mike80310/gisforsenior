//
//  TGOSUISettings.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/4/3.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGUISettings : NSObject

/**
 * Controls whether scroll gestures are enabled (default) or disabled. If
 * enabled, users may swipe to pan the camera. This does not limit programmatic
 * movement of the camera.
 */
@property (nonatomic, assign) BOOL scrollGestures;

/**
 * Controls whether zoom gestures are enabled (default) or disabled. If
 * enabled, users may double tap/two-finger tap or pinch to zoom the camera.
 * This does not limit programmatic movement of the camera.
 *
 * Note that zoom gestures may allow the user to pan around the map, as a double
 * tap gesture will move the camera towards the specified point, and conversely
 * for two-finger tap to zoom out.
 */
@property (nonatomic, assign) BOOL zoomGestures;

/**
 * Enables or disables the My Location button. This is a button visible on the
 * map that, when tapped by users, will center the map on the current user
 * location.
 */
@property (nonatomic, assign) BOOL myLocationButton;

@end
