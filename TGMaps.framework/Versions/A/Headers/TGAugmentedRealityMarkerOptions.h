//
//  AugmentedRealityMarkerOptions.h
//  TGOSMaps
//
//  Created by supergeo.rd on 13/5/1.
//  Copyright (c) 2013年 supergeo.rd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TGAugmentedRealityMarkerOptions : NSObject

@property (nonatomic, assign) CLLocationCoordinate2D position;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) UIImage *icon;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, strong) id userData;

@property (nonatomic, assign) CGSize size;

/** Convenience constructor for default initialized options. */
+ (TGAugmentedRealityMarkerOptions *)options;

@end
