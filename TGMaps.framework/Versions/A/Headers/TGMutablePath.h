//
//  addCoordinate.h
//  SuperGIS SDK
//
//  Created by supergeo.rd on 13/3/1.
//  Copyright (c) 2013年 supergeo.rd@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "TGPath.h"

@interface TGMutablePath : TGPath

/** Adds |coord| at the end of the path. */
- (void)addCoordinate:(CLLocationCoordinate2D)coord;

/** Adds a new CLLocationCoordinate2D instance with the given lat/lng. */
- (void)addLatitude:(CLLocationDegrees)latitude
          longitude:(CLLocationDegrees)longitude;

/**
 * Inserts |coord| at |index|.
 *
 * If this is smaller than the size of the path, shifts all coordinates forward
 * by one. Otherwise, behaves as replaceCoordinateAtIndex:withCoordinate: and
 * grows the array with kCLLocationCoordinate2DInvalid.
 */
- (void)insertCoordinate:(CLLocationCoordinate2D)coord
                 atIndex:(NSUInteger)index;

/**
 * Replace the coordinate at |index| with |coord|.
 *
 * If |index| is after the end, grows the array
 * with kCLLocationCoordinate2DInvalid.
 */
- (void)replaceCoordinateAtIndex:(NSUInteger)index
                  withCoordinate:(CLLocationCoordinate2D)coord;

/**
 * Remove entry at |index|.
 *
 * If |index| < count decrements size. If |index| >= count this is a silent
 * no-op.
 */
- (void)removeCoordinateAtIndex:(NSUInteger)index;

/**
 * Removes the last coordinate of the path.
 *
 * If the array is non-empty decrements size. If the array is empty, this is
 * a silent no-op.
 */
- (void)removeLastCoordinate;

/**
 * Removes all coordinates in this path.
 */
- (void)removeAllCoordinates;

@end
